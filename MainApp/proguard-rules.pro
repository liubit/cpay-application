# Add project specific ProGuard rules here.
# By default, the flags in this file are appended to flags specified
# in C:\Work\Android_SDK/tools/proguard/proguard-android.txt
# You can edit the include path and order by changing the proguardFiles
# directive in build.gradle.
#
# For more details, see
#   http://developer.android.com/guide/developing/tools/proguard.html

# Add any project specific keep options here:

# If your project uses WebView with JS, uncomment the following
# and specify the fully qualified class name to the JavaScript interface
# class:
#-keepclassmembers class fqcn.of.javascript.interface.for.webview {
#   public *;
#}
#忽略警告
-ignorewarnings

-keep public class * extends android.app.Activity
-keep public class * extends android.app.Fragment
-keep public class * extends android.app.Application
-keep public class * extends android.app.Service
-keep public class * extends android.content.BroadcastReceiver
-keep public class * extends android.content.ContentProvider
-keep public class * extends android.support.v4.app.Fragment

-keep interface **{*;}
-keep class org.apache.**{*;}
-keep class com.j256.ormlite.**{*;}
-keep class com.nostra13.universalimageloader.**{*;}
-keep class com.loopj.android.http.**{*;}
-keep class com.google.**{*;}
-keep class com.centerm.smartpos.aidl.**{*;}
-keep class com.centerm.cpay.applicationshop.db.DbHelper{*;}
-keepclassmembers class com.centerm.cpay.applicationshop.bean.**{
    <fields>;
    void set*(***);
    *** get*();
}
-keep class com.centerm.cpay.applicationshop.web.**{*;}
-keepattributes *JavascriptInterface*
################### region for xUtils
-keepattributes Signature,*Annotation*
-keep public class org.xutils.** {
    public protected *;
}
-keep public interface org.xutils.** {
    public protected *;
}
-keepclassmembers class * extends org.xutils.** {
    public protected *;
}
-keepclassmembers @org.xutils.db.annotation.* class * {*;}
-keepclassmembers @org.xutils.http.annotation.* class * {*;}
-keepclassmembers class * {
    @org.xutils.view.annotation.Event <methods>;
}

#################### end region