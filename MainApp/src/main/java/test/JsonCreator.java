package test;

import com.centerm.cpay.applicationshop.net.cont.JsonKey;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import static com.centerm.cpay.applicationshop.net.cont.JsonKey.*;

/**
 * Created by linwanliang on 2016/6/24.
 */
public class JsonCreator {

    public static String getAppTypeList() {
        JSONObject root = createRoot();
        JSONObject body = new JSONObject();
        JSONArray typeArray = new JSONArray();
        try {
            for (int i = 0; i < 5; i++) {
                JSONObject item = new JSONObject();
                item.put(JsonKey.apkTypeId, "0000" + i);
                item.put(JsonKey.apkTypeName, i + "金融理财");
                item.put(JsonKey.iconUrl, "http://120.26.234.49:6922/files/cpay2.0/fenlei_03.png");
                JSONArray childs = new JSONArray();
                item.put(JsonKey.apps, childs);
                JSONArray childApps = new JSONArray();
                for (int j = 0; j < 5; j++) {
                    JSONObject jj = new JSONObject();
                    if (j == 0) {
                        jj.put(packageName, "com.android.nearshop");
                    } else {
                        jj.put(packageName, "000" + j);
                    }
                    jj.put(name, j + "常惠生活");
                    jj.put(rate, "3");
                    jj.put(downloadCount, "15268");
                    jj.put(downloadUrl, "http://112.65.222.35/dd.myapp.com/16891/EA3C003AD876A119A48424D9287DF161.apk?mkey=57735e2ea3bddf29&f=d388&c=0&fsname=CHSH_4.4.6.apk/&p=.apk");
                    jj.put(iconUrl, "http://image.baidu.com/search/down?tn=download&word=download&ie=utf8&fr=detail&url=http%3A%2F%2Fpic.weifengke.com%2Fattachments%2F2%2F2995%2Fd64c720250ba04dd3c11a0d745af9ecc.jpg&thumburl=http%3A%2F%2Fimg0.imgtn.bdimg.com%2Fit%2Fu%3D334758862%2C3562811084%26fm%3D21%26gp%3D0.jpg");
                    jj.put(size, "8763862");
                    jj.put(version, "86");
                    jj.put(previewImg, "http://e.hiphotos.bdimg.com/wisegame/pic/item/20166d224f4a20a44231c46997529822720ed075.jpg|http://g.hiphotos.bdimg.com/wisegame/pic/item/6f338744ebf81a4c47241467d02a6059252da60e.jpg|http://f.hiphotos.bdimg.com/wisegame/pic/item/6ff3d7ca7bcb0a46ff0d73976c63f6246b60af75.jpg|http://g.hiphotos.bdimg.com/wisegame/pic/item/462dd42a2834349b1d05962bceea15ce36d3be19.jpg");
                    jj.put(versionName, "4.4.6");
                    jj.put(appType, "金融理财");
                    childApps.put(j, jj);
                }
                item.put(JsonKey.apps, childApps);
                typeArray.put(i, item);
            }
            body.put(JsonKey.types, typeArray);
            root.put(JsonKey.body, body);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return root.toString();
    }


    public static String getApplist() {
        JSONObject root = createRoot();
        JSONObject body = new JSONObject();
        try {
            body.put(totalNumber, "20");
            JSONArray array = new JSONArray();
            for (int i = 0; i < 10; i++) {
                JSONObject jj = new JSONObject();
                if (i == 0) {
                    jj.put(packageName, "com.android.nearshop");
                } else {
                    jj.put(packageName, "000" + i);
                }
                jj.put(name, i + "常惠生活");
                jj.put(rate, "3");
                jj.put(downloadCount, "15268");
                jj.put(downloadUrl, "http://112.65.222.35/dd.myapp.com/16891/EA3C003AD876A119A48424D9287DF161.apk?mkey=57735e2ea3bddf29&f=d388&c=0&fsname=CHSH_4.4.6.apk/&p=.apk");
                jj.put(iconUrl, "http://image.baidu.com/search/down?tn=download&word=download&ie=utf8&fr=detail&url=http%3A%2F%2Fpic.weifengke.com%2Fattachments%2F2%2F2995%2Fd64c720250ba04dd3c11a0d745af9ecc.jpg&thumburl=http%3A%2F%2Fimg0.imgtn.bdimg.com%2Fit%2Fu%3D334758862%2C3562811084%26fm%3D21%26gp%3D0.jpg");
                jj.put(size, "8763862");
                jj.put(previewImg, "http://e.hiphotos.bdimg.com/wisegame/pic/item/20166d224f4a20a44231c46997529822720ed075.jpg|http://g.hiphotos.bdimg.com/wisegame/pic/item/6f338744ebf81a4c47241467d02a6059252da60e.jpg|http://f.hiphotos.bdimg.com/wisegame/pic/item/6ff3d7ca7bcb0a46ff0d73976c63f6246b60af75.jpg|http://g.hiphotos.bdimg.com/wisegame/pic/item/462dd42a2834349b1d05962bceea15ce36d3be19.jpg");
                jj.put(version, "86");
                jj.put(versionName, "4.4.6");
                jj.put(appType, "金融理财");
                array.put(i, jj);
            }
            body.put(apps, array);
            root.put(JsonKey.body, body);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return root.toString();
    }

    public static JSONObject getAppDetail() {
        JSONObject root = createRoot();
        JSONObject realBody = new JSONObject();
        JSONObject body = new JSONObject();
        try {
            body.put(appCode, "2");
            body.put(name, "常惠生活");
            body.put(rate, "4.5");
            body.put(downloadCount, 459871);
            body.put(description, "【常惠生活，您身边的优惠专家】\n" +
                    "常惠生活是中国邮政集团公司中邮电子商务有限公司为旗下常惠生活项目配套的手机APP，常惠生活专注本地O2O服务，与大量吃喝玩乐购各行各业商家签约合作，为会员提供线下打折优惠+线上团购服务；目前在东莞、恩施、菏泽、延边四个地市上线运营，旗下拥有上万家各地市优质商家，比如麦当劳、真功夫、全家蛋糕等等；本地商户电话、地址、地图、点评、比价一网打尽，更有1元专享、免费赠送优惠券等活动，让你轻轻松松搜索优惠享受生活，赶快下载体验吧！\n" +
                    "【客服电话】\n" +
                    "东莞客服：400-620-1185\n" +
                    "恩施客服：400-809-1185");
            body.put(updateInfo, "1. 修复一些Bug，提升功能性能；\n" +
                    "2.. 改善部分界面的用户操作体验。");
            body.put(downloadUrl, "http://112.65.222.35/dd.myapp.com/16891/EA3C003AD876A119A48424D9287DF161.apk?mkey=57735e2ea3bddf29&f=d388&c=0&fsname=CHSH_4.4.6.apk/&p=.apk");
            body.put(iconUrl, "http://image.baidu.com/search/down?tn=download&word=download&ie=utf8&fr=detail&url=http%3A%2F%2Fpic.weifengke.com%2Fattachments%2F2%2F2995%2Fd64c720250ba04dd3c11a0d745af9ecc.jpg&thumburl=http%3A%2F%2Fimg0.imgtn.bdimg.com%2Fit%2Fu%3D334758862%2C3562811084%26fm%3D21%26gp%3D0.jpg");
            body.put(size, "8763862");
            body.put(version, "86");
            body.put(versionName, "4.4.6");
            body.put(developer, "Dg11185");
            body.put(publicTime, "2016-06-30");
            body.put(previewImg, "http://img.wdjimg.com/mms/screenshot/c/6b/25d3dc62257191a2af2309d3ba7946bc_320_570.jpeg|" +
                    "http://img.wdjimg.com/mms/screenshot/c/5a/2f751001b726cfe6f61b7877de3315ac_320_570.jpeg|"
                    + "http://img.wdjimg.com/mms/screenshot/c/20/09a66802af2ee6f2d92fcb7508e0220c_320_570.jpeg|"
                    + "http://img.wdjimg.com/mms/screenshot/8/9c/616d3602cfae6c1f3deb8ecd7aa119c8_320_570.jpeg");
            body.put(typeName, "金融理财");
            body.put(keyword, "生活|金融|便民");
            realBody.put(app, body);
            root.put(JsonKey.body, realBody);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return root;
    }


    private static JSONObject createRoot() {
        JSONObject root = new JSONObject();
        try {
            root.put(MAC, "12345678");
            root.put(header, createHeader());
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return root;
    }


    private static JSONObject createHeader() {
        JSONObject header = new JSONObject();
        try {
            header.put(version, "01");
            header.put(devMask, "1234567890123456");
            header.put(timestamp, "20160101010101");
            header.put(token, "123456");
            header.put(status, "000000");
            header.put(msg, "请求成功");
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return header;
    }


}
