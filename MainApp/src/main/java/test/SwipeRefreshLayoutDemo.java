package test;

import android.os.Handler;
import android.support.v4.widget.SwipeRefreshLayout;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.ScrollView;
import android.widget.Toast;

import com.centerm.cpay.applicationshop.R;
import com.centerm.cpay.applicationshop.base.BaseActivity_v21;

/**
 * Created by linwanliang on 2016/6/24.
 */
public class SwipeRefreshLayoutDemo extends BaseActivity_v21 {

    @Override
    public int onInitLayoutId() {
        return R.layout.demo_swipe_refresh_layout;
    }

    @Override
    public void onInitLocalData() {

    }

    @Override
    public void onInitView(View view) {
        final SwipeRefreshLayout refreshLayout = (SwipeRefreshLayout) findViewById(R.id.refresh_layout);
        ListView listView = (ListView) findViewById(R.id.list_v);
        ScrollView scrollView = (ScrollView) findViewById(R.id.scroll_v);
        if (listView != null) {
            listView.setAdapter(new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1
                    , new String[]{"1", "2", "2", "2", "2", "2", "2", "2", "2", "2", "2", "2", "2", "2", "2", "2", "2", "2", "2", "2", "2", "2", "2", "2", "2", "2", "2", "2", "2", "2", "2", "2", "2", "2", "2", "2", "2", "2", "2", "2", "2", "2", "2"}));
            if (refreshLayout != null) {
                refreshLayout.setProgressBackgroundColorSchemeResource(R.color.refresh_layout_progress_bg);
//                refreshLayout.setColorScheme(getResources().getColor(R.color.colorAccent));
                refreshLayout.setColorSchemeResources(R.color.colorAccent);
                refreshLayout.setSize(SwipeRefreshLayout.LARGE);
                refreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
                    @Override
                    public void onRefresh() {
                        new Handler().postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                Toast.makeText(SwipeRefreshLayoutDemo.this, "加载完成", Toast.LENGTH_LONG).show();
                                refreshLayout.setRefreshing(false);
                            }
                        }, 1000);
                    }
                });
            }
        }
        if (scrollView != null) {
            scrollView.scrollTo(0, 0);
        }
    }
}
