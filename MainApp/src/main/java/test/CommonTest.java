package test;

import android.content.Context;
import android.telephony.TelephonyManager;

import com.centerm.cloudsys.sdk.common.log.Log4d;

/**
 * Created by linwanliang on 2016/6/21.
 */
public class CommonTest {
    private static Log4d LOGGER = Log4d.getDefaultInstance();

    public static void getIMEI(Context context) {
        TelephonyManager manager = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
        LOGGER.info("test", "imei = " + manager.getDeviceId());
    }

    public static void getIMSI(Context context) {
        TelephonyManager manager = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
        LOGGER.info("test", "imsi = " + manager.getSubscriberId());
    }


}
