package config;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.text.TextUtils;
import android.util.Log;

import com.centerm.cpay.applicationshop.MyApplication;
import com.centerm.cpay.applicationshop.R;
import com.centerm.cpay.applicationshop.utils.FileUtils;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.display.RoundedBitmapDisplayer;

import java.io.File;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Created by linwanliang on 2016/6/20.
 * 默认配置信息
 */
public class Config {
    public final static boolean DEMO = false;
    public final static boolean CHECK_MAC = true;
    public final static boolean CACHE = true;

    public final static String DB_NAME = "CpayAppStore.db";//数据库名称
    public final static int DB_VERSION = 2;//数据库版本号
    public final static String SCREEN_TYPE = "1";//1是heng屏终端，2是shu屏终端,3兼容
    public final static long MIN_LAUNCH_STAY_TIME = 1 * 1000;//启动界面停留的最小时间
    public final static long AD_SCROLL_TIME = 3 * 1000;//广告自动轮播的时间
    public final static long DELAY_LOAD_DATA = 700;//为了页面切换更加顺畅。延迟加载界面数据的等待时间
    public final static String ACTION_UPDATE_TIPS = "LAUNCHER_NOTIFICATION_UPDATE";//广播ACTION，用于发送给Launcher当前可更新应用数
    public final static long QUERY_APP_VERSION_INTERVAL = 60 * 60 * 1000;//定时检测应用更新的时间间隔
    public final static int MAC_KEY_INDEX = 2;//计算MAC的key索引
    public final static String LOG_PATH = File.separator + "CpayAppStore" + File.separator + "logs";//Log文件存放位置

    public final static int DEFAULT_APP_ICON = R.drawable.launcher_loading;//应用图标未加载出来时的显示
    public final static int DEFAULT_APP_SCREENSHOT = R.drawable.details_loading;//截图未加载出来时的显示
    public final static int DEFAULT_SORT_APP_COUNTS = 3;//分类应用界面的预览应用数
    public final static int DEFAULT_APP_PAGE_SIZE = 18;//应用分页加载的应用数
    public final static int[] AD_PAGE_INDICATION_ICON = {R.drawable.dot, R.drawable.dot_on};


    public final static DisplayImageOptions APP_ICON_OPTIONS = new DisplayImageOptions.Builder()
            .showImageForEmptyUri(Config.DEFAULT_APP_ICON)
            .showImageOnFail(Config.DEFAULT_APP_ICON)
            .cacheInMemory(false)
            .cacheOnDisk(true)
            .displayer(new RoundedBitmapDisplayer(10))
            .build();

    public final static DisplayImageOptions TYPE_ICON_OPTIONS = new DisplayImageOptions.Builder()
            .cacheInMemory(false)
            .cacheOnDisk(true)
            .displayer(new RoundedBitmapDisplayer(10))
            .build();

    public final static DisplayImageOptions AD_IMAGE_OPTIONS = new DisplayImageOptions.Builder()
            .showImageForEmptyUri(R.drawable.default_ad_img)
            .showImageOnFail(R.drawable.default_ad_img)
            .cacheInMemory(false)
            .cacheOnDisk(true)
            .build();
    public final static Set<String> MASK_APP_SETS;
    public final static List<String> DEFALUT_HOT_KEYWORDS;

    static {
        MASK_APP_SETS = new HashSet<String>();
        MASK_APP_SETS.add("com.google.android.inputmethod.pinyin");
        MASK_APP_SETS.add("com.centerm.cpay.launcher");
        MASK_APP_SETS.add("com.centerm.cpay.applicationshop");
        MASK_APP_SETS.add("com.centerm.cpay.gtms");
        MASK_APP_SETS.add("com.centerm.gtms.daemon");
        MASK_APP_SETS.add("com.centerm.cpay.cpaytermset");
        MASK_APP_SETS.add("com.centerm.upgrade");
        MASK_APP_SETS.add("com.centerm.v8pqc");
        MASK_APP_SETS.add("com.centerm.smartposservice");
        MASK_APP_SETS.add("com.centerm.gtms.daemon");
        MASK_APP_SETS.add("com.centerm.cpay.cpaytermset");
        MASK_APP_SETS.add("com.centerm.cpay.securitysuite");
        MASK_APP_SETS.add("com.centerm.cpay.helper");
//        MASK_APP_SETS.add("com.centerm.cpay.payment");
        MASK_APP_SETS.add("com.centerm.cpay.autoactive");
        MASK_APP_SETS.add("com.centerm.tools.meidinject");
        MASK_APP_SETS.add("com.centerm.cpay.lklhelper");
        MASK_APP_SETS.add("com.centerm.cpay.member");
        DEFALUT_HOT_KEYWORDS = new ArrayList<>();
        DEFALUT_HOT_KEYWORDS.add("Q哥点餐");
        DEFALUT_HOT_KEYWORDS.add("Cpay");
        DEFALUT_HOT_KEYWORDS.add("POS助手");
        DEFALUT_HOT_KEYWORDS.add("百度糯米商家版");
        DEFALUT_HOT_KEYWORDS.add("通联支付");
        DEFALUT_HOT_KEYWORDS.add("便民缴费");
        DEFALUT_HOT_KEYWORDS.add("美团商家版");
    }


    public static class FILE_DOWNLOADER {
        private final static String FILE_PATH_PREFIX = File.separator + "CpayAppStore";
        public final static long MIN_FREE_SIZE = 50 * FileUtils.MB;//下载文件时的最小空间要求
        public final static String APK_PATH = FILE_PATH_PREFIX + File.separator + "download" + File.separator + "apks";//APK文件的存放位置
        public final static String AD_PATH = FILE_PATH_PREFIX + File.separator + "download" + File.separator + "ads";//广告文件的存放位置
        public final static String CACHE_PATH = FILE_PATH_PREFIX + File.separator + "cache";//缓存文件存放位置
    }

    public static class NET {
        public final static int DEFAULT_TIMEOUT = 15 * 1000;//默认超时时间
        public final static String DEFAULT_CHARSET = "UTF-8";//默认字符编码
        public final static String PROTOCOL_VERSION = "01";//通讯协议版本
        //荣平地址http://192.168.31.102:9527
        //内网服务器http://192.168.48.45:8080/cpay-store
//        public static String DEFAULT_ADDRESS = "http://192.168.48.45:8080/cpay-store";//默认接口通讯地址
        //        public final static String DEFAULT_ADDRESS = "http://114.55.227.72:8089/cpay-ams-gate";
        public final static long DEFAULT_CACHE_VALIDITY = 24 * 60 * 60 * 1000;//网络缓存有效期
        //        private static String address = getNetAddress();
        private static String charset = DEFAULT_CHARSET;

//        public static String getAddress() {
//            return address;
//        }
//
//        public static void setAddress(String address) {
//            NET.address = address;
//        }

        public static String getCharset() {
            return charset;
        }

        public static void setCharset(String charset) {
            NET.charset = charset;
        }
    }

    public static class WEB {
        public static final String APP_CACAHE_DIRNAME = File.separator + "webcache";
    }

    private static String getNetAddressFromGtms(Context context) {
        try {
            Context otherAppContext = context.createPackageContext("com.centerm.cpay.gtms", Context.CONTEXT_IGNORE_SECURITY);
            SharedPreferences sharedPreferences = otherAppContext.getSharedPreferences("OtherCommonParams", Context.MODE_WORLD_READABLE | Context.MODE_MULTI_PROCESS);
            String ip = sharedPreferences.getString("shopIp", "");
            String port = sharedPreferences.getString("shopPort", "");
            if (!TextUtils.isEmpty(ip)) {
                return "http://" + ip + ":" + port;
            }
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static String getNetAddress(Context context) {
//        Log.w("Config前", System.currentTimeMillis() + "");
        String netAddress = getNetAddressFromGtms(context);
        Log.w("获取到GTMS的地址", netAddress + "@@@@@@@@@@@@@@@@@");
//        Log.w("Config后", System.currentTimeMillis() + "");
        try {
            if (TextUtils.isEmpty(netAddress)) {
                ApplicationInfo info =
                        context.getPackageManager().getApplicationInfo(context.getPackageName(),
                                PackageManager.GET_META_DATA);
                netAddress = info.metaData.getString("NET_ADDRESS");
                Log.w("获取到的本地的ip：", netAddress);
            }
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        return netAddress;
    }

    public static String getChannelName(Context context) {
        String channelName = "";
        try {
            ApplicationInfo info =
                    context.getPackageManager().getApplicationInfo(context.getPackageName(),
                            PackageManager.GET_META_DATA);
            channelName = info.metaData.getString("CHANNEL_NAME");
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        return channelName;
    }

}
