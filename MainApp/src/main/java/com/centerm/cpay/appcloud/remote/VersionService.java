package com.centerm.cpay.appcloud.remote;

import android.content.Context;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.os.RemoteException;

import com.centerm.cloudsys.sdk.common.log.Log4d;
import com.centerm.cpay.applicationshop.net.BaseParser;
import com.centerm.cpay.applicationshop.net.DefaultHttpClient;
import com.centerm.cpay.applicationshop.net.ResponseHandler;
import com.centerm.cpay.applicationshop.net.request.GetAppDetailReq;

/**
 * author: linwanliang</br>
 * date:2016/7/2</br>
 * 提供给第三方应用的版本服务，供第三方应用在本平台查询最新版本信息
 */
public class VersionService extends IVersionInfoProvider.Stub implements IBinder {

    private Log4d logger;
    private String tag;
    private Context context;
    private String packageName;
    private IVersionInfoCallback callback;
    private static VersionService instance;
    private Handler mainHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            DefaultHttpClient client = DefaultHttpClient.getInstance();
            GetAppDetailReq req = new GetAppDetailReq(context, packageName);
            ResponseHandler handler = new ResponseHandler() {
                @Override
                public void onSuccess(String response) {
                    if (callback == null) {
                        return;
                    }
                    BaseParser parser = new BaseParser(response);
                    try {
                        if (parser.hasData()) {
                            VersionInfo info = VersionInfo.convert(parser.getBody().getJSONObject("app"));
                            callback.onSuccess(info);
                        } else {
                            callback.onError(-1, parser.getMsg());
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(int errorCode, String errorInfo) {
                    if (callback == null) {
                        return;
                    }
                    try {
                        callback.onError(errorCode, errorInfo);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            };
            client.post(context, req, handler);
        }
    };

    public static VersionService getInstance(Context context) {
        if (instance == null) {
            instance = new VersionService(context.getApplicationContext());
        }
        return instance;
    }

    private VersionService(Context context) {
        this.context = context;
        logger = Log4d.getDefaultInstance();
        tag = getClass().getSimpleName();
    }


    @Override
    public void getLatestVersion(final String pkgName, final IVersionInfoCallback callback) throws RemoteException {
        if (pkgName == null || callback == null) {
            logger.warn(tag, "请求最新版本信息失败，参数为空");
            return;
        }
        this.packageName = pkgName;
        this.callback = callback;
        mainHandler.obtainMessage().sendToTarget();
/*        QueryVersionReq request = new QueryVersionReq(context, pkgName);
        ResponseHandler handler = new ResponseHandler() {
            @Override
            public void onSuccess(String response) {
                QueryVersionParser parser = new QueryVersionParser(response);
                if (parser.hasData() && parser.getTotal() == 1) {
                    List<VersionInfo> versionInfos = parser.getVersionList();
                    logger.debug(tag, pkgName + "  最新版本信息：" + versionInfos.get(0).toString());
                    try {
                        callback.onSuccess(versionInfos.get(0));
                    } catch (RemoteException e) {
                        e.printStackTrace();
                    }
                } else {
                    try {
                        callback.onError(-1, pkgName + "  无法查询到最新版本信息");
                    } catch (RemoteException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(int errorCode, String errorInfo) {
                try {
                    callback.onError(-2, pkgName + "  查询失败，请稍候再试");
                } catch (RemoteException e) {
                    e.printStackTrace();
                }
            }
        };*/
    }

}

