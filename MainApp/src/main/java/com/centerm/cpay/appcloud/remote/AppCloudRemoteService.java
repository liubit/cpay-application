package com.centerm.cpay.appcloud.remote;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.support.annotation.Nullable;

import com.centerm.cloudsys.sdk.common.log.Log4d;

/**
 * author:wanliang527</br>
 * date:2016/10/24</br>
 */

public class AppCloudRemoteService extends Service {
    private Log4d LOGGER = Log4d.getDefaultInstance();
    private final static String TAG = AppCloudRemoteService.class.getSimpleName();

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        LOGGER.info(TAG, "服务被绑定");
        VersionService service = VersionService.getInstance(this);
        return service;
    }

    @Override
    public void onCreate() {
        LOGGER.info(TAG, "onCreate");
        super.onCreate();
    }

    @Override
    public void onDestroy() {
        LOGGER.info(TAG, "onDestroy");
        super.onDestroy();
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        LOGGER.info(TAG, "onStartCommand");
        return super.onStartCommand(intent, flags, startId);
    }

    @Override
    public void onRebind(Intent intent) {
        super.onRebind(intent);
    }

    @Override
    public boolean onUnbind(Intent intent) {
        return super.onUnbind(intent);
    }
}
