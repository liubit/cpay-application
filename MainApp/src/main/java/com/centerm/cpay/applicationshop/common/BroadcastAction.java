package com.centerm.cpay.applicationshop.common;

/**
 * Created by linwanliang on 2016/6/20.
 * 广播Aciton定义
 */
public class BroadcastAction {

    public final static String ACTION_OPEN_APPDETAIL = "com.centerm.cpay.appstore.OPEN_APP_DETAIL";
    public final static String ACTION_OPEN_MORE_APP = "com.centerm.cpay.appstore.OPEN_MORE_APP";
    public final static String ACTION_INSTALL_APP = "com.centerm.cpay.appstore.INSTALL_APP";
    public final static String ACTION_WEB_ACTIVITY = "com.centerm.cpay.appstore_WEB";


    public final static class DOWNLOAD {
        public final static String ACTION_CANCELED = "com.centerm.cpay.appstore.download.ACTION_CANCELED";
        public final static String ACTION_PAUSE = "com.centerm.cpay.appstore.download.ACTION_PAUSE";
        public final static String ACTION_START = "com.centerm.cpay.appstore.download.ACTION_DOWNLOAD_START";
        public final static String ACTION_PROGRESS = "com.centerm.cpay.appstore.download.ACTION_PROGRESS";
        public final static String ACTION_SUCCESS = "com.centerm.cpay.appstore.download.ACTION_SUCCESS";
        public final static String ACTION_FAILED = "com.centerm.cpay.appstore.download.ACTION_FAILED";
        public final static String ACTION_PAUSE_ALL = "com.centerm.cpay.appstore.download.ACTION_PAUSE_ALL";
    }

    public final static class INSTALL {
        public final static String ACTION_SUCCESS = "com.centerm.cpay.appstore.install.ACTION_INSTALL_SUCCESS";
        public final static String ACTION_FAILED = "com.centerm.cpay.appstore.install.INSTALL_ACTION_FAILED";
    }

}
