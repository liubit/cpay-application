package com.centerm.cpay.applicationshop.bean;

/**
 * Created by 王玮 on 2016/7/16.
 */
public class H5Bean {

    /**
     * action : 1是打开应用，2是打开指定应用的指定页面，3是打开网页，4是下载app，5是跳转到应用商店详情页
     * packageName : 【需要打开的应用包名】action为3非必填
     * activityName : 【指定的Activity名称】action为2必填
     * url : 【需要下载的应用地址】action为3、4必填
     */

    private Integer action;
    private String packageName;
    private String activityName;
    private String url;

    public Integer getAction() {
        return action;
    }

    public void setAction(Integer action) {
        this.action = action;
    }

    public String getPackageName() {
        return packageName;
    }

    public void setPackageName(String packageName) {
        this.packageName = packageName;
    }

    public String getActivityName() {
        return activityName;
    }

    public void setActivityName(String activityName) {
        this.activityName = activityName;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }
}
