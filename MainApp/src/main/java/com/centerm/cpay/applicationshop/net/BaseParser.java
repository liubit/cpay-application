package com.centerm.cpay.applicationshop.net;

import com.centerm.cpay.applicationshop.MyApplication;
import com.centerm.cpay.applicationshop.net.cont.JsonKey;
import com.centerm.cloudsys.sdk.common.log.Log4d;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by linwanliang on 2016/6/21.
 */
public class BaseParser {

    protected final static Log4d LOGGER = Log4d.getDefaultInstance();
    protected String TAG;
    private String response;
    private JSONObject body;
    private JSONArray bodyArray;
    private String status = "";
    private String msg = "";

    public BaseParser(String response) {
        TAG = getClass().getSimpleName();
        this.response = response;
        try {
            JSONObject json = new JSONObject(response);
            JSONObject header = json.getJSONObject(JsonKey.header);
            status = header.getString(JsonKey.status);
            msg = header.getString(JsonKey.msg);
            if (json.has(JsonKey.body) && !json.isNull(JsonKey.body)) {
                Object bodyObj = json.get(JsonKey.body);
                if (bodyObj instanceof JSONArray) {
                    bodyArray = (JSONArray) bodyObj;
                } else if (bodyObj instanceof JSONObject) {
                    body = (JSONObject) bodyObj;
                }
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public JSONObject getBody() {
        return body;
    }

    protected JSONArray getBodyArray() {
        return bodyArray;
    }

    public String getStatus() {
        return status;
    }

    public String getMsg() {
        return msg;
    }

    public boolean hasData() {
        return status.endsWith("00");
    }


}
