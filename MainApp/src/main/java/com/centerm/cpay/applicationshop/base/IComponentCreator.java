package com.centerm.cpay.applicationshop.base;

import android.view.View;

/**
 * Created by linwanliang on 2016/6/24.
 */
public interface IComponentCreator {

    /**
     * 初始化布局文件ID，如果无须设置布局文件，返回-1即可。<br/>
     * 初始化方法调用顺序为onInitLayoutId(),onInitLocalData(),onInitView()
     *
     * @return 该Activity对应的布局文件ID。
     */
    int onInitLayoutId();

    /**
     * 初始化本地数据，不包括网络数据。<br/>
     * 初始化方法调用顺序为onInitLayoutId(),onInitLocalData(),onInitView()
     */

    void onInitLocalData();

    /**
     * 初始化视图，例如查找控件ID，设置监听器等等操作。<br/>
     * 初始化方法调用顺序为onInitLayoutId(),onInitLocalData(),onInitView()
     */
    void onInitView(View view);

    /**
     * 初始化视图之后，开始加载网络数据
     */
    void doThingsAfterInitView();

}
