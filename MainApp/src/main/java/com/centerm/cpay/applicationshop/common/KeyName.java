package com.centerm.cpay.applicationshop.common;

/**
 * Created by linwanliang on 2016/6/20.
 */
public class KeyName {

    public final static String PACKAGE_NAME = "PACKAGE_NAME";
    public final static String PACKAGE_URL = "PACKAGE_URL";
    public final static String DOWNLOAD_PROGRESS = "DOWNLOAD_PROGRESS";
    public final static String DOWNLOAD_SPEED = "DOWNLOAD_SPEED";
    public final static String APP_INFO = "APP_INFO";
    public final static String APP_TYPE = "APP_TYPE";
    public final static String TYPE_RESPONSE = "TYPE_RESPONSE";
    public final static String NEED_UPDATE_COUNTS = "NEED_UPDATE_COUNTS";
    public final static String APK_PATH = "APK_PATH";
//    public final static String KEY_PKG_NAME = "KEY_PKG_NAME";


}
