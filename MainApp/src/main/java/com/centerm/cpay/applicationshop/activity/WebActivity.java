package com.centerm.cpay.applicationshop.activity;

import android.os.Environment;
import android.util.Log;
import android.view.View;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.widget.ProgressBar;

import com.centerm.cpay.applicationshop.R;
import com.centerm.cpay.applicationshop.base.BaseActivity_v21;
import com.centerm.cpay.applicationshop.bean.Advertisement;
import com.centerm.cpay.applicationshop.common.DialogFactory;
import com.centerm.cpay.applicationshop.download.AdDownloadManager;
import com.centerm.cpay.applicationshop.view.MyAlertDialog;
import com.centerm.cpay.applicationshop.web.MyWebChromeClient;
import com.centerm.cpay.applicationshop.web.MyWebViewClient;
import com.centerm.cpay.applicationshop.web.WebListener;
import com.centerm.cpay.applicationshop.web.WebView4JSFuaction;

import config.Config;

/**
 * Created by 王玮 on 2016/7/27.
 */
public class WebActivity extends BaseActivity_v21 implements WebListener {
    private ProgressBar bar;
    private WebView webView;
    private Advertisement currentAd;
    private String url;

    @Override
    public int onInitLayoutId() {
        return R.layout.activity_web;
    }

    @Override
    public void onInitLocalData() {

    }

    @Override
    public void onInitView(View view) {
        webView = (WebView) findViewById(R.id.webView);
        bar = (ProgressBar) findViewById(R.id.progressBar);
        currentAd = (Advertisement) getIntent().getSerializableExtra("ad");
        if (currentAd == null) {
            url = getIntent().getStringExtra("url");
            initWebView(url);
            return;
        }
        if (currentAd.getType().equals("4")) {
            initWebView(currentAd.getFileUrl());
        } else {
            doDownloadOrZip();
        }
    }

    public void doDownloadOrZip() {
        //H5广告，先下载，在显示
        AdDownloadManager.getInstance().setDownLoadCallBack(new AdDownloadManager.DownloadCallBack() {
            @Override
            public void onStartDownLoad() {
                showLoading();
            }

            @Override
            public void onResult(String fileUrl) {
                stopLoading();
                initWebView(fileUrl);
            }

            @Override
            public void onFail(String msg) {
                DialogFactory.showAlert(context, msg, new MyAlertDialog.ButtonClickListener() {
                    @Override
                    public void onClick(View view) {
                        finish();
                    }
                });

            }
        });
        AdDownloadManager.getInstance().downloadAd(this, currentAd.getFileUrl(), currentAd.getFileMd5());
    }

    public void initWebView(String url) {
        webView.setWebViewClient(new MyWebViewClient(this));
        // 添加Js函数调用接口
        webView.addJavascriptInterface(new WebView4JSFuaction(context, webView), "jsObj");
        webView.setWebChromeClient(new MyWebChromeClient(this));
        WebSettings webSettings = webView.getSettings();
        // WebView自适应屏幕
        webSettings.setUseWideViewPort(false);
        webSettings.setLoadWithOverviewMode(true);
        webSettings.setSupportZoom(false); // 缩放开关
        // 设置此属性，仅支持双击缩放，不支持触摸缩放
        webSettings.setRenderPriority(WebSettings.RenderPriority.HIGH);// 提高渲染的优先级
        webSettings.setDatabaseEnabled(true);
        String cacheDirPath = Environment.getExternalStorageDirectory()
                .getPath() + Config.WEB.APP_CACAHE_DIRNAME;
        // 设置数据库缓存路径
        webSettings.setDatabasePath(cacheDirPath);
        // 设置 Application Caches 缓存目录
        webSettings.setAppCachePath(cacheDirPath);
        webSettings.setDomStorageEnabled(true);// 设置可以使用localStorage
        webSettings.setCacheMode(WebSettings.LOAD_NO_CACHE);// 默认使用缓存
        webSettings.setAppCacheMaxSize(8 * 1024 * 1024);// 缓存最多可以有8M
        webSettings.setAllowFileAccess(true);// 可以读取文件缓存(manifest生效)
        webSettings.setAppCacheEnabled(true);// 应用可以有缓存
        webSettings.setSaveFormData(false);
        webSettings.setJavaScriptEnabled(true);
        webView.loadUrl(url);
    }

    @Override
    public void onStartLoading() {
        bar.setVisibility(View.VISIBLE);
        bar.setProgress(0);
    }

    @Override
    public void onChangeProgress(int progress) {
        Log.i("精度", progress + "");
        bar.setProgress(progress);
    }

    @Override
    public void onEndLoading() {
        bar.setVisibility(View.GONE);
    }
}
