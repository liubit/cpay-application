package com.centerm.cpay.applicationshop.download;

import android.content.Context;
import android.util.Log;

import com.centerm.cpay.applicationshop.download.xutils.ProgressCallBack;
import com.centerm.cpay.applicationshop.download.xutils.XutilsDownloadTools;
import com.centerm.cpay.applicationshop.utils.AdUtils;
import com.centerm.cpay.applicationshop.utils.FileUtils;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.FileAsyncHttpResponseHandler;

import org.apache.http.Header;

import java.io.File;

import config.Config;


public class AdApkDownloadManager {
    private static AdApkDownloadManager instance;
    private DownloadCallBack callBack;

    public static AdApkDownloadManager getInstance() {
        if (instance == null) {
            instance = new AdApkDownloadManager();
        }
        return instance;
    }

    private AdApkDownloadManager() {

    }

    public void setDownLoadCallBack(DownloadCallBack callBack) {
        this.callBack = callBack;
    }

    public boolean downloadApk(Context context, String url) {
        if (context == null || url == null || url.trim().isEmpty()) {
            callBack.onFail("参数为空！");
            return false;
        }
        long sdFreeSize = FileUtils.getFreeDiskSpace();
        if (sdFreeSize == -1) {
            callBack.onFail("没有挂载SD卡！");
            return false;
        } else if (sdFreeSize < Config.FILE_DOWNLOADER.MIN_FREE_SIZE) {
            callBack.onFail("SD卡空间不足！");
            return false;
        }
        String fileName = DownloadTools.extractFileNameFromUrl(url);
        if (fileName == null) {
            callBack.onFail("解析文件名失败！");
            return false;
        }
        // 不支持单文件多线程下载
        final File file = DownloadTools.getAdFile(url);
        if (file.exists()) {
            return false;
        } else {
            new XutilsDownloadTools().download(url, new ProgressCallBack() {
                @Override
                public void onStarted() {
                    callBack.onStartDownLoad();//表示开始下载
                }

                @Override
                public void onProgress(int progress, boolean isDownloading) {
                    callBack.onProgress(progress + "%");
                }

                @Override
                public void onSuccess(File result) {
                    callBack.onResult(file.getAbsolutePath());//下载完毕
                }

                @Override
                public void onError(Throwable ex, boolean isOnCallback) {
                    callBack.onFail("下载失败");
                }
            });
            return true;
        }
    }


    public interface DownloadCallBack {
        public void onStartDownLoad();

        public void onResult(String fileUrl);

        public void onFail(String msg);

        public void onProgress(String progress);
    }

}
