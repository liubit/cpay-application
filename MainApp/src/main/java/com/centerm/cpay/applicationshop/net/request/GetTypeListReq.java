package com.centerm.cpay.applicationshop.net.request;

import android.content.Context;

import com.centerm.cpay.applicationshop.net.BaseRequest;
import com.centerm.cpay.applicationshop.net.cont.Method;

import org.json.JSONException;
import org.json.JSONObject;

import config.Config;

import static com.centerm.cpay.applicationshop.net.cont.JsonKey.screenType;

/**
 * Created by linwanliang on 2016/6/22.
 */
public class GetTypeListReq extends BaseRequest {
    public GetTypeListReq(Context context) {
        super(context);
        setMethod(Method.GET_APP_TYPE_LIST);
        JSONObject body = new JSONObject();
        try {
            body.put(screenType, Config.SCREEN_TYPE);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        addBody(body);
    }
}
