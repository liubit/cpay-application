package com.centerm.cpay.applicationshop.service;

import android.app.Service;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.support.annotation.Nullable;

import com.centerm.cpay.appcloud.remote.VersionInfo;
import com.centerm.cpay.appcloud.remote.VersionService;
import com.centerm.cpay.applicationshop.common.AppSettings;
import com.centerm.cpay.applicationshop.common.KeyName;
import com.centerm.cpay.applicationshop.net.DefaultHttpClient;
import com.centerm.cpay.applicationshop.net.ResponseHandler;
import com.centerm.cpay.applicationshop.net.request.QueryVersionReq;
import com.centerm.cpay.applicationshop.net.response.QueryVersionParser;
import com.centerm.cpay.applicationshop.utils.PackageUtils;
import com.centerm.cloudsys.sdk.common.log.Log4d;

import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import config.Config;

/**
 * author: linwanliang</br>
 * date:2016/7/2</br>
 */
public class CheckUpdateService extends Service {

    private Log4d logger = Log4d.getDefaultInstance();
    private String tag = getClass().getSimpleName();
    private Timer timer;
    private TimerTask checkTask;
    private Handler handler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            if (msg != null && msg.obj != null) {
                String pkgNames = (String) msg.obj;
                requestQueryVersion(pkgNames, responseHandler);
            }
        }
    };

    private ResponseHandler responseHandler = new ResponseHandler() {
        @Override
        public void onSuccess(String response) {
            QueryVersionParser parser = new QueryVersionParser(response);
            if (parser.hasData()) {
                List<VersionInfo> versions = parser.getVersionList();
                int needUpdate = 0;
                for (VersionInfo items : versions) {
                    String pkgName = items.getPkgName();
                    int localV = PackageUtils.getInstalledVersionCode(CheckUpdateService.this, pkgName);
                    int latestV = items.getVersionCode();
                    logger.debug(tag, pkgName + "  本地版本：" + localV + "  最新版本：" + latestV);
                    if (latestV > localV) {
                        needUpdate++;
                    }
                }
                logger.info(tag, "需要更新的应用个数：" + needUpdate);
                if (needUpdate > 0) {
                    //发送广播给Launcher
                    Intent intent = new Intent(Config.ACTION_UPDATE_TIPS);
                    intent.putExtra(KeyName.NEED_UPDATE_COUNTS, needUpdate);
                    intent.putExtra(KeyName.PACKAGE_NAME, getPackageName());
                    intent.setPackage("com.centerm.cpay.launcher");
                    sendBroadcast(intent);
                }
            }
        }

        @Override
        public void onFailure(int errorCode, String errorInfo) {
            logger.warn(tag, "查询需要更新的应用个数失败，" + errorInfo);
        }
    };


    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        logger.info(tag, "CheckUpdateService is onBind");
        return VersionService.getInstance(this);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        logger.info(tag, "CheckUpdateService is onDestroy");
        if (checkTask != null) {
            checkTask.cancel();
            checkTask = null;
        }
        if (timer != null) {
            timer.cancel();
            timer = null;
        }
    }

    @Override
    public void onCreate() {
        super.onCreate();
        logger.info(tag, "CheckUpdateService is onCreate");
        timer = new Timer();
        checkTask = new TimerTask() {
            @Override
            public void run() {
                new Thread(new CheckThread()).start();
            }
        };
        timer.schedule(checkTask, 10 * 1000, Config.QUERY_APP_VERSION_INTERVAL);
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        logger.info(tag, "CheckUpdateService is onStartCommand");
        return super.onStartCommand(intent, flags, startId);
    }

    @Override
    public boolean onUnbind(Intent intent) {
        logger.info(tag, "CheckUpdateService is onUnbind");
        return super.onUnbind(intent);
    }

    @Override
    public void onRebind(Intent intent) {
        logger.info(tag, "CheckUpdateService is onRebind");
        super.onRebind(intent);
    }

    private void requestQueryVersion(String pkgNames, ResponseHandler handler) {
        DefaultHttpClient client = DefaultHttpClient.getInstance();
        QueryVersionReq request = new QueryVersionReq(this, pkgNames);
        client.post(this, request, handler);
    }


    private class CheckThread implements Runnable {
        @Override
        public void run() {
            if (!AppSettings.isAutoRemind(CheckUpdateService.this)) {
                logger.info(tag, "不进行应用版本信息的自动检测");
                return;
            }
            logger.info(tag, "查询当前应用的版本信息...");
            List<PackageInfo> apps = PackageUtils.getNonSystemAppList(CheckUpdateService.this);
            StringBuilder sBuilder = new StringBuilder();
            if (apps != null) {
                int size = apps.size();
                for (int i = 0; i < size; i++) {
                    PackageInfo info = apps.get(i);
                    sBuilder.append(info.packageName);
                    if (i != size - 1) {
                        sBuilder.append("|");
                    }
                }
            }
            logger.verbose(tag, "本地应用列表：" + sBuilder.toString());
            Message msg = new Message();
            msg.obj = sBuilder.toString();
            if (handler != null) {
                handler.sendMessage(msg);
            }
        }
    }
}
