package com.centerm.cpay.applicationshop.net.cont;

/**
 * Created by linwanliang on 2016/6/21.
 */
public class JsonKey {
    //公共报文头
    public final static String header = "header";
    public final static String version = "version";
    public final static String devMask = "devMask";
    public final static String timestamp = "timestamp";
    public final static String token = "token";
    public final static String status = "status";
    public final static String msg = "msg";
    public final static String body = "body";
    public final static String imei = "imei";
    public final static String netMark = "netMark";
    public final static String MAC = "MAC";

    //具体业务参数
    public final static String screenType = "screenType";//1竖屏，2横屏，3兼容
    public final static String apkTypeId = "apkTypeId";
    public final static String apkTypeName = "apkTypeName";
    public final static String iconUrl = "iconUrl";
    public final static String apps = "apps";
    public final static String app = "app";
    public final static String packageName = "packageName";
    public final static String name = "name";
    public final static String rate = "rate";
    public final static String downloadCount = "downloadCount";
    public final static String downloadUrl = "downloadUrl";
    public final static String versionName = "versionName";
    public final static String appType = "appType";//应用所属名称
    public final static String pageSize = "pageSize";
    public final static String pageIndex = "pageIndex";
    public final static String picUrl = "picUrl";
    public final static String type = "type";
    public final static String fileUrl = "fileUrl";
    public final static String appCode = "appCode";
    public final static String publicTime = "publicTime";
    public final static String typeName = "typeName";
    public final static String keyword = "keyword";
    public final static String words = "words";
    public final static String apkListStr = "apkListStr";
    public final static String checkResult = "checkResult";
    public final static String appMd5 = "appMd5";
    public final static String adMd5 = "adMd5";


    public final static String items = "items";
    public final static String totalNumber = "totalNumber";
    public final static String size = "size";
    public final static String types = "types";
    public final static String description = "description";
    public final static String updateInfo = "updateInfo";
    public final static String developer = "developer";
    public final static String upTime = "upTime";
    public final static String previewImg = "previewImg";
//    public final static String types = "types";


}
