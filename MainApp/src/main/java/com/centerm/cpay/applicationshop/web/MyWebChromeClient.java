package com.centerm.cpay.applicationshop.web;

import android.webkit.WebChromeClient;
import android.webkit.WebView;

/**
 * Created by 王玮 on 2016/7/27.
 */
public class MyWebChromeClient extends WebChromeClient {
    WebListener listener;

    public MyWebChromeClient(WebListener listener) {
        this.listener = listener;
    }

    @Override
    public void onProgressChanged(WebView view, int newProgress) {
        listener.onChangeProgress(newProgress);
        if (newProgress == 100) {
            listener.onEndLoading();
        }
    }
}
