package com.centerm.cpay.applicationshop.utils;


import android.content.Context;
import android.content.res.AssetManager;
import android.os.Environment;

import com.centerm.cpay.applicationshop.download.DownloadTools;
import com.centerm.cloudsys.sdk.common.log.Log4d;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;

import config.Config;

public class FileUtils extends com.centerm.cloudsys.sdk.common.utils.FileUtils {

    public static String getApkFilePath(String url) {
        String path = Config.FILE_DOWNLOADER.APK_PATH;
        String fileName = DownloadTools.extractFileNameFromUrl(url);
        return getSDCardRootPath() + path + fileName;
    }

    public static void test(Context context) {
        Log4d logger = Log4d.getDefaultInstance();
        String tag = "lwl";
//        logger.info(tag, "Environment_getDataDirectory:" + Environment.getDataDirectory().toString());
//        logger.info(tag, "Environment_getDownloadCacheDirectory:" + Environment.getDownloadCacheDirectory().toString());
//        logger.info(tag, "Environment_getExternalStorageDirectory:" + Environment.getExternalStorageDirectory().toString());
//        logger.info(tag, "context.getCacheDir:" + context.getCacheDir());
//        logger.info(tag, "context.getExternalCacheDir:" + context.getExternalCacheDir());
//        logger.info(tag, "context.getFilesDir:" + context.getFilesDir());

       /* writeNetCache(context, "lwl", "我曹高速的方是否违法是否接收到客户粉色人偶");
        String cache = readNetCache(context, "lwl");
        logger.info(tag, "读取到的缓存：" + cache);*/
    }

 /*   public static void writeNetCache(Context context, String fileName, String content) {
        long now = System.currentTimeMillis();
        String dir = getCacheDir(context);
        File file = new File(dir + File.separator + fileName);
        FileWriter writer = null;
        BufferedWriter bufW = null;
        try {
            writer = new FileWriter(file);
            bufW = new BufferedWriter(writer);
            bufW.write("time:" + now);
            bufW.newLine();
            bufW.write(content);
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (bufW != null) {
                try {
                    bufW.close();
                } catch (IOException e) {
                    e.printStackTrace();
                } finally {
                    if (writer != null) {
                        try {
                            writer.close();
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                }
            }
        }
    }

    public static String readNetCache(Context context, String fileName) {
        String dir = getCacheDir(context);
        File file = new File(dir + File.separator + fileName);
        FileReader reader = null;
        BufferedReader bufR = null;
        StringBuilder sBuilder = new StringBuilder();
        try {
            reader = new FileReader(file);
            bufR = new BufferedReader(reader);
            String line = null;
            while ((line = bufR.readLine()) != null) {
                if (line.startsWith("time:"))
                    continue;
                sBuilder.append(line);
            }
        } catch (IOException e) {
            e.printStackTrace();
            sBuilder = null;
        } finally {
            if (bufR != null) {
                try {
                    bufR.close();
                } catch (IOException e) {
                    e.printStackTrace();
                } finally {
                    if (reader != null) {
                        try {
                            reader.close();
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                }
            }
        }
        return sBuilder == null ? null : sBuilder.toString();
    }*/

    public static String readAssetsText(AssetManager manager, String fileName) {
        StringBuilder sBuilder = new StringBuilder();
        try {
            InputStream is = manager.open(fileName);
            byte[] buffer = new byte[5 * 1024];
            int len = 0;
            while ((len = is.read(buffer)) != -1) {
                byte[] temp = new byte[len];
                System.arraycopy(buffer, 0, temp, 0, len);
                sBuilder.append(new String(temp));
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return sBuilder.toString();
    }

/*    public static String getDirSize(String filePath) {
        File file = new File(filePath);
        long size = 0;
        if (file.exists()) {
            if (file.isDirectory()) {
                File[] childs = file.listFiles();
                for (File f : childs) {
                    size += getDirSize(f);
                }
            } else {
                size = file.length();
            }
        }
        return formatSize(size);
    }*/


    public static String readFile(Context context, String fileName) {
        File file = new File(fileName);
        FileReader reader = null;
        BufferedReader bufR = null;
        StringBuilder sBuilder = new StringBuilder();
        try {
            reader = new FileReader(file);
            bufR = new BufferedReader(reader);
            String line = null;
            while ((line = bufR.readLine()) != null) {
                if (line.startsWith("time:"))
                    continue;
                sBuilder.append(line);
            }
        } catch (IOException e) {
            e.printStackTrace();
            sBuilder = null;
        } finally {
            if (bufR != null) {
                try {
                    bufR.close();
                } catch (IOException e) {
                    e.printStackTrace();
                } finally {
                    if (reader != null) {
                        try {
                            reader.close();
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                }
            }
        }
        return sBuilder == null ? null : sBuilder.toString();
    }
}
