package com.centerm.cpay.applicationshop.net;

import android.content.Context;
import android.os.RemoteException;

import com.centerm.cpay.applicationshop.MyApplication;
import com.centerm.cpay.applicationshop.common.AppSettings;
import com.centerm.cpay.applicationshop.net.cont.JsonKey;
import com.centerm.cpay.applicationshop.utils.CommonUtils;
import com.centerm.cloudsys.sdk.common.utils.NetUtils;
import com.centerm.cpay.securitysuite.aidl.IVirtualPinPad;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Random;

import config.Config;

/**
 * Created by linwanliang on 2016/6/21.
 */
public class BaseRequest {

    private JSONObject params;
    private JSONObject body;
    private JSONObject header;
    private SimpleDateFormat dateFormatter;
    private String method;
    private Context appContext;

    public BaseRequest(Context context) {
        params = new JSONObject();
        addBody(new JSONObject());
        try {
            getBody().put(JsonKey.screenType, Config.SCREEN_TYPE);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        dateFormatter = new SimpleDateFormat("yyyyMMddHHmmss");
        appContext = context.getApplicationContext();
    }


    public String getMethod() {
        return method;
    }

    public void setMethod(String method) {
        this.method = method;
    }

    private JSONObject createHeader() {
        JSONObject header = new JSONObject();
        try {
            header.put(JsonKey.version, Config.NET.PROTOCOL_VERSION);
            header.put(JsonKey.devMask, getDevMask());
            header.put(JsonKey.imei, getImei());
            header.put(JsonKey.netMark, getNetMark());
            header.put(JsonKey.timestamp, getTimestamp());
            header.put(JsonKey.token, getToken());
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return header;
    }

    private String getDevMask() {
        return AppSettings.getDevMask(appContext);
    }

    /**
     * 返回终端IMEI
     *
     * @return
     */
    private String getImei() {
        return CommonUtils.getIMEI(appContext);
    }

    /**
     * 获取网络标识码
     *
     * @return 移动网络情况下返回IMSI，Wifi情况下返回mac地址，否则返回null
     */
    private String getNetMark() {
        NetUtils.NetType type = NetUtils.getNetType(appContext);
        if (NetUtils.NetType.WIFI.equals(type)) {
            return CommonUtils.getMacAddress(appContext);
        } else {
            return CommonUtils.getIMSI(appContext);
        }
    }

    private String getTimestamp() {
        return dateFormatter.format(new Date());
    }

    private String getToken() {
        String smstext = "";
        for (int i = 0; i < 6; i++) {
            smstext = smstext + getRandom(9);
        }
        return smstext;
    }

    /**
     * 获取0-max的随机数
     *
     * @param max 最大值
     * @return
     */
    private static String getRandom(int max) {
        Random random = new Random();
        int a = random.nextInt(max + 1);
        return a + "";
    }

    private String getMAC() {
        IVirtualPinPad service = MyApplication.getMacService(appContext);
        if (service != null) {
            try {
                return service.calculateMac(Config.MAC_KEY_INDEX, params.toString());
            } catch (RemoteException e) {
                e.printStackTrace();
            }
        }
        return "null";
    }

    public JSONObject getParams() {
        try {
            params.put(JsonKey.header, createHeader());
            params.put(JsonKey.MAC, getMAC());
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return params;
    }


    public void addBody(JSONObject body) {
        this.body = body;
        try {
            params.put(JsonKey.body, body);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public JSONObject getBody() {
        return body;
    }

    /*   public static void test() {
        BaseRequest request = new BaseRequest();
        JSONObject body = new JSONObject();
        try {
            body.put("params1", "1111");
            body.put("params2", "2222");
            body.put("params3", "3333");
            body.put("params4", "4444");
        } catch (JSONException e) {
            e.printStackTrace();
        }
        request.addBody(body);
        Log4d.getDefaultInstance().warn("test", request.getParams().toString());
    }*/

}
