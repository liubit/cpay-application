package com.centerm.cpay.applicationshop.bean;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

/**
 * Created by linwanliang on 2016/6/26.
 */
@DatabaseTable(tableName = "tb_net_cache")
public class CacheData {

    @DatabaseField(id = true)
    private String id;
    @DatabaseField
    private long timeStamp;
    @DatabaseField
    private String content;

    public CacheData() {
        this.timeStamp = System.currentTimeMillis();
    }

    public CacheData(String id, String content) {
        this.id = id;
        this.content = content;
        this.timeStamp = System.currentTimeMillis();
    }

    public String getContent() {
        return content;
    }

    public String getId() {
        return id;
    }

    public long getTimeStamp() {
        return timeStamp;
    }

    public void setContent(String content) {
        this.content = content;
        this.timeStamp = System.currentTimeMillis();
    }

}
