package com.centerm.cpay.applicationshop.activity;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.centerm.cpay.applicationshop.R;
import com.centerm.cpay.applicationshop.base.BaseActivity_v21;
import com.centerm.cpay.applicationshop.bean.AppInfo;
import com.centerm.cpay.applicationshop.net.ResponseHandler;
import com.centerm.cpay.applicationshop.net.request.UninstallVerifyReq;
import com.centerm.cpay.applicationshop.net.response.UninstallVerifyParser;
import com.centerm.cpay.applicationshop.utils.FileUtils;
import com.centerm.cpay.applicationshop.utils.PackageUtils;
import com.centerm.cpay.applicationshop.utils.ViewUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import config.Config;

/**
 * @author linwanliang
 * @date 2016-06-27
 */
public class AppManageActivity extends BaseActivity_v21 implements View.OnClickListener {

    private MyReceiver receiver;
    private ListView listView;
    private MyAdapter listAdapter;
    private Set<String> uninstallSets;
    private Runnable runnable;//最好申明一下

    @Override
    public int onInitLayoutId() {
        return R.layout.activity_app_manage;
    }

    @Override
    public void onInitLocalData() {
    }

    @Override
    public void onInitView(View view) {
        initUsualToolbar(R.string.title_app_manage);
        updateMomoryProgress();
        listView = (ListView) findViewById(R.id.list_v);
        showLoading();
        refreshListView(listView);
    }

    @Override
    public void doThingsAfterInitView() {
        registerBroadcastReceiver();
    }

    private void refreshListView(final ListView listView) {
        runnable = new Runnable() {
            @Override
            public void run() {
                PackageUtils.obtainNonSystemAppList(context, new PackageUtils.ObtainAppInfoCallback() {
                    @Override
                    public void onResult(List<AppInfo> appList) {
                        if (listAdapter == null) {
                            listAdapter = new MyAdapter(appList, context);
                        } else {
                            listAdapter.notifyDataSetChanged(appList);
                        }
                        if (uninstallSets == null) {
                            requestUninstallVerify(appList);
                        }
                    }
                });
            }
        };
        listView.postDelayed(runnable, Config.DELAY_LOAD_DATA);
    }

    private String formatMemoryShow(long used, long free) {
        String labelUsed = getResources().getString(R.string.label_used);
        String labelRemain = getResources().getString(R.string.label_remain);
        return labelUsed+" " + FileUtils.formatSize(used) + "/"+labelRemain +" "+ FileUtils.formatSize(free);
    }

    private void updateMomoryProgress() {
        long usedSpace = FileUtils.getUsedDiskSpace();
        long freeSpace = FileUtils.getFreeDiskSpace();
        TextView memoryShow = (TextView) findViewById(R.id.memory_show);
        if (memoryShow != null) {
            memoryShow.setText(formatMemoryShow(usedSpace, freeSpace));
        }
        int progress = (int) (usedSpace * 100 / (usedSpace + freeSpace));
        ProgressBar progressBar = (ProgressBar) findViewById(R.id.memory_progress);
        if (progressBar != null) {
            progressBar.setProgress(progress);
            if (progress >= 80) {
                progressBar.setProgressDrawable(getResources().getDrawable(R.drawable.progress_drawable_wraning));
            } else {
                progressBar.setProgressDrawable(getResources().getDrawable(R.drawable.progress_drawable));
            }
        }
    }

    /**
     * 注册广播接收器
     */
    private void registerBroadcastReceiver() {
        IntentFilter filter = new IntentFilter();
        filter.addAction(Intent.ACTION_PACKAGE_REMOVED);
//        filter.addAction(Intent.ACTION_PACKAGE_ADDED);
//        filter.addAction(Intent.ACTION_PACKAGE_REPLACED);
        //必须加上这句，否则接收不到广播
        filter.addDataScheme("package");
        receiver = new MyReceiver();
        context.registerReceiver(receiver, filter);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.operate_btn:
                Toast.makeText(context, R.string.label_uninstall, Toast.LENGTH_SHORT).show();
                break;
        }
    }

    @Override
    protected void onDestroy() {
        if (listView != null && runnable != null) {
            listView.removeCallbacks(runnable);
        }
        super.onDestroy();
        unregisterReceiver(receiver);
    }

    private void requestUninstallVerify(final List<AppInfo> apps) {
        UninstallVerifyReq req = new UninstallVerifyReq(context);
        req.addVeriryList(apps);
        ResponseHandler handler = new ResponseHandler() {
            @Override
            public void onSuccess(String response) {
                stopLoading();
                UninstallVerifyParser parser = new UninstallVerifyParser(apps, response);
                if (parser.hasData()) {
                    uninstallSets = parser.getUninstallSets();
                }
                listView.setAdapter(listAdapter);
            }

            @Override
            public void onFailure(int errorCode, String errorInfo) {
                stopLoading();
                listView.setAdapter(listAdapter);
            }
        };
        getHttpClient().post(context, req, handler);
    }


    private class MyAdapter extends BaseAdapter {

        private Context context;
        private List<AppInfo> appList;

        public MyAdapter(List<AppInfo> appList, Context context) {
            this.appList = appList;
            this.context = context;
        }

        @Override
        public int getCount() {
            return appList.size();
        }

        @Override
        public Object getItem(int position) {
            return appList.get(position);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            AppInfo data = appList.get(position);
            if (convertView == null) {
                LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                convertView = inflater.inflate(R.layout.manage_app_list_item, null);
                new ViewBinder(convertView, data);
            }
            ((ViewBinder) convertView.getTag()).reset(data);
            return convertView;
        }

        public void notifyDataSetChanged(List<AppInfo> apps) {
            if (this.appList == null) {
                appList = apps;
            } else {
                appList.clear();
                appList.addAll(apps);
            }
            super.notifyDataSetChanged();
        }
    }


    private class MyReceiver extends BroadcastReceiver {

        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            if (action.equals(Intent.ACTION_PACKAGE_REMOVED)) {
                ListView listView = (ListView) findViewById(R.id.list_v);
                refreshListView(listView);
                updateMomoryProgress();
            }
        }
    }

    private class ViewBinder {
        private AppInfo data;
        private CheckBox checkBox;
        private ImageView appIcon;
        private TextView appName;
        private TextView appExtras;
        private TextView uninstallBtn;

        private View.OnClickListener uninstallClick = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (v.getTag() instanceof AppInfo) {
                    AppInfo data = (AppInfo) v.getTag();
                    if (uninstallSets == null) {
                        requestUninstall(data);
                    } else {
                        PackageUtils.unintallApp(v.getContext(), data.getPackageName());
                    }
                } else {
                    logger.warn("AppManageActivity", "该按钮携带的数据集为空");
                }
            }
        };

        private ViewBinder(View itemView, AppInfo data) {
            this.data = data;
            checkBox = (CheckBox) itemView.findViewById(R.id.check_box);
            appIcon = (ImageView) itemView.findViewById(R.id.app_icon_show);
            appName = (TextView) itemView.findViewById(R.id.app_name_show);
            appExtras = (TextView) itemView.findViewById(R.id.app_extras_show);
            uninstallBtn = (TextView) itemView.findViewById(R.id.uninstall_btn);
            itemView.setTag(this);
        }

//        public static ViewBinder bind(View itemView, AppInfo data) {
//            ViewBinder binder = new ViewBinder(itemView, data);
//            itemView.setTag(binder);
//            return binder;
//        }

        public void reset(AppInfo data) {
            this.data = data;
            reset();
        }

        public void reset() {
            if (data != null) {
                checkBox.setVisibility(View.GONE);
                appIcon.setImageDrawable(data.getIconDrawable());
                appName.setText(data.getName());
                appExtras.setText(data.getVersionName() + "  |  " + FileUtils.formatSize(data.getSize()));
                uninstallBtn.setTag(data);
                uninstallBtn.setOnClickListener(uninstallClick);
                if (uninstallSets != null && uninstallSets.contains(data.getPackageName())) {
                    uninstallBtn.setEnabled(false);
                    uninstallBtn.setBackgroundResource(R.drawable.btn_bg_disable);
                    uninstallBtn.setTextColor(getResources().getColor(R.color.common_disabled));
                } else {
                    uninstallBtn.setEnabled(true);
                    uninstallBtn.setBackgroundResource(R.drawable.selector_btn_bg);
                    uninstallBtn.setTextColor(getResources().getColor(R.color.selector_text_color1));
                }
            }
        }

        private void requestUninstall(final AppInfo app) {
            showLoading();
            UninstallVerifyReq req = new UninstallVerifyReq(context);
            final List<AppInfo> list = new ArrayList<>();
            list.add(app);
            req.addVeriryList(list);
            ResponseHandler handler = new ResponseHandler() {
                @Override
                public void onSuccess(String response) {
                    stopLoading();
                    UninstallVerifyParser parser = new UninstallVerifyParser(list, response);
                    if (parser.hasData() && parser.canInstall(app.getPackageName())) {
                        PackageUtils.unintallApp(uninstallBtn.getContext(), data.getPackageName());
                    } else {
                        ViewUtils.showToast(context,R.string.tips_uninstall_auth_failed);
                        uninstallBtn.setBackgroundResource(R.drawable.btn_bg_disable);
                        uninstallBtn.setTextColor(getResources().getColor(R.color.common_disabled));
                        uninstallBtn.setEnabled(false);
                    }
                }

                @Override
                public void onFailure(int errorCode, String errorInfo) {
                    stopLoading();
                    ViewUtils.showToast(context,R.string.tips_uninstall_auth_failed_retry);
                    /*uninstallBtn.setBackgroundResource(R.drawable.btn_bg_disable);
                    uninstallBtn.setTextColor(getResources().getColor(R.color.common_disabled));
                    uninstallBtn.setEnabled(false);*/
                }
            };
            getHttpClient().post(context, req, handler);
        }

        public void setCheckBoxEnabled(boolean enabled) {
            if (enabled) {
                checkBox.setVisibility(View.VISIBLE);
            } else {
                checkBox.setVisibility(View.GONE);
            }
        }
    }

}
