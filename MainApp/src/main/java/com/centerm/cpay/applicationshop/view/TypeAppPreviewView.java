package com.centerm.cpay.applicationshop.view;

import android.annotation.TargetApi;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.centerm.cloudsys.sdk.common.log.Log4d;
import com.centerm.cloudsys.sdk.common.utils.MD5Utils;
import com.centerm.cpay.applicationshop.MyApplication;
import com.centerm.cpay.applicationshop.R;
import com.centerm.cpay.applicationshop.bean.AppInfo;
import com.centerm.cpay.applicationshop.bean.AppType;
import com.centerm.cpay.applicationshop.common.BroadcastAction;
import com.centerm.cpay.applicationshop.common.KeyName;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by linwanliang on 2016/6/24.
 */
public class TypeAppPreviewView extends LinearLayout implements View.OnClickListener {
    private static int DEFAULT_COLUMNS = 2;
    private Log4d logger = Log4d.getDefaultInstance();
    private String tag = getClass().getSimpleName();
    private Context context;
    private int columns = DEFAULT_COLUMNS;
    private List<AppInfo> appList = new ArrayList<AppInfo>();
    private AppType type;

    public TypeAppPreviewView(Context context) {
        super(context);
        this.context = context;
        initView();
    }

    public TypeAppPreviewView(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.context = context;
        initView();
    }

    public TypeAppPreviewView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        this.context = context;
        initView();
    }

    @TargetApi(value = Build.VERSION_CODES.LOLLIPOP)
    public TypeAppPreviewView(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        this.context = context;
        initView();
    }

    private void initView() {
        setOrientation(VERTICAL);
    }

    public void updateItems(AppType type, List<AppInfo> apps) {
        this.type = type;
        setOrientation(VERTICAL);
        appList.clear();
        appList.addAll(apps);
        removeAllViews();
        int len = appList.size();
        if (len > 0) {
            AppInfo more = new AppInfo();
            more.setName("更多");
            more.setPackageName(getMoreFlag());
            appList.add(more);
            int rows = ++len / columns;
            if (len % columns != 0) {
                rows++;
            }
            if (rows == 0) {
                setOrientation(HORIZONTAL);
                for (int i = 0; i < len; i++) {
                    addView(createItem(appList.get(i)));
                }
            } else {
                for (int i = 0; i < rows; i++) {
                    LinearLayout lineLayout = null;
                    if (i == rows - 1) {
                        lineLayout = createLine(appList.subList(i * DEFAULT_COLUMNS, appList.size()));
                    } else {
                        lineLayout = createLine(appList.subList(i * DEFAULT_COLUMNS, (i + 1) * DEFAULT_COLUMNS));
                    }
                    LayoutParams lineParams = new LayoutParams(-1, 0);
                    lineParams.weight = 1;
                    addView(lineLayout, lineParams);
                }
                if (rows == 1) {
                    LinearLayout lineLayout = new LinearLayout(context);
                    LayoutParams lineParams = new LayoutParams(-1, 0);
                    lineParams.weight = 1;
                    addView(lineLayout, lineParams);
                }
            }
        }
    }


    private TextView createItem(AppInfo app) {
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        TextView item = (TextView) inflater.inflate(R.layout.main_type_app_item, null);
//        TextView item = new TextView(context);
//        item.setTextAppearance(context, R.style.MainSortAppName);
//        item.setSingleLine(true);
        item.setText(app.getName());
//        item.setGravity(Gravity.LEFT | Gravity.CENTER_VERTICAL);
        item.setTag(app);
        item.setOnClickListener(this);
        return item;
    }

    private LinearLayout createLine(List<AppInfo> apps) {
        LinearLayout lineLayout = new LinearLayout(context);
        lineLayout.setOrientation(LinearLayout.HORIZONTAL);
        int size = apps.size();
        for (int i = 0; i < size; i++) {
            TextView item = createItem(apps.get(i));
            lineLayout.addView(item);
            LayoutParams params = (LayoutParams) item.getLayoutParams();
            params.height = -1;
            params.width = 0;
            params.weight = 1;
        }
        return lineLayout;
    }

    private String getMoreFlag() {
        return MD5Utils.getMD5Str("moremoremore");
    }


    @Override
    public void onClick(View v) {
        AppInfo info = (AppInfo) v.getTag();
        String pkgName = info.getPackageName();
        if (getMoreFlag().equals(pkgName)) {
            Intent intent = new Intent();
            intent.setAction(BroadcastAction.ACTION_OPEN_MORE_APP);
            intent.putExtra(KeyName.APP_TYPE, type);
            context.sendBroadcast(intent);
        } else {
            Intent intent = new Intent();
            intent.setAction(BroadcastAction.ACTION_OPEN_APPDETAIL);
            intent.putExtra(KeyName.APP_INFO, info);
            context.sendBroadcast(intent);
        }

    }
}
