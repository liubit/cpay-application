package com.centerm.cpay.applicationshop.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Handler;
import android.view.View;
import android.widget.ListView;

import com.centerm.cpay.applicationshop.R;
import com.centerm.cpay.applicationshop.adapter.CommonAppListAdapter;
import com.centerm.cpay.applicationshop.asyncTask.AppAsyncTask;
import com.centerm.cpay.applicationshop.asyncTask.AppMatchListener;
import com.centerm.cpay.applicationshop.base.BaseDownloadActivity;
import com.centerm.cpay.applicationshop.bean.AppInfo;
import com.centerm.cpay.applicationshop.bean.AppType;
import com.centerm.cpay.applicationshop.common.KeyName;
import com.centerm.cpay.applicationshop.net.ResponseHandler;
import com.centerm.cpay.applicationshop.net.cont.Method;
import com.centerm.cpay.applicationshop.net.request.GetAppListReq;
import com.centerm.cpay.applicationshop.net.response.AppListParser;
import com.centerm.cpay.applicationshop.utils.FileUtils;
import com.centerm.cpay.applicationshop.utils.ViewUtils;
import com.cjj.MaterialRefreshLayout;
import com.cjj.MaterialRefreshListener;

import org.json.JSONArray;

import java.util.List;

import config.Config;

/**
 * author: linwanliang</br>
 * date:2016/7/7</br>
 */
public class MoreTypeAppActivity extends BaseDownloadActivity {
    private AppType appType;
    private MaterialRefreshLayout refreshLayout;
    private ListView listView;
    private CommonAppListAdapter adapter;

    private int total = -1;
    private int pageIndex = 0;

    private String DEMO_STRING;

    @Override
    public int onInitLayoutId() {
        return R.layout.activity_more_type_app;
    }

    @Override
    public void onInitLocalData() {
        isReceivePackageChanged = true;
        appType = (AppType) getIntent().getSerializableExtra(KeyName.APP_TYPE);
        DEMO_STRING = FileUtils.readAssetsText(getAssets(), "json/" + Method.GET_APP_LIST.replace("/", "_"));
        initUsualToolbar(appType.getName());
    }

    @Override
    public void onInitView(View view) {
        refreshLayout = (MaterialRefreshLayout) findViewById(R.id.refresh_layout);
        initRefreshLayout(refreshLayout);
        listView = (ListView) findViewById(R.id.list_v);
        initListView(listView);
    }

    private void initRefreshLayout(MaterialRefreshLayout refreshLayout) {
        if (Config.DEMO) {
            refreshLayout.setLoadMore(true);
        } else {
            refreshLayout.setLoadMore(false);
        }
        refreshLayout.setMaterialRefreshListener(new MaterialRefreshListener() {
            @Override
            public void onRefresh(final MaterialRefreshLayout materialRefreshLayout) {
                if (Config.DEMO) {
                    materialRefreshLayout.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            materialRefreshLayout.finishRefresh();
                        }
                    }, 3000);
                } else {
                    requestAppList(true, false);
                }
            }

            @Override
            public void onfinish() {
            }

            @Override
            public void onRefreshLoadMore(final MaterialRefreshLayout materialRefreshLayout) {
                if (Config.DEMO) {
                    materialRefreshLayout.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            AppListParser parser = new AppListParser(context, DEMO_STRING);
                            adapter.notifyDataSetChanged(parser.getAppList(), true);
                            materialRefreshLayout.finishRefreshLoadMore();
                        }
                    }, 3000);
                } else {
                    requestAppList(false, false);
                }
            }
        });
    }

    private void initListView(ListView listView) {
        if (Config.DEMO) {
            AppListParser parser = new AppListParser(context, DEMO_STRING);
            List<AppInfo> allApps = parser.getAppList();
            if (allApps != null) {
                adapter = new CommonAppListAdapter(context, R.layout.common_app_list_item, allApps);
                listView.setAdapter(adapter);
            }
        } else {
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    requestAppList(true, true);
                }
            }, Config.DELAY_LOAD_DATA);
        }
    }

    private void requestAppList(final boolean refresh, boolean showLoading) {
        int c = adapter == null ? 0 : adapter.getCount();//当前列表数量
        int pi = this.pageIndex + 1;//页码
        if (refresh) {
            pi = 1;
        } else {
            if (c >= total) {
                return;
            }
        }
        if (showLoading) {
            showLoading();
        }
        final GetAppListReq req = new GetAppListReq(context);
        req.setApkTypeId(appType.getId());
        req.setPageIndex(pi);
        ResponseHandler handler = new ResponseHandler() {
            @Override
            public void onSuccess(String response) {
                AppListParser parser = new AppListParser(context, response);
                if (parser.hasData()) {
                    if (total == -1) {
                        total = parser.getTotal();
                    }
                    pageIndex = req.getPageIndex();
                    localMatchApp(parser.getAppsJson(), refresh);
                } else {
                    stopLoading();
                    refreshLayout.finishRefresh();
                    refreshLayout.finishRefreshLoadMore();
                    ViewUtils.showToast(context, parser.getMsg());
                }
            }

            @Override
            public void onFailure(int errorCode, String errorInfo) {
                stopLoading();
                refreshLayout.finishRefresh();
                ViewUtils.showToast(context, errorInfo);
            }
        };
        getHttpClient().post(context, req, handler);
    }

    private void localMatchApp(JSONArray appJson, final boolean refresh) {
        AppAsyncTask task = new AppAsyncTask();
        task.setAppMatchLisenter(this, new AppMatchListener() {
            @Override
            public void onStart() {

            }

            @Override
            public void onResult(List<AppInfo> apps) {
                if (adapter == null) {
                    adapter = new CommonAppListAdapter(context, R.layout.common_app_list_item, apps);
                    listView.setAdapter(adapter);
                } else {
                    adapter.notifyDataSetChanged(apps, !refresh);
                }
                int counts = adapter == null ? 0 : adapter.getCount();
                if (counts < total) {
                    refreshLayout.setLoadMore(true);
                } else {
                    refreshLayout.setLoadMore(false);
                }
                stopLoading();
                refreshLayout.finishRefresh();
                refreshLayout.finishRefreshLoadMore();
            }
        });
        task.execute(appJson);
    }

    @Override
    public void onReceiveAppStatus(Context context, Intent intent) {
        String action = intent.getAction();
        if (action.equals(Intent.ACTION_PACKAGE_ADDED)) {
            String packageName = intent.getData().getSchemeSpecificPart();
            if (adapter != null) {
                adapter.notifyPackageInstalled(packageName);
            }
            return;
        }

        if (adapter != null) {
            AppInfo app = (AppInfo) intent.getSerializableExtra(KeyName.APP_INFO);
            if (app != null) {
                adapter.notifyItemStatusChanged(app);
            }
        }
    }
}
