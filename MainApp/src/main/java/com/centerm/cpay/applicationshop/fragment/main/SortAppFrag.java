package com.centerm.cpay.applicationshop.fragment.main;

import android.content.Context;
import android.content.Intent;
import android.os.Handler;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ListView;
import android.widget.Toast;

import com.centerm.cpay.applicationshop.MyApplication;
import com.centerm.cpay.applicationshop.R;
import com.centerm.cpay.applicationshop.activity.MainActivity;
import com.centerm.cpay.applicationshop.adapter.AppTypeListAdapter;
import com.centerm.cpay.applicationshop.adapter.AppTypeListAdapterHD;
import com.centerm.cpay.applicationshop.base.BaseDownloadFragment;
import com.centerm.cpay.applicationshop.base.BaseFragment;
import com.centerm.cpay.applicationshop.bean.AppInfo;
import com.centerm.cpay.applicationshop.bean.AppType;
import com.centerm.cpay.applicationshop.bean.CacheData;
import com.centerm.cpay.applicationshop.common.KeyName;
import com.centerm.cpay.applicationshop.net.ResponseHandler;
import com.centerm.cpay.applicationshop.net.cont.JsonKey;
import com.centerm.cpay.applicationshop.net.cont.Method;
import com.centerm.cpay.applicationshop.net.request.GetTypeListReq;
import com.centerm.cpay.applicationshop.net.response.AppTypeParser;
import com.centerm.cpay.applicationshop.utils.FileUtils;
import com.centerm.cpay.applicationshop.utils.ViewUtils;
import com.cjj.MaterialRefreshLayout;
import com.cjj.MaterialRefreshListener;
import com.j256.ormlite.dao.Dao;


import java.sql.SQLException;
import java.util.List;
import java.util.Map;

import config.Config;
import test.JsonCreator;

/**
 * Created by linwanliang on 2016/6/24.
 */
public class SortAppFrag extends BaseDownloadFragment {

    private MaterialRefreshLayout refreshLayout;
    private List<AppType> typeList;
    private Map<String, List<AppInfo>> idMapApps;
    private ListView listView;
    private AppTypeListAdapter adapter;
    private AppTypeListAdapterHD adapterHD;
    public static boolean isRefresh = false;
    private String DEMO_STRING;

    @Override
    public int onInitLayoutId() {
        return R.layout.main_fragment_sort;
    }

    @Override
    public void onInitLocalData() {
        String response;
        if (Config.DEMO) {
            DEMO_STRING = FileUtils.readAssetsText(getActivity().getAssets(), "json/" + Method.GET_APP_TYPE_LIST.replace("/", "_"));
            response = DEMO_STRING;
        } else {
            response = getArguments().getString(KeyName.TYPE_RESPONSE);
        }
        if (response != null) {
            AppTypeParser parser = new AppTypeParser(response);
            typeList = parser.getTypeList();
            idMapApps = parser.getIdMapApps();
        }
    }

    @Override
    public void onInitView(View view) {
        refreshLayout = (MaterialRefreshLayout) view.findViewById(R.id.refresh_layout);
        initRefreshView(refreshLayout);
        listView = (ListView) view.findViewById(R.id.list_v);
        initList(listView);
    }

    private void initRefreshView(MaterialRefreshLayout view) {
        view.setLoadMore(false);
        view.setMaterialRefreshListener(new MaterialRefreshListener() {
            @Override
            public void onRefresh(final MaterialRefreshLayout materialRefreshLayout) {
                Log.d("===", " sort onRefresh");
                if (Config.DEMO) {
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            materialRefreshLayout.finishRefresh();
                        }
                    }, 3000);
                } else {
                    requestAppTypeList();
                }
            }

            @Override
            public void onfinish() {
            }

            @Override
            public void onRefreshLoadMore(final MaterialRefreshLayout materialRefreshLayout) {
            }
        });

        initMaterialRefreshLayout(refreshLayout);

    }

    private void initList(ListView listView) {
        if(MyApplication.is960F()){
            adapterHD = new AppTypeListAdapterHD(context, idMapApps, typeList);
            listView.setAdapter(adapterHD);
        }else {
            adapter = new AppTypeListAdapter(context, idMapApps, typeList);
            listView.setAdapter(adapter);
        }
    }

    private void refreshList(List<AppType> typeList, Map<String, List<AppInfo>> idMapApps) {
        if(MyApplication.is960F()){
            adapterHD.notifyDataSetChanged(typeList, idMapApps);
        }else {
            adapter.notifyDataSetChanged(typeList, idMapApps);
        }
    }

    private void requestAppTypeList() {
        GetTypeListReq req = new GetTypeListReq(context);
        ResponseHandler handler = new ResponseHandler() {
            @Override
            public void onSuccess(String response) {
                refreshLayout.finishRefresh();
                AppTypeParser parser = new AppTypeParser(response);
                if (parser.hasData()) {
                    refreshList(parser.getTypeList(), parser.getIdMapApps());
                } else {
                    ViewUtils.showToast(context, "刷新失败，" + parser.getMsg());
                }
            }

            @Override
            public void onFailure(int errorCode, String errorInfo) {
                refreshLayout.finishRefresh();
                ViewUtils.showToast(context, "刷新失败，" + errorInfo);
            }
        };
        getHttpClient().post(context, req, handler);
    }


    @Override
    public void onReceiveAppStatus(Context context, Intent intent) {

    }

    @Override
    public void onResume() {
        super.onResume();
        if (isRefresh){
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    requestAppTypeList();
                    isRefresh = false;
                }
            }, Config.DELAY_LOAD_DATA);
        }
    }
}
