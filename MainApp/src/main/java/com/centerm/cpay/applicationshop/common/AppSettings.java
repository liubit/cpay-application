package com.centerm.cpay.applicationshop.common;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import com.centerm.cpay.applicationshop.utils.CommonUtils;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

/**
 * Created by linwanliang on 2016/6/22.
 * 应用偏好设置
 */
public class AppSettings {

    private final static String KEY_DEV_MASK = "KEY_DEV_MASK";
    private final static String KEY_UPDATE_AUTO_REMIND = "KEY_UPDATE_AUTO_REMIND";
    private final static String KEY_SEARCH_KEYS = "KEY_SEARCH_KEYS";

    private static SharedPreferences getDefaultPres(Context context) {
        return PreferenceManager.getDefaultSharedPreferences(context);
    }

    public static String getDevMask(Context context) {
        if (context == null) {
            return null;
        }
        return getDefaultPres(context).getString(KEY_DEV_MASK, null);
    }

    public static void setDevMask(Context context, String value) {
        if (context == null) {
            return;
        }
        getDefaultPres(context.getApplicationContext()).edit().putString(KEY_DEV_MASK, CommonUtils.calculateDevMask(value)).commit();
    }

    public static boolean isAutoRemind(Context context) {
        if (context == null) {
            return true;
        }
        return getDefaultPres(context).getBoolean(KEY_UPDATE_AUTO_REMIND, true);
    }

    public static void setAutoRemind(Context context, boolean value) {
        if (context == null) {
            return;
        }
        getDefaultPres(context.getApplicationContext()).edit().putBoolean(KEY_UPDATE_AUTO_REMIND, value).commit();
    }

    public static void setSearchHistory(Context context, List<String> keyList) {
        if (context == null || keyList == null) {
            return;
        }
        String spliter = "||";
        int size = keyList.size();
        StringBuilder sBuilder = null;
        if (size > 0) {
            sBuilder = new StringBuilder();
            for (int i = 0; i < keyList.size(); i++) {
                sBuilder.append(keyList.get(i));
                if (i != keyList.size() - 1) {
                    sBuilder.append(spliter);
                }
            }
            getDefaultPres(context.getApplicationContext())
                    .edit().putString(KEY_SEARCH_KEYS, sBuilder.toString()).apply();
        } else {
            getDefaultPres(context.getApplicationContext()).edit().remove(KEY_SEARCH_KEYS).commit();
        }


    }

    public static LinkedList<String> getSearchHistory(Context context) {
        if (context == null) {
            return null;
        }
        String spliter = "\\|\\|";
        String keyStr = getDefaultPres(context).getString(KEY_SEARCH_KEYS, null);
        LinkedList<String> keyList = new LinkedList<>();
        if (keyStr != null) {
            String[] array = keyStr.split(spliter);
            Collections.addAll(keyList, array);
        }
        return keyList;
    }
}
