package com.centerm.cpay.applicationshop.download.xutils;

import org.xutils.common.Callback;
import org.xutils.common.task.PriorityExecutor;
import org.xutils.http.RequestParams;
import org.xutils.x;

/**
 * Created by 王玮 on 2016/7/4.
 */
public class XutilsDownloadTools {
    private Callback.Cancelable cancelable;

    public Callback.Cancelable download(String path, ProgressCallBack callBack) {
        RequestParams requestParams = new RequestParams(path);
        String downloadPath =
                com.centerm.cpay.applicationshop.download.DownloadTools.getAdFile(path).getAbsolutePath();
        requestParams.setAutoResume(true);
        requestParams.setSaveFilePath(downloadPath);
        cancelable = x.http().get(requestParams, callBack);
        return cancelable;
    }
}
