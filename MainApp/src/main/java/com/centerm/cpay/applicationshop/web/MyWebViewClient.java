package com.centerm.cpay.applicationshop.web;

import android.webkit.WebView;
import android.webkit.WebViewClient;

public class MyWebViewClient extends WebViewClient {
    WebListener listener;

    public MyWebViewClient(WebListener listener) {
        this.listener = listener;
    }

    @Override
    public boolean shouldOverrideUrlLoading(WebView view, String url) {
        listener.onStartLoading();
        view.loadUrl(url);
        return true;
    }


}
