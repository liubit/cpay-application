package com.centerm.cpay.applicationshop.utils;

import android.content.Context;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.telephony.TelephonyManager;
import android.util.Base64;

import com.centerm.cpay.applicationshop.MyApplication;
import com.centerm.cloudsys.sdk.common.log.Log4d;
import com.centerm.cloudsys.sdk.common.utils.NetUtils;
import com.centerm.cloudsys.sdk.common.utils.Utils;
import com.centerm.cpay.midsdk.dev.common.utils.HexUtils;

import org.apache.http.conn.util.InetAddressUtils;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.security.MessageDigest;
import java.util.Collections;
import java.util.List;

/**
 * Created by linwanliang on 2016/6/21.
 */
public class CommonUtils extends Utils {
    private final static byte[] MASK = new byte[]{(byte) 0x13, (byte) 0x43, (byte) 0x45, (byte) 0x35
            , (byte) 0x56, (byte) 0x23, (byte) 0x15, (byte) 0x35, (byte) 0x56, (byte) 0x58, (byte) 0x43,
            (byte) 0x55, (byte) 0x32, (byte) 0x45, (byte) 0x33, (byte) 0x44, (byte) 0x55, (byte) 0x35,
            (byte) 0x56, (byte) 0x23, (byte) 0x15, (byte) 0x34, (byte) 0x45, (byte) 0x35, (byte) 0x34,
            (byte) 0x66, (byte) 0x33, (byte) 0x43, (byte) 0x77, (byte) 0x18, (byte) 0x19, (byte) 0x37};

    /**
     * 获取硬件物理地址，需要声明权限android.permission.ACCESS_WIFI_STATE
     *
     * @param context
     * @return 如果没有连接上Wifi，返回null
     */
    public static String getMacAddress(Context context) {
        String mac = NetUtils.getMacAddress(context);
        if (mac != null) {
            mac = mac.toUpperCase().replace(":", "");
        }
        return "000" + mac;
    }

    /**
     * 终端掩码
     *
     * @param terminalSn
     * @return
     */
    public static String calculateDevMask(String terminalSn) {
        if (terminalSn == null) {
            return null;
        }
        byte[] snBytes = terminalSn.getBytes();
        int len = snBytes.length;

        int maxLen = MASK.length;
        byte[] result = new byte[len < maxLen ? len : maxLen];
        for (int i = 0; (i < len && i < maxLen); i++) {
            result[i] = (byte) (MASK[i] ^ snBytes[i]);
        }
        byte[] devMask = Base64.encode(result, Base64.NO_WRAP);
        return new String(devMask);
    }

    /**
     * 获取文件MD5值
     *
     * @param file     文件对象
     * @param callback 回调
     */
    public static void getMD5(final File file, final FileMd5Callback callback) {
        if (file == null || !file.exists()) {
            if (callback != null) {
                callback.gotMd5(null);
            }
            return;
        }
        new Thread(new Runnable() {
            @Override
            public void run() {

                FileInputStream fis = null;
                try {
                    MessageDigest md = MessageDigest.getInstance("MD5");
                    fis = new FileInputStream(file);
                    byte[] buffer = new byte[2048];
                    int length = -1;
                    while ((length = fis.read(buffer)) != -1) {
                        md.update(buffer, 0, length);
                    }
                    //32位加密
                    byte[] b = md.digest();
                    if (callback != null) {
                        callback.gotMd5(HexUtils.bytesToHexString(b));
                    }
                    // 16位加密
                    // return buf.toString().substring(8, 24);
                } catch (Exception ex) {
                    ex.printStackTrace();
                    if (callback != null) {
                        callback.gotMd5(null);
                    }
                } finally {
                    try {
                        fis.close();
                    } catch (IOException ex) {
                        ex.printStackTrace();
                    }
                }
            }
        }).start();
    }

    public interface FileMd5Callback {
        void gotMd5(String value);
    }

}
