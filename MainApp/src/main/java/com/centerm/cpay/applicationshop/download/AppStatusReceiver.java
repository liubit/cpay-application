package com.centerm.cpay.applicationshop.download;

import android.content.Context;
import android.content.Intent;

/**
 * Created by linwanliang on 2016/6/25.
 */
public interface AppStatusReceiver {
    void onReceiveAppStatus(Context context, Intent intent);
}
