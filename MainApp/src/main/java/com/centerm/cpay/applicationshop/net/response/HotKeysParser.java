package com.centerm.cpay.applicationshop.net.response;

import com.centerm.cpay.applicationshop.net.BaseParser;
import com.centerm.cpay.applicationshop.net.cont.JsonKey;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * author: linwanliang</br>
 * date:2016/7/12</br>
 */
public class HotKeysParser extends BaseParser {
    private List<String> hotKeyList = new ArrayList<>();

    public HotKeysParser(String response) {
        super(response);
        hotKeyList = new ArrayList<>();
        if (!hasData()) {
            return;
        }
        JSONObject body = getBody();
        if (body == null) {
            return;
        } else if (body.has(JsonKey.words)) {
            try {
                String[] words = getBody().getString(JsonKey.words).split("\\|");
                Collections.addAll(hotKeyList, words);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    public List<String> getHotKeyList() {
        return hotKeyList;
    }
}
