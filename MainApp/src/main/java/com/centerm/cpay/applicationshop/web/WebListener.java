package com.centerm.cpay.applicationshop.web;

/**
 * Created by 王玮 on 2016/7/27.
 */
public interface WebListener {
    public void onStartLoading();

    public void onChangeProgress(int progress);

    public void onEndLoading();
}
