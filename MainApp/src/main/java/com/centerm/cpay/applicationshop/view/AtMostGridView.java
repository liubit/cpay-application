package com.centerm.cpay.applicationshop.view;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.GridView;

/**
 * 消除滑动冲突的GridView
 *
 * @author wanliang527
 * @date 2014-5-6
 */
public class AtMostGridView extends GridView {

    public AtMostGridView(Context context, AttributeSet attrs,
                          int defStyle) {
        super(context, attrs, defStyle);
    }

    public AtMostGridView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public AtMostGridView(Context context) {
        super(context);
    }

    @Override
    public void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        int expandSpec = MeasureSpec.makeMeasureSpec(Integer.MAX_VALUE >> 2,
                MeasureSpec.AT_MOST);
        super.onMeasure(widthMeasureSpec, expandSpec);
    }
}
