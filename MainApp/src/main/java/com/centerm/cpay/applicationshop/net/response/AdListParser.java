package com.centerm.cpay.applicationshop.net.response;

import android.util.Log;

import com.centerm.cpay.applicationshop.bean.Advertisement;
import com.centerm.cpay.applicationshop.net.BaseParser;
import com.centerm.cpay.applicationshop.net.cont.JsonKey;
import com.centerm.cpay.applicationshop.utils.JsonUtils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * author: linwanliang</br>
 * date:2016/7/8</br>
 */
public class AdListParser extends BaseParser {
    private List<Advertisement> adList;

    public AdListParser(String response) {
        super(response);
        JSONArray array = getBodyArray();
        if (array == null) {
            return;
        }
        try {
            adList = new ArrayList<>();
            for (int i = 0; i < array.length(); i++) {
                JSONObject item = array.getJSONObject(i);
                String picUrl = item.getString(JsonKey.picUrl);
                String type = item.getString(JsonKey.type);
                String fileUrl = item.getString(JsonKey.fileUrl);
                String md5 = JsonUtils.getString(item, JsonKey.adMd5);
                Advertisement ad = new Advertisement(type, picUrl, fileUrl, md5);
                adList.add(ad);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public List<Advertisement> getAdList() {
        return adList;
    }

    public void setAdList(List<Advertisement> adList) {
        this.adList = adList;
    }
}
