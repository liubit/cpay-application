package com.centerm.cpay.applicationshop.adapter;

import android.animation.AnimatorInflater;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import com.centerm.cpay.applicationshop.R;
import com.centerm.cpay.applicationshop.activity.AppDetailActivity;
import com.centerm.cpay.applicationshop.bean.AppInfo;
import com.centerm.cpay.applicationshop.common.BroadcastAction;
import com.centerm.cpay.applicationshop.common.KeyName;
import com.centerm.cpay.applicationshop.download.ApkDownloadManager;
import com.centerm.cpay.applicationshop.utils.FileUtils;
import com.centerm.cloudsys.sdk.common.log.Log4d;
import com.centerm.cloudsys.sdk.common.utils.PackageUtils;
import com.nostra13.universalimageloader.core.ImageLoader;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import config.Config;

/**
 * @author linwanliang
 * @date 2016-06-23
 * 应用列表适配器
 */
public class MainAppGridAdapter extends BaseAdapter {
    private final static String TAG = MainAppGridAdapter.class.getSimpleName();
    private final static Log4d logger = Log4d.getDefaultInstance();
    private Context context;
    private List<AppInfo> appList;
    private int resourceId;
    private int columns = 3;
    private Map<String, View> idMapView;

    public MainAppGridAdapter(Context context, List<AppInfo> appList, int columns) {
        this.appList = appList;
        this.context = context.getApplicationContext();
        this.columns = columns;
        idMapView = new HashMap<>();
    }

    @Override
    public int getCount() {
        int counts = appList == null ? 0 : appList.size();
        return counts + calculateSpaceCounts();
    }

    @Override
    public Object getItem(int position) {
        return appList == null ? null : appList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (resourceId <= 0) {
            return null;
        }
        AppInfo data;
        if (position < appList.size()) {
            data = appList.get(position);
        } else {
            data = createSpacingItem();
        }
        String pkgName = data.getPackageName();
        idMapView.remove(pkgName);
        if (convertView == null) {
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(resourceId, null);
            ViewBinder.bind(context, convertView, data);
        }
        //更新索引字典
        idMapView.put(pkgName, convertView);
        //更新视图
        ViewBinder binder = (ViewBinder) convertView.getTag();
        binder.reset(data);
    /*    FrameLayout.LayoutParams params = (FrameLayout.LayoutParams) convertView.findViewById(R.id.item_root_view).getLayoutParams();
        int p = position % columns;
        int margin = context.getResources().getDimensionPixelSize(R.dimen.common_margin_10px);
        if (p == 0) {
            params.setMargins(margin, 0, 0, 0);
        } else if (p == 1) {
            params.setMargins(0, 0, 0, 0);
        } else if (p == 2) {
            params.setMargins(0, 0, margin, 0);
        }*/
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            if (position < appList.size()) {
                convertView.setStateListAnimator(AnimatorInflater.loadStateListAnimator(context, R.drawable.touch_raise));
            } else {
                convertView.setStateListAnimator(null);
            }
        }
        return convertView;
    }

    private AppInfo createSpacingItem() {
        AppInfo space = new AppInfo();
        space.setPackageName(null);
        return space;
    }

    private int calculateSpaceCounts() {
        if (appList == null) {
            return 0;
        }
        int size = appList.size();
        if (size % columns == 0) {
            return 0;
        } else {
            return columns - size % columns;
        }
    }

    /**
     * 获取适配器当前使用的LayoutID
     *
     * @return
     */
    public int getResourceId() {
        return resourceId;
    }

    /**
     * 设置适配器使用的LayoutID
     *
     * @param resourceId
     */
    public void setResourceId(int resourceId) {
        this.resourceId = resourceId;
    }

    public Map<String, View> getIdMapView() {
        return idMapView;
    }

    public void notifyItemStatusChanged(String pkgName, AppInfo.AppStatus status, int progress) {
        if (pkgName == null || !idMapView.containsKey(pkgName)) {
            logger.warn(TAG, "更新[" + pkgName + "]状态失败");
        } else {
            View view = idMapView.get(pkgName);
            ViewBinder binder = (ViewBinder) view.getTag();
            binder.updateStatus(status, progress);
        }
    }

    public void notifyPackageInstalled(String pkgPrefix) {
        if (appList == null) {
            return;
        }
        for (AppInfo app : appList) {
            if (app.getPackageName().startsWith(pkgPrefix)) {
                app.setStatus(AppInfo.AppStatus.CAN_OPEN);
                break;
            }
        }
    }

    public void notifyPackageUnInstalled(String pkgPrefix) {
        if (appList == null) {
            return;
        }
        for (AppInfo app : appList) {
            if (app.getPackageName().startsWith(pkgPrefix)) {
                app.setStatus(AppInfo.AppStatus.CAN_DOWNLOAD);
                break;
            }
        }
    }

    public void notifyAllPaused() {
        if (appList != null && appList.size() > 0) {
            for (AppInfo item : appList) {
                if (item.getStatus().equals(AppInfo.AppStatus.DOWNLOADING)
                        || item.getStatus().equals(AppInfo.AppStatus.WAIT_DOWNLOAD)) {
                    item.setStatus(AppInfo.AppStatus.PAUSE_DOWNLOAD);
                }
            }
        }
    }

    public void notifyDataChanged(List<AppInfo> appList, boolean append) {
        if (!append) {
            this.appList.clear();
        }
        this.appList.addAll(appList);
        notifyDataSetChanged();
    }

    public static class ViewBinder implements View.OnClickListener {
        private Context context;
        private AppInfo appInfo;
        private ImageView iconShow;
        private TextView nameShow;
        private TextView sizeShow;

        private ViewBinder(Context context, View view, AppInfo data) {
            this.context = context;
            this.appInfo = data;
            iconShow = (ImageView) view.findViewById(R.id.app_icon_show);
            nameShow = (TextView) view.findViewById(R.id.app_name_show);
            sizeShow = (TextView) view.findViewById(R.id.app_size_show);
        }

        public static ViewBinder bind(Context context, View view, AppInfo data) {
            ViewBinder binder = new ViewBinder(context, view, data);
            view.setTag(binder);
            return binder;
        }

        public void reset(AppInfo appInfo) {
            this.appInfo = appInfo;
            reset();
        }

        public void reset() {
            if (appInfo == null || appInfo.getPackageName() == null) {
                return;
            }
            if (iconShow != null) {
                ImageLoader.getInstance().displayImage(appInfo.getIconUrl(), iconShow, Config.APP_ICON_OPTIONS);
            }
            if (nameShow != null) {
                nameShow.setText(appInfo.getName());
            }
            if (sizeShow != null) {
//                sizeShow.setText(FileUtils.formatSize(appInfo.getSize()));
                sizeShow.setText(appInfo.getTypeName());
            }
            updateStatus(appInfo.getStatus(), -1);
        }

        public AppInfo getBindedData() {
            return appInfo;
        }

        public void updateStatus(AppInfo.AppStatus status, int progress) {
            appInfo.setStatus(status);
            appInfo.getDownloadExtra().setProgress(progress);
           /* appInfo.setStatus(status.ordinal());
            switch (status) {
                //该状态由按钮点击传递
                case WAIT_DOWNLOAD:
                    operateBtn.setText("等待");
                    progressBar.setVisibility(View.VISIBLE);
                    progressBar.setIndeterminate(true);
                    break;
                //该状态由初始化View时进行设置
                case CAN_DOWNLOAD:
                    operateBtn.setText("下载");
                    progressBar.setVisibility(View.INVISIBLE);
                    break;
                //该状态由下载管理器传递
                case DOWNLOADING:
                    operateBtn.setText("正在下载");
                    progressBar.setVisibility(View.VISIBLE);
                    progressBar.setProgress(progress);
                    progressBar.setIndeterminate(false);
                    break;
                //该状态由按钮点击传递
                case PAUSE_DOWNLOAD:
                    operateBtn.setText("暂停");
                    progressBar.setVisibility(View.VISIBLE);
                    progressBar.setIndeterminate(false);
                    break;
                //初始化时
                case WAIT_INSTALL:
                    operateBtn.setText("安装");
                    progressBar.setProgress(0);
                    progressBar.setVisibility(View.INVISIBLE);
                    break;
                //由下载管理器传递
                case INSTALLING:
                    operateBtn.setText("正在安装");
                    progressBar.setProgress(100);
                    progressBar.setVisibility(View.VISIBLE);
                    progressBar.setIndeterminate(true);
                    break;
                //初始化时
                case CAN_UPDATE:
                    operateBtn.setText("更新");
                    progressBar.setProgress(0);
                    progressBar.setVisibility(View.INVISIBLE);
                    break;
                //初始化时
                case CAN_OPEN:
                    operateBtn.setText("打开");
                    progressBar.setIndeterminate(false);
                    progressBar.setProgress(0);
                    progressBar.setVisibility(View.INVISIBLE);
                    break;
                case DOWNLOAD_FAILED:
                    operateBtn.setText("下载失败");
                    progressBar.setVisibility(View.INVISIBLE);
                    break;
            }*/
        }

        @Override
        public void onClick(View v) {
            switch (v.getId()) {
                case R.id.item_root_view:
                    //为了使用MD风格的页面切换
                    //发出广播，由MainActivity接收并且打开详情界面
                    Intent intent = new Intent();
                    intent.setAction(BroadcastAction.ACTION_OPEN_APPDETAIL);
                    intent.putExtra(KeyName.APP_INFO, appInfo);
                    context.sendBroadcast(intent);
                    break;
            }
        }
    }
}
