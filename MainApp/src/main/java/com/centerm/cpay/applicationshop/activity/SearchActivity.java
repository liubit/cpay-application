package com.centerm.cpay.applicationshop.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Handler;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.TextView;

import com.centerm.cloudsys.sdk.common.utils.StringUtils;
import com.centerm.cpay.applicationshop.R;
import com.centerm.cpay.applicationshop.adapter.CommonAppListAdapter;
import com.centerm.cpay.applicationshop.base.BaseDownloadActivity;
import com.centerm.cpay.applicationshop.bean.AppInfo;
import com.centerm.cpay.applicationshop.common.AppSettings;
import com.centerm.cpay.applicationshop.common.BroadcastAction;
import com.centerm.cpay.applicationshop.common.KeyName;
import com.centerm.cpay.applicationshop.common.MaterialDialogManager;
import com.centerm.cpay.applicationshop.net.ResponseHandler;
import com.centerm.cpay.applicationshop.net.request.GetHotKeysReq;
import com.centerm.cpay.applicationshop.net.request.SearchAppReq;
import com.centerm.cpay.applicationshop.net.response.HotKeysParser;
import com.centerm.cpay.applicationshop.net.response.SearchResultParser;
import com.centerm.cpay.applicationshop.utils.ViewUtils;

import org.apmem.tools.layouts.FlowLayout;

import java.util.LinkedList;
import java.util.List;

import config.Config;

/**
 * Created by linwanliang on 2016/6/22.
 */
public class SearchActivity extends BaseDownloadActivity implements View.OnClickListener {

    private LinkedList<String> historyKeyList;
    private LinkedList<String> hotKeyList = new LinkedList<>();
    private int[] textColorSet = new int[]{
            R.color.search_text_color_random1,
            R.color.search_text_color_random2,
            R.color.search_text_color_random3,
            R.color.search_text_color_random4
    };
    private int[] keyBgSet = new int[]{
            R.drawable.search_key_bg_random1,
            R.drawable.search_key_bg_random2,
            R.drawable.search_key_bg_random3,
            R.drawable.search_key_bg_random4,
    };
    private View hotKeyShow;
    private FlowLayout historyKeyFlowLayout, hotKeyFlowLayout;
    private EditText searchBox;
    private ListView listView;
    private CommonAppListAdapter adapter;


    @Override
    public int onInitLayoutId() {
        return R.layout.activity_search;
    }

    @Override
    public void onInitLocalData() {
    }

    @Override
    public void onInitView(View view) {
        Toolbar toolbar = (Toolbar) findViewById(R.id.tool_bar);
        if (toolbar != null) {
            toolbar.setNavigationIcon(R.drawable.icon_back);
        }
        setSupportActionBar(toolbar);

        ImageButton searchBtn = (ImageButton) findViewById(R.id.search_btn);
        if (searchBtn != null) {
            searchBtn.setOnClickListener(this);
        }
        TextView clearBtn = (TextView) findViewById(R.id.clear_btn);
        if (clearBtn != null) {
            clearBtn.setOnClickListener(this);
        }
        hotKeyShow = findViewById(R.id.hot_keys_view);
        historyKeyFlowLayout = (FlowLayout) findViewById(R.id.history_keys);
        hotKeyFlowLayout = (FlowLayout) findViewById(R.id.hot_keys);
        listView = (ListView) findViewById(R.id.list_v);
        searchBox = (EditText) findViewById(R.id.search_key_edit);
        initSearchBox(searchBox);
        initHistoryKeys();
        initHotKeys();
        delayShowSoftInput();
    }


    @Override
    protected void onPause() {
        super.onPause();
        AppSettings.setSearchHistory(context, historyKeyList);
    }


    private void initSearchBox(EditText box) {
        box.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                listView.setVisibility(View.GONE);
                findViewById(R.id.keys_view).setVisibility(View.VISIBLE);
            }
        });
    }


    private void initHistoryKeys() {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                historyKeyList = AppSettings.getSearchHistory(context);
                refreshHistoryKeys();
            }
        }, Config.DELAY_LOAD_DATA);
    }

    private void initHotKeys() {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                requestHotkeys();
            }
        }, Config.DELAY_LOAD_DATA);
    }


    private void addHistoryKeys(String keys) {
        if (hotKeyList.contains(keys)) {
            return;
        }
        if (!historyKeyList.contains(keys)) {
            historyKeyList.add(keys);
            historyKeyFlowLayout.addView(createKeyItemView(-1, keys), 0);
        }
    }

    private View createKeyItemView(int index, String str) {
        View view = getLayoutInflater().inflate(R.layout.search_keyword_item, null);
        TextView text = (TextView) view.findViewById(R.id.search_key_btn);
        if (index > 0) {
            int i = index % keyBgSet.length;
            int textColor = textColorSet[i];
            int bgColor = keyBgSet[i];
            text.setTextColor(getResources().getColorStateList(textColor));
            text.setBackgroundResource(bgColor);
        }
        text.setOnClickListener(this);
        text.setText(str);
        return view;
    }

    private void refreshHistoryKeys() {
        historyKeyFlowLayout.removeAllViews();
        for (int i = 0; i < historyKeyList.size(); i++) {
            historyKeyFlowLayout.addView(createKeyItemView(-1, historyKeyList.get(i)), 0);
        }
    }

    private void refreshHotKeys(List<String> keys) {
        hotKeyList.clear();
        hotKeyList.addAll(keys);
        hotKeyFlowLayout.removeAllViews();
        hotKeyShow.setVisibility(View.VISIBLE);
        for (int i = 0; i < hotKeyList.size(); i++) {
            hotKeyFlowLayout.addView(createKeyItemView(i, hotKeyList.get(i)));
        }
    }

    private void delayShowSoftInput() {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                InputMethodManager m = (InputMethodManager)
                        getSystemService(Context.INPUT_METHOD_SERVICE);
                m.toggleSoftInput(0, InputMethodManager.HIDE_NOT_ALWAYS);
            }
        }, 700);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.search_btn:
                String keywords = checkInput();
                requestSearch(keywords);
                break;
            case R.id.clear_btn:
                onClearHistory();
                break;
            case R.id.search_key_btn:
                String key = ((TextView) v).getText().toString();
                searchBox.setText(key);
                searchBox.setSelection(key.length());
                requestSearch(key);
                break;
        }
    }

    private String checkInput() {
        EditText inputBox = (EditText) findViewById(R.id.search_key_edit);
        String input = inputBox.getText().toString();
        if (StringUtils.isStrNull(input)) {
            return null;
        }
        return input;
    }

    private void requestSearch(final String keywords) {
        if (keywords == null) {
            return;
        }
        showLoading();
        SearchAppReq req = new SearchAppReq(context);
        req.setKeyword(keywords);
        ResponseHandler handler = new ResponseHandler() {
            @Override
            public void onSuccess(String response) {
                stopLoading();
                SearchResultParser parser = new SearchResultParser(context, response);
                List<AppInfo> apps = parser.getAppList();
                if (parser.hasData()) {
                    if (adapter == null) {
                        adapter = new CommonAppListAdapter(context, R.layout.common_app_list_item, apps);
                        adapter.setOperateEnabled(true);
                        listView.setAdapter(adapter);
                    }
                    adapter.notifyDataSetChanged(apps, false);
                    //搜索词加入历史搜索
                    addHistoryKeys(keywords);
                    onShowResult();
                } else {
                    ViewUtils.showToast(context, parser.getMsg());
                }
            }

            @Override
            public void onFailure(int errorCode, String errorInfo) {
                stopLoading();
                ViewUtils.showToast(context, errorInfo);
            }
        };
        getHttpClient().post(context, req, handler);
    }

    private void requestHotkeys() {
        GetHotKeysReq req = new GetHotKeysReq(context);
        ResponseHandler handler = new ResponseHandler() {
            @Override
            public void onSuccess(String response) {
                if (isFinishing()) {
                    return;
                }
                HotKeysParser parser = new HotKeysParser(response);
                if (parser.hasData()) {
                    refreshHotKeys(parser.getHotKeyList());
                } else {
                    refreshHotKeys(Config.DEFALUT_HOT_KEYWORDS);
                }
            }

            @Override
            public void onFailure(int errorCode, String errorInfo) {
                if (isFinishing()) {
                    return;
                }
                refreshHotKeys(Config.DEFALUT_HOT_KEYWORDS);
            }
        };
        getHttpClient().post(context, req, handler);
    }


    private void onShowResult() {
        listView.setVisibility(View.VISIBLE);
        findViewById(R.id.keys_view).setVisibility(View.GONE);
    }


    private void onClearHistory() {
        MaterialDialogManager.getChooser(context, R.string.tips_clear_all_search_records
                , new MaterialDialogManager.ButtonClick() {
                    @Override
                    public void onClick() {
                        historyKeyList.clear();
                        refreshHistoryKeys();
                    }
                }).show();
    }


    //    @Override
    public void onReceiveAppStatus(Context context, Intent intent) {
        String action = intent.getAction();
        if (action.equals(BroadcastAction.DOWNLOAD.ACTION_PAUSE_ALL)) {
            return;
        }
        AppInfo app = (AppInfo) intent.getSerializableExtra(KeyName.APP_INFO);
        if (adapter != null) {
            //更新显示状态
            adapter.notifyItemStatusChanged(app);
        }
    }
}
