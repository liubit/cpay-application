package com.centerm.cpay.applicationshop.utils;

import android.os.Environment;

import com.centerm.cloudsys.sdk.common.utils.MD5Utils;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Enumeration;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;

import config.Config;

/**
 * Created by linwanliang on 2016/3/7.
 */
public class AdUtils {


    /**
     * 获取ZIP文件存储目录的绝对路径
     *
     * @return
     */
    public static String getZipFileDir(String url) {
        return Environment.getExternalStorageDirectory()
                + Config.FILE_DOWNLOADER.AD_PATH + MD5Utils.getMD5Str(url);
    }


    /**
     * 获取已解压的文件夹绝对路径
     *
     * @param packageUrl
     * @return
     */
    public static String getUnzipDir(String packageUrl) {
//        return getZipFileDir() + File.separator;
        return getZipFileDir(packageUrl) + MD5Utils.getMD5Str(packageUrl);
    }

    public static void unzip(final File zipFile, String unzipPath, ZipResultCallback callback) {
        if (zipFile == null) {
            if (callback != null) {
                callback.onUnZipFailed();
            }
            return;
        }

        InputStream in = null;
        OutputStream out = null;
        try {
            ZipFile zip = new ZipFile(zipFile);
            for (Enumeration entries = zip.entries(); entries.hasMoreElements(); ) {
                ZipEntry entry = (ZipEntry) entries.nextElement();
                String zipEntryName = entry.getName();
                in = zip.getInputStream(entry);
                String outPath = (unzipPath + File.separator + zipEntryName).replaceAll("\\*", "/");
                //判断路径是否存在,不存在则创建文件路径
                File file = new File(outPath.substring(0, outPath.lastIndexOf('/')));
                if (!file.exists()) {
                    file.mkdirs();
                }
                //判断文件全路径是否为文件夹,如果是,不需要解压
                if (new File(outPath).isDirectory()) {
                    continue;
                }
                out = new FileOutputStream(outPath);
                byte[] buf1 = new byte[1024];
                int len;
                while ((len = in.read(buf1)) > 0) {
                    out.write(buf1, 0, len);
                }
            }
        } catch (IOException e) {
            zipFile.delete();
            e.printStackTrace();
            if (callback != null) {
                callback.onUnZipFailed();
            }
        } finally {
            try {
                if (in != null) {
                    in.close();
                }
                if (out != null) {
                    out.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
                zipFile.delete();
                if (callback != null) {
                    callback.onUnZipFailed();
                }
            }
            if (callback != null) {
                callback.onUnZipFinished(unzipPath);
            }
        }
    }


    public interface ZipResultCallback {
        void onUnZipFailed();

        void onUnZipFinished(String unzipPath);
    }


}
