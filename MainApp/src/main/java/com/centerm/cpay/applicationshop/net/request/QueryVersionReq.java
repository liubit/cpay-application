package com.centerm.cpay.applicationshop.net.request;

import android.content.Context;

import com.centerm.cpay.applicationshop.net.BaseRequest;
import com.centerm.cpay.applicationshop.net.cont.JsonKey;
import com.centerm.cpay.applicationshop.net.cont.Method;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * author: linwanliang</br>
 * date:2016/7/2</br>
 */
public class QueryVersionReq extends BaseRequest {
    public QueryVersionReq(Context context, String pkgNames) {
        super(context);
        setMethod(Method.GET_APP_VERSIONS);
        JSONObject body = getBody();
        try {
            body.put(JsonKey.apkListStr, pkgNames);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        addBody(body);
    }
}
