package com.centerm.cpay.applicationshop.adapter;

import android.content.Context;
import android.content.Intent;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.centerm.cpay.applicationshop.R;
import com.centerm.cpay.applicationshop.bean.Advertisement;
import com.centerm.cpay.applicationshop.common.BroadcastAction;
import com.centerm.cpay.applicationshop.view.banner.CBPageAdapter;

/**
 * Created by 王玮 on 2016/8/1.
 */
public class AdBannerAdapter implements CBPageAdapter.Holder<Advertisement> {
    private ImageView imageView;

    public AdBannerAdapter() {
    }

    @Override
    public View createView(Context context) {
        View view = LayoutInflater.from(context).inflate(
                R.layout.convenientbanner_viewpager_item, null);
        imageView = (ImageView) view.findViewById(R.id.image);
        return view;
    }

    @Override
    public void UpdateUI(final Context context, int position, final Advertisement data) {
        if (data.getDrawableId() != -1) {
            imageView.setImageResource(data.getDrawableId());
        } else {
            Glide.with(context).load(data.getImgUrl()).placeholder(R.drawable.default_ad_img)
                    .error(R.drawable.default_ad_img).into(imageView);
        }
        imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.i("ss",data.getFileUrl());
                Log.i("11",data.getImgUrl());
                String type = data.getType();
                if (!TextUtils.isEmpty(type)) {
                    if (type.equals("2") || type.equals("4")) {
                        //网页类
                        Intent intent = new Intent();
                        intent.setAction(BroadcastAction.ACTION_WEB_ACTIVITY);
                        intent.putExtra("ad", data);
                        context.sendBroadcast(intent);
                    } else if (type.equals("3")) {
                        //TODO 流媒体
                    }
                }
            }
        });
    }
}
