package com.centerm.cpay.applicationshop.view;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.widget.TextView;

import com.centerm.cpay.applicationshop.R;


/**
 * author: linwanliang</br>
 * date:2016/7/10</br>
 */
public class MyAlertDialog extends Dialog implements View.OnClickListener {

    private TextView msgShow;
    private TextView positiveBtn;
    private TextView negativeBtn;
    private ButtonClickListener positiveClick, negativeClick;
    private boolean canCancel = true;


    public MyAlertDialog(Context context) {
        super(context, R.style.CommonCustomDialog);
        init();
    }

    protected MyAlertDialog(Context context, boolean cancelable, OnCancelListener cancelListener) {
        super(context, cancelable, cancelListener);
        init();
    }

    public MyAlertDialog(Context context, int themeResId) {
        super(context, themeResId);
        init();
    }

    private void init() {
        LayoutInflater inflater = getLayoutInflater();
        View view = inflater.inflate(R.layout.common_alert_dialog, null);
        setContentView(view);
        msgShow = (TextView) view.findViewById(R.id.msg_show);
        negativeBtn = (TextView) view.findViewById(R.id.negative_btn);
        negativeBtn.setOnClickListener(this);
        positiveBtn = (TextView) view.findViewById(R.id.positive_btn);
        positiveBtn.setOnClickListener(this);
        setCanceledOnTouchOutside(false);
        setOnKeyListener(new OnKeyListener() {
            @Override
            public boolean onKey(DialogInterface dialog, int keyCode, KeyEvent event) {
                switch (keyCode) {
                    case KeyEvent.KEYCODE_HOME:
                        if (canCancel) {
                            dismiss();
                            return false;
                        } else {
                            return true;
                        }
                    case KeyEvent.KEYCODE_BACK:
                        if (canCancel) {
                            return false;
                        } else {
                            return true;
                        }
                }
                return false;
            }
        });
    }

    public void setMessage(String msg) {
        msgShow.setText(msg);
    }

    public void setNegativeClick(String btnText, ButtonClickListener listener) {
        negativeBtn.setText(btnText);
        this.negativeClick = listener;
    }

    public void setPositiveClick(String btnText, ButtonClickListener listener) {
        positiveBtn.setText(btnText);
        this.positiveClick = listener;
    }

    public void setCancelable(boolean cancelable) {
        this.canCancel = cancelable;
    }

    @Override
    public void onClick(View v) {
        dismiss();
        switch (v.getId()) {
            case R.id.negative_btn:
                if (negativeClick != null) {
                    negativeClick.onClick(v);
                }
                break;
            case R.id.positive_btn:
                if (positiveClick != null) {
                    positiveClick.onClick(v);
                }
                break;
        }
    }

    public interface ButtonClickListener {
        void onClick(View view);
    }

    @Override
    public void show() {
//        WindowManager.LayoutParams params = getWindow().getAttributes();
//        params.width = dip2px(200);
//        params.height = dip2px(120);
//        getWindow().setAttributes(params);
//        getWindow().getAttributes().width =360;
//                getContext().getResources().getDimensionPixelOffset(R.dimen.dialog_width);
//        getWindow().getAttributes().height =240;
//                getContext().getResources().getDimensionPixelOffset(R.dimen.dialog_height);
        super.show();
    }
}
