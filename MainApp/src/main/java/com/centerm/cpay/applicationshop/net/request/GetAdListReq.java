package com.centerm.cpay.applicationshop.net.request;

import android.content.Context;

import com.centerm.cpay.applicationshop.net.BaseRequest;
import com.centerm.cpay.applicationshop.net.cont.Method;

import org.json.JSONException;
import org.json.JSONObject;

import config.Config;

import static com.centerm.cpay.applicationshop.net.cont.JsonKey.screenType;

/**
 * author: linwanliang</br>
 * date:2016/7/8</br>
 */
public class GetAdListReq extends BaseRequest {
    public GetAdListReq(Context context) {
        super(context);
        setMethod(Method.GET_AD_LIST);
        JSONObject body = new JSONObject();
        try {
            body.put(screenType, Config.SCREEN_TYPE);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        addBody(body);
    }
}
