package com.centerm.cpay.applicationshop.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RatingBar;
import android.widget.TextView;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.centerm.cpay.applicationshop.R;
import com.centerm.cpay.applicationshop.activity.AppDetailActivity;
import com.centerm.cpay.applicationshop.bean.AppInfo;
import com.centerm.cpay.applicationshop.common.BroadcastAction;
import com.centerm.cpay.applicationshop.common.DialogFactory;
import com.centerm.cpay.applicationshop.common.KeyName;
import com.centerm.cpay.applicationshop.common.MaterialDialogManager;
import com.centerm.cpay.applicationshop.download.ApkDownloadManager;
import com.centerm.cpay.applicationshop.utils.AppViewHelper;
import com.centerm.cpay.applicationshop.utils.FileUtils;
import com.centerm.cpay.applicationshop.view.MyAlertDialog;
import com.centerm.cloudsys.sdk.common.log.Log4d;
import com.j256.ormlite.logger.Log;
import com.j256.ormlite.logger.Logger;
import com.nostra13.universalimageloader.core.ImageLoader;

import config.Config;

/**
 * author: linwanliang</br>
 * date:2016/7/7</br>
 */
public class AppViewBinder implements View.OnClickListener {
    //    private Log4d logger = Log4d.getDefaultInstance();
//    private String tag = AppViewBinder.class.getSimpleName();

    private Context context;
    private AppInfo appInfo;
    private ImageView iconShow;
    private TextView nameShow;
    private TextView sizeShow;
    private RatingBar ratingBar;
    private TextView operateBtn;
    private TextView extraShow;
    private TextView speedShow;
    private ProgressBar progressBar;
    private long lastClick;

    private TextView operateBtn1;
    private TextView operateBtn2;

    private AppViewBinder(Context context, View view, AppInfo data) {
        this.context = context;
        this.appInfo = data;
        view.setOnClickListener(this);
        iconShow = (ImageView) view.findViewById(R.id.app_icon_show);
        nameShow = (TextView) view.findViewById(R.id.app_name_show);
        sizeShow = (TextView) view.findViewById(R.id.app_size_show);
        ratingBar = (RatingBar) view.findViewById(R.id.rating_bar);
        operateBtn = (TextView) view.findViewById(R.id.operate_btn);
        if (operateBtn != null) {
            operateBtn.setOnClickListener(this);
        }
        progressBar = (ProgressBar) view.findViewById(R.id.app_progress);
        extraShow = (TextView) view.findViewById(R.id.app_extras_show);
        speedShow = (TextView) view.findViewById(R.id.app_speed_show);

        operateBtn2 = (TextView) view.findViewById(R.id.operate_btn2);
        if (operateBtn2 != null) {
            operateBtn2.setOnClickListener(this);
        }
        int progress = data.getDownloadExtra().getProgress();
        if (progressBar != null && progress != -1) {
            progressBar.setVisibility(View.VISIBLE);
            progressBar.setProgress(progress);
        }

    }

    public static AppViewBinder bind(Context context, View view, AppInfo data) {
        AppViewBinder binder = new AppViewBinder(context, view, data);
        view.setTag(binder);
        return binder;
    }

    public void reset(AppInfo appInfo) {
        this.appInfo = appInfo;
        reset();
    }

    public void reset() {
        if (appInfo != null) {
            if (iconShow != null) {
                ImageLoader.getInstance().displayImage(appInfo.getIconUrl(), iconShow, Config.APP_ICON_OPTIONS);
            }
            if (nameShow != null) {
                nameShow.setText(appInfo.getName());
            }
            if (sizeShow != null) {
                sizeShow.setText(FileUtils.formatSize(appInfo.getSize()));
            }
            if (operateBtn != null) {
                operateBtn.setOnClickListener(this);
                operateBtn.setTag(appInfo);
            }
            if (ratingBar != null) {
                ratingBar.setRating(appInfo.getRate());
            }
            if (extraShow != null) {
                extraShow.setText(combineExtra(appInfo.getSize(), appInfo.getDownloadCount()));
            }
            if (progressBar != null) {
                progressBar.setVisibility(View.GONE);
            }
            if (speedShow != null) {
                AppViewHelper.updateSpeedShow(appInfo, speedShow);
            }
            AppViewHelper.updateStatus(operateBtn, progressBar,
                    appInfo.getStatus(),
                    appInfo.getDownloadExtra().getProgress(),
                    AppViewHelper.BUTTON_STYLE1);
        }
    }

    public AppInfo getBindedData() {
        return appInfo;
    }


    @Override
    public void onClick(View v) {
        if (!canPerformClick()) {
            return;
        }
        switch (v.getId()) {
            case R.id.operate_btn:
                AppViewHelper.onOperate(context, appInfo, operateBtn, progressBar, AppViewHelper.BUTTON_STYLE1);
                break;
            case R.id.item_root_view:
                //为了使用MD风格的页面切换
                //发出广播，由MainActivity接收并且打开详情界面
                Intent intent = new Intent();
//                AppDetailActivity.data = appInfo;
                intent.setAction(BroadcastAction.ACTION_OPEN_APPDETAIL);
                intent.putExtra(KeyName.APP_INFO, appInfo);
                context.sendBroadcast(intent);
                break;
            //下载管理界面的暂停/继续按钮
//            case R.id.operate_btn1:
//                AppViewHelper.onOperate(context, appInfo, operateBtn1, progressBar, AppViewHelper.BUTTON_STYLE1);
//                break;
            //下载管理界面的取消按钮
            case R.id.operate_btn2:
                MaterialDialogManager.getChooserByTips(context, R.string.p_delete_task, new MaterialDialogManager.ButtonClick() {
                    @Override
                    public void onClick() {
                        ApkDownloadManager.getInstance().stop(context, appInfo);
                    }
                }).show();
                break;
        }
    }

    public void updateStatus(AppInfo info, boolean isVisible) {
        AppInfo.DownloadExtra extra = info.getDownloadExtra();
        int progress = extra == null ? -1 : extra.getProgress();
        appInfo.getDownloadExtra().setProgress(progress);
        appInfo.setStatus(info.getStatus());
//        if (isVisible) {
        AppViewHelper.updateStatus(operateBtn, progressBar, info.getStatus(), progress, AppViewHelper.BUTTON_STYLE1);
        if (speedShow != null) {
            AppViewHelper.updateSpeedShow(info, speedShow);
        }
//        }
    }

    private String combineExtra(long size, int downloadCounts) {
        return FileUtils.formatSize(size) + " | " + downloadCounts + "次下载";
    }

    private boolean canPerformClick() {
        long now = System.currentTimeMillis();
        if (now - lastClick >= 1000) {
            lastClick = now;
            return true;
        }
        return false;
    }

    public void hideOperate() {
        operateBtn.setVisibility(View.INVISIBLE);
    }
}
