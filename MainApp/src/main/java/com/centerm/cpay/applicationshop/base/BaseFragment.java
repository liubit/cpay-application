package com.centerm.cpay.applicationshop.base;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.centerm.cpay.applicationshop.net.DefaultHttpClient;
import com.centerm.cloudsys.sdk.common.log.Log4d;

import config.Config;

/**
 * Created by linwanliang on 2016/6/24.
 */
public abstract class BaseFragment extends Fragment implements IComponentCreator {

    protected final static Log4d LOGGER = Log4d.getDefaultInstance();
    protected Context context;
    protected String TAG;
    private long lastClick;
    protected boolean isCreated;

    public BaseFragment() {
        TAG = this.getClass().getSimpleName();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
//        LOGGER.info(TAG, "onCreate");
        super.onCreate(savedInstanceState);
        this.context = getActivity();
        onInitLocalData();
    }

    @Override
    public void doThingsAfterInitView() {
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
//        LOGGER.info(TAG, "onCreateView");
        int layoutId = onInitLayoutId();
        if (layoutId > 0) {
            View view = inflater.inflate(layoutId, null);
            onInitView(view);
            doThingsAfterInitView();
            return view;
        }
        return super.onCreateView(inflater, container, savedInstanceState);
    }

    @Override
    public void onResume() {
        super.onResume();
//        LOGGER.info(TAG, "onResume");
        isCreated = true;
    }

    /**
     * 防止快速点击
     *
     * @return
     */
    protected boolean canPerformClick() {
        long now = System.currentTimeMillis();
        if (now - lastClick >= 1000) {
            lastClick = now;
            return true;
        }
        LOGGER.warn(TAG, "点击速度过快，不响应事件");
        return false;
    }

    public void showLoading() {
        final Activity activity = getActivity();
        if (activity != null && activity instanceof BaseActivity_v21) {
            ((BaseActivity_v21) activity).showLoading();
        }
    }


    public void stopLoading() {
        final Activity activity = getActivity();
        if (activity != null && activity instanceof BaseActivity_v21) {
            ((BaseActivity_v21) activity).stopLoading();
        }
    }

    public DefaultHttpClient getHttpClient() {
        return ((BaseActivity_v21) getActivity()).getHttpClient();
    }

}
