package com.centerm.cpay.applicationshop.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RatingBar;
import android.widget.ScrollView;
import android.widget.TextView;

import com.centerm.cpay.applicationshop.R;
import com.centerm.cpay.applicationshop.base.BaseDownloadActivity;
import com.centerm.cpay.applicationshop.bean.AppInfo;
import com.centerm.cpay.applicationshop.common.BroadcastAction;
import com.centerm.cpay.applicationshop.common.KeyName;
import com.centerm.cpay.applicationshop.download.AppInfoSync;
import com.centerm.cpay.applicationshop.net.ResponseHandler;
import com.centerm.cpay.applicationshop.net.request.GetAppDetailReq;
import com.centerm.cpay.applicationshop.net.response.AppDetailParser;
import com.centerm.cpay.applicationshop.utils.AppViewHelper;
import com.centerm.cpay.applicationshop.utils.FileUtils;
import com.centerm.cpay.applicationshop.utils.ViewUtils;
import com.centerm.cpay.applicationshop.view.ScreenShotGallery;

import butterknife.Bind;
import butterknife.ButterKnife;
import config.Config;
import test.JsonCreator;

/**
 * Created by linwanliang on 2016/6/21.
 */
public class AppDetailActivity extends BaseDownloadActivity implements View.OnClickListener {
    private AppInfo data;
    private Button operateBtn;
    private ProgressBar progressBar;
    private TextView nameShow;
    private TextView extraShow;
    private RatingBar ratingBar;
    private boolean isFromWeb = false;

    @Override
    public int onInitLayoutId() {
        return R.layout.activity_app_detail;
    }

    @Override
    public void onInitLocalData() {
        isReceivePackageChanged = true;
        data = (AppInfo) getIntent().getSerializableExtra(KeyName.APP_INFO);
        if (data == null) {
            logger.warn(TAG, "传递过来的数据对象为空");
            data = new AppInfo();
            String packageName = getIntent().getStringExtra("packageName");
            data.setPackageName(packageName);
            isFromWeb = true;
        } else {
            AppInfoSync.sync(context, data);
        }
    }

    @Override
    public void onInitView(View view) {
        //设置ActionBar
        initUsualToolbar(R.string.title_app_detail);
        operateBtn = (Button) findViewById(R.id.operate_btn);
        progressBar = (ProgressBar) findViewById(R.id.app_progress);
        //初始化部分基本信息
        nameShow = ((TextView) findViewById(R.id.app_name_show));
        extraShow = (TextView) findViewById(R.id.app_extras_show);
        //星级
        ratingBar = (RatingBar) findViewById(R.id.rating_bar);
        operateBtn.setOnClickListener(AppDetailActivity.this);
        if (!isFromWeb) {
            refreshAppView();
        }
        ((ScrollView) findViewById(R.id.scroll_v)).fullScroll(View.FOCUS_DOWN);
    }

    private void refreshAppView() {
        //应用名称
        if (nameShow != null) {
            nameShow.setText(data.getName());
        }
        String labelSize = getResources().getString(R.string.label_file_size);
        String labelDownloaded = getResources().getString(R.string.label_times_downloaded);
        //大小和下载量
        long size = data.getSize();
        int downloadCounts = data.getDownloadCount();
        String extraInfo = labelSize + " " + FileUtils.formatSize(size) + " | " + downloadCounts + " " + labelDownloaded;
        if (extraShow != null) {
            extraShow.setText(extraInfo);
        }
        if (ratingBar != null) {
            ratingBar.setRating(data.getRate());
        }
        AppViewHelper.updateStatus(operateBtn, progressBar,
                data.getStatus(),
                data.getDownloadExtra().getProgress(), AppViewHelper.BUTTON_STYLE2);
    }

    @Override
    public void doThingsAfterInitView() {
        super.doThingsAfterInitView();
        if (Config.DEMO) {
            AppDetailParser parser = new AppDetailParser(JsonCreator.getAppDetail().toString());
            refresh(parser);
        } else {
            requestDetail();
        }
    }

    @Override
    public void onReceiveAppStatus(Context context, Intent intent) {
        String action = intent.getAction();
//        Log.e("广播", action);
//        ViewUtils.showToast(context, action);
        if (BroadcastAction.DOWNLOAD.ACTION_PAUSE_ALL.equals(action)) {
            data.setStatus(AppInfo.AppStatus.PAUSE_DOWNLOAD);
            operateBtn.setText(data.getStatus().getText());
            return;
        }
        operateBtn = (Button) findViewById(R.id.operate_btn);
        progressBar = (ProgressBar) findViewById(R.id.app_progress);
        if (Intent.ACTION_PACKAGE_ADDED.equals(action)) {
            String pkgPrefix = intent.getData().getSchemeSpecificPart();
            if (data.getPackageName().startsWith(pkgPrefix)) {
                data.setStatus(AppInfo.AppStatus.CAN_OPEN);
                AppViewHelper.updateStatus(operateBtn, progressBar, data.getStatus(), -1, AppViewHelper.BUTTON_STYLE2);
            }
            return;
        }
        if (Intent.ACTION_PACKAGE_REMOVED.equals(action)) {
            String pkgPrefix = intent.getData().getSchemeSpecificPart();
            if (data.getPackageName().startsWith(pkgPrefix)) {
                data.setStatus(AppInfo.AppStatus.CAN_DOWNLOAD);
                AppViewHelper.updateStatus(operateBtn, progressBar, data.getStatus(), -1, AppViewHelper.BUTTON_STYLE2);
            }
            return;
        }
        if (BroadcastAction.DOWNLOAD.ACTION_SUCCESS.equals(action)) {
            data.setStatus(AppInfo.AppStatus.WAIT_INSTALL);
        }
        AppInfo app = (AppInfo) intent.getSerializableExtra(KeyName.APP_INFO);
        if (!data.getPackageName().equals(app.getPackageName())) {
            return;
        }
        AppInfo.AppStatus status = app.getStatus();
        int progress = app.getDownloadExtra().getProgress();
        AppViewHelper.updateStatus(operateBtn, progressBar, status, progress, AppViewHelper.BUTTON_STYLE2);
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.operate_btn:
                if (data == null) {
                    ViewUtils.showToast(context, "无法下载");
                    return;
                }
                ProgressBar progressBar = (ProgressBar) findViewById(R.id.app_progress);
                AppViewHelper.onOperate(AppDetailActivity.this, data, (TextView) v, progressBar, AppViewHelper.BUTTON_STYLE2);
                break;
            case R.id.mLlBack:
                supportedFinish();
                break;
        }
    }

    private void detailToAppInfo(AppDetailParser parser) {
        data.setName(parser.getName());
        data.setRate(parser.getRate());
        data.setSize(parser.getSize());
        data.setDescription(parser.getDescription());
        data.setVersion(parser.getVersion());
        data.setVersionName(parser.getVersionName());
        data.setDownloadCount(parser.getDownloadCounts());
        data.setTypeName(parser.getTypeName());
        data.setIconUrl(parser.getIconUrl());
        data.setKeyWords(parser.getKeyWords());
        data.setDeveloper(parser.getDeveloper());
        data.setPackageUrl(parser.getDownloadUrl());
    }

    private void requestDetail() {
        showLoading();
        GetAppDetailReq req = new GetAppDetailReq(context, data.getPackageName());
        ResponseHandler handler = new ResponseHandler() {
            @Override
            public void onSuccess(String response) {
                stopLoading();
                AppDetailParser parser = new AppDetailParser(response);
                if (parser.hasData()) {
                    refresh(parser);
                    operateBtn.setEnabled(true);
                    if (isFromWeb) {
                        detailToAppInfo(parser);//详情转换为appInfo
                        AppInfoSync.sync(context, data);
                        //isFromWeb代表是从广告界面跳转到该界面
                        //这种方式下，只传过来一个packageName，因此需要与平台交互，将新的文件md5值更新到当前界面数据
                        //否则下载文件会出现MD5校验失败
                        data.setFileMd5(parser.getFileMd5());
                        refreshAppView();
                    }
                } else {
                    ViewUtils.showToast(context, R.string.tips_get_app_info_failed);
                    operateBtn.setEnabled(false);
                    operateBtn.setBackgroundDrawable(getResources().getDrawable(R.drawable.btn_bg_gray));
                }
            }

            @Override
            public void onFailure(int errorCode, String errorInfo) {
                stopLoading();
                ViewUtils.showToast(context, R.string.tips_get_app_info_failed);
                operateBtn.setEnabled(false);
                operateBtn.setBackgroundDrawable(getResources().getDrawable(R.drawable.btn_bg_gray));
            }
        };
        getHttpClient().post(context, req, handler);
    }

    private void refresh(AppDetailParser parser) {
        ScreenShotGallery gallery = (ScreenShotGallery) findViewById(R.id.gallery);
        gallery.loadImage(parser.getPreviewImg());
        TextView infoShow = (TextView) findViewById(R.id.app_info_show);
        if (infoShow != null) {
            infoShow.setText(combineBasicInfo(parser));
        }
        TextView updateShow = (TextView) findViewById(R.id.app_update_show);
        if (updateShow != null) {
            updateShow.setText(parser.getUpdateInfo());
        }
        TextView desShow = (TextView) findViewById(R.id.app_desc_show);
        if (desShow != null) {
            desShow.setText(parser.getDescription());
        }
    }

    private String combineBasicInfo(AppDetailParser parser) {
        StringBuilder sBuilder = new StringBuilder();
        sBuilder.append(context.getResources().getString(R.string.label_version) + "：")
                .append(parser.getVersionName());
        sBuilder.append("\n");
        sBuilder.append(context.getResources().getString(R.string.label_release_time) + "：")
                .append(parser.getPublicTime());
        sBuilder.append("\n");
        sBuilder.append(context.getResources().getString(R.string.label_sort) + "：")
                .append(parser.getTypeName());
        sBuilder.append("\n");
        sBuilder.append(context.getResources().getString(R.string.label_publisher) + "：")
                .append(parser.getDeveloper());
        sBuilder.append("\n");
        sBuilder.append(context.getResources().getString(R.string.label_keywords) + "：" + parser.getKeyWords());
        return sBuilder.toString();
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // TODO: add setContentView(...) invocation
        ButterKnife.bind(this);
    }
}