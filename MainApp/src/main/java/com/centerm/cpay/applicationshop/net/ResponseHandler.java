package com.centerm.cpay.applicationshop.net;

/**
 * Created by linwanliang on 2016/6/21.
 */
public interface ResponseHandler {

    public void onSuccess(String response);

    public void onFailure(int errorCode, String errorInfo);

}
