package com.centerm.cpay.applicationshop.download;

import android.app.DownloadManager;
import android.content.Context;
import android.content.Intent;

import com.centerm.cpay.applicationshop.bean.AppInfo;
import com.centerm.cpay.applicationshop.common.BroadcastAction;
import com.centerm.cpay.applicationshop.common.KeyName;
import com.centerm.cpay.applicationshop.db.DbHelper;
import com.centerm.cpay.applicationshop.utils.FileUtils;
import com.centerm.cpay.applicationshop.utils.ViewUtils;
import com.centerm.cpay.midsdk.dev.DeviceFactory;
import com.centerm.cpay.midsdk.dev.EnumDeviceType;
import com.centerm.cpay.midsdk.dev.define.ISystemService;
import com.centerm.cpay.midsdk.dev.define.system.InstallStatusListener;
import com.centerm.cloudsys.sdk.common.log.Log4d;
import com.j256.ormlite.android.apptools.OpenHelperManager;
import com.j256.ormlite.dao.Dao;

import java.io.File;
import java.sql.SQLException;
import java.util.LinkedList;

/**
 * author: linwanliang</br>
 * date:2016/7/12</br>
 */
public class SilentInstallManager {
    private static SilentInstallManager instance;
    private LinkedList<AppInfo> queue = new LinkedList<>();
    private Log4d logger = Log4d.getDefaultInstance();
    private String tag = SilentInstallManager.class.getSimpleName();
    private boolean taskRunning;
    private Context context;
    private ApkDownloadManager downloadManager;

    public static SilentInstallManager getInstance(Context context) {
        if (instance == null) {
            instance = new SilentInstallManager();
            instance.downloadManager = ApkDownloadManager.getInstance();
        }
        instance.context = context.getApplicationContext();
        return instance;
    }

    public synchronized boolean push(final AppInfo app) {
        if (app == null) {
            return false;
        }
        final String name = app.getName();
        final File file = DownloadTools.getApkFile(app);
        if (!file.exists()) {
            logger.warn(tag, name + "，文件不存在，无法自动安装");
            return false;
        }
        if (file.length() != app.getSize()) {
            logger.debug(tag, "文件大小：" + app.getSize() + "，实际大小：" + file.length());
            logger.warn(tag, name + "，文件大小错误，无法自动安装，请重新下载");
            ViewUtils.showToast(context, "【" + name + "】下载失败，请重新下载");

            queue.remove(app);
            notityQueueChanged();
            app.setStatus(AppInfo.AppStatus.DOWNLOAD_FAILED);
            downloadManager.updateAppStatusPool(app.getPackageName(), app.getStatus(), -1);
            refreshDb(context, app);

            Intent intent = createIntent(BroadcastAction.DOWNLOAD.ACTION_FAILED, app);
            intent.setAction(BroadcastAction.INSTALL.ACTION_SUCCESS);
            context.sendBroadcast(intent);
            deleteFile(name, file);
            return false;
        }
        if (queue.contains(app)) {
            queue.remove(app);
        }
        logger.info(tag, name + "，加入静默安装队列");
        queue.addLast(app);
        notityQueueChanged();
        return true;
    }

    private void notityQueueChanged() {
        if (taskRunning) {
            logger.warn(tag, "当前有任务执行中，等待....");
            return;
        }
        if (queue.size() > 0) {
            AppInfo app = queue.getFirst();
            silentInstall(context, app);
        } else {
            logger.info(tag, "所有静默安装任务执行完毕！");
        }

    }

    private void silentInstall(final Context context, final AppInfo info) {
        final String name = info.getName();
        final ISystemService systemService = (ISystemService) DeviceFactory.getInstance().getDevice(EnumDeviceType.SYSTEM_SERVICE);
        if (systemService != null) {
            taskRunning = true;
            final String path = FileUtils.getApkFilePath(info.getPackageUrl());
            final Intent intent = new Intent();
            intent.putExtra(KeyName.APP_INFO, info);
            new Thread(new Runnable() {
                @Override
                public void run() {
                    try {
                        long delay = 2000;
                        logger.info(tag, name + "，延迟" + delay + "启动静默安装");
                        Thread.sleep(delay);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    logger.info(tag, name + "，开始静默安装");
                    systemService.silentInstall(path, new InstallStatusListener() {
                        @Override
                        public void onFinished(String s) {
                            logger.info(tag, name + "，静默安装完成");
                            //更新队列并通知队列
                            taskRunning = false;
                            queue.remove(info);
                            notityQueueChanged();
                            //更新数据状态
                            info.setStatus(AppInfo.AppStatus.CAN_OPEN);
                            downloadManager.updateAppStatusPool(info.getPackageName(), info.getStatus(), -1);
                            refreshDb(context, info);
                            //发出广播
                            intent.setAction(BroadcastAction.INSTALL.ACTION_SUCCESS);
                            context.sendBroadcast(intent);
                            //静默安装完成后删除APK文件
                            deleteFile(name, new File(path));
                        }

                        @Override
                        public void onError(int i, String s) {
                            logger.warn(tag, name + "，静默安装失败，改为手动安装");
                            taskRunning = false;
                            queue.remove(info);
                            notityQueueChanged();
                            info.setStatus(AppInfo.AppStatus.WAIT_INSTALL);
                            downloadManager.updateAppStatusPool(info.getPackageName(), info.getStatus(), -1);
                            refreshDb(context, info);
                            intent.setAction(BroadcastAction.INSTALL.ACTION_FAILED);
                            context.sendBroadcast(intent);
                        }
                    });

                }
            }).start();
        } else {
            logger.warn(tag, "SDK服务异常，无法静默安装，改为手动安装");
            queue.remove(info);
            notityQueueChanged();
            info.setStatus(AppInfo.AppStatus.WAIT_INSTALL);
            Intent intent = createIntent(BroadcastAction.INSTALL.ACTION_FAILED, info);
            intent.setAction(BroadcastAction.INSTALL.ACTION_FAILED);
            context.sendBroadcast(intent);
        }
    }

    private void refreshDb(Context context, AppInfo app) {
        DbHelper dbHelper = OpenHelperManager.getHelper(context, DbHelper.class);
        Dao<AppInfo, String> appDao = dbHelper.getAppDao();
        try {
            if (app.getStatus().equals(AppInfo.AppStatus.CAN_OPEN)) {
                appDao.deleteById(app.getPackageName());
            } else {
                appDao.createOrUpdate(app);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }


    private Intent createIntent(String action, AppInfo app) {
        Intent intent = new Intent();
        intent.setAction(action);
        intent.putExtra(KeyName.APP_INFO, app);
        return intent;
    }

    private void deleteFile(final String name, final File file) {
        if (file != null && file.exists()) {
            new Thread(new Runnable() {
                @Override
                public void run() {
                    try {
                        Thread.sleep(1000);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    boolean result = file.delete();
                    logger.info(tag, name + "，删除安装包结果：" + result);
                }
            }).start();
        } else {
            logger.warn(tag, name + "，安装包不存在，删除失败");
        }
    }

}
