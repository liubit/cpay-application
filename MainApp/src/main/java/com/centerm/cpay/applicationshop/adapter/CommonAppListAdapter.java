package com.centerm.cpay.applicationshop.adapter;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import com.centerm.cpay.applicationshop.bean.AppInfo;
import com.centerm.cloudsys.sdk.common.log.Log4d;
import com.centerm.cloudsys.sdk.common.utils.StringUtils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * author: linwanliang</br>
 * date:2016/7/7</br>
 */
public class CommonAppListAdapter extends BaseAdapter {

    private Log4d logger = Log4d.getDefaultInstance();
    private String tag = CommonAppListAdapter.class.getSimpleName();
    private Context context;
    private int resId;
    private Map<String, View> idMapView;
    private List<AppInfo> appList = new ArrayList<>();
    private boolean hideOperate;
    private boolean isViewVisible = true;

    public CommonAppListAdapter(Context context, int itemId, List<AppInfo> appList) {
        this.context = context;
        this.resId = itemId;
        this.appList.addAll(appList);
        idMapView = new HashMap<>();
    }

    public void setOperateEnabled(boolean enabled) {
        this.hideOperate = enabled;
    }

    @Override
    public int getCount() {
        int counts = appList == null ? 0 : appList.size();
        return counts;
    }

    @Override
    public Object getItem(int position) {
        return appList == null ? null : appList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        AppInfo data = appList.get(position);
//        logger.warn(tag, data.toString());
        String pkgName = data.getPackageName();
        if (convertView == null) {
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(resId, null);
            AppViewBinder.bind(context, convertView, data);
        }
        AppViewBinder binder = (AppViewBinder) convertView.getTag();
        String oldPkgName = binder.getBindedData().getPackageName();
        //先移除旧的映射关系
        idMapView.remove(oldPkgName);
        idMapView.put(pkgName, convertView);

        binder.reset(data);
        if (hideOperate) {
            binder.hideOperate();
        }
        return convertView;
    }

    public void notifyDataSetChanged(List<AppInfo> apps, boolean append) {
        if (!append) {
            appList.clear();
        }
        appList.addAll(apps);
//        if (isViewVisible) {
        super.notifyDataSetChanged();
//        }
    }

    public boolean viewExists(String pkgName) {
        return idMapView.containsKey(pkgName);
    }

    public void notifyItemStatusChanged(AppInfo info) {
//        logger.debug(tag, info.getName() + "删除");
        for (int i = 0; i < appList.size(); i++) {
            AppInfo item = appList.get(i);
            if (item.getPackageName().equals(info.getPackageName())) {
                item.setStatus(info.getStatus());
                break;
            }
        }
        View view = idMapView.get(info.getPackageName());
        if (view != null) {
            AppViewBinder binder = (AppViewBinder) view.getTag();
            binder.updateStatus(info, isViewVisible);
        }
    }

    public int notifyItemRemoved(String pkgName) {
        if (pkgName == null || StringUtils.isStrNull(pkgName)) {
            return 0;
        }
        for (int i = 0; i < appList.size(); i++) {
            if (appList.get(i).getPackageName().equals(pkgName)) {
                appList.remove(i);
//                if (isViewVisible) {
                super.notifyDataSetChanged();
//                }
                return appList.size();
            }
        }
        return 0;
    }

    public void notifyPackageUnInstalled(String pkgPrefix) {
        if (appList == null) {
            return;
        }
        for (AppInfo app : appList) {
            if (app.getPackageName().startsWith(pkgPrefix)) {
                app.setStatus(AppInfo.AppStatus.CAN_DOWNLOAD);
//                if (isViewVisible) {
                super.notifyDataSetChanged();
//                }
                break;
            }
        }
    }

    public void notifyPackageInstalled(String pkgPrefix) {
        Log.i("233423===========", pkgPrefix);
        if (appList == null) {
            return;
        }
        for (AppInfo app : appList) {
            if (app.getPackageName().startsWith(pkgPrefix)) {
                app.setStatus(AppInfo.AppStatus.CAN_OPEN);
//                if (isViewVisible) {
                super.notifyDataSetChanged();
//                }
                break;
            }
        }
    }

    public void notifyAllPaused() {
        if (appList != null && appList.size() > 0) {
            for (AppInfo item : appList) {
                if (item.getStatus().equals(AppInfo.AppStatus.DOWNLOADING)
                        || item.getStatus().equals(AppInfo.AppStatus.WAIT_DOWNLOAD)) {
                    item.setStatus(AppInfo.AppStatus.PAUSE_DOWNLOAD);
                }
            }
//            if (isViewVisible) {
            super.notifyDataSetChanged();
//            }
        }
    }


    public List<AppInfo> getData() {
        return appList;
    }

    public void setPause() {
        isViewVisible = false;
    }

    public void setResume() {
        isViewVisible = true;
    }

    public boolean isViewVisible() {
        return isViewVisible;
    }
}
