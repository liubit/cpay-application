package com.centerm.cpay.applicationshop.net.request;

import android.content.Context;

import com.centerm.cpay.applicationshop.bean.AppInfo;
import com.centerm.cpay.applicationshop.net.BaseRequest;
import com.centerm.cpay.applicationshop.net.cont.JsonKey;
import com.centerm.cpay.applicationshop.net.cont.Method;

import org.apache.http.client.entity.InputStreamFactory;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;

/**
 * author: linwanliang</br>
 * date:2016/7/13</br>
 */
public class UninstallVerifyReq extends BaseRequest {


    public UninstallVerifyReq(Context context) {
        super(context);
        setMethod(Method.UNINSTALL_CHECK);
        JSONObject body = new JSONObject();
        addBody(body);
    }

    public void addVeriryList(List<AppInfo> apps) {
        if (apps == null || apps.size() == 0) {
            return;
        }
        StringBuilder sBuilder = new StringBuilder();
        for (int i = 0; i < apps.size(); i++) {
            sBuilder.append(apps.get(i).getPackageName());
            if (i != apps.size() - 1) {
                sBuilder.append("|");
            }
        }
        try {
            getBody().put(JsonKey.apkListStr, sBuilder.toString());
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }


}
