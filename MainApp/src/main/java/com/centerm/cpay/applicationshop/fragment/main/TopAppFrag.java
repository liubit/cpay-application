package com.centerm.cpay.applicationshop.fragment.main;

import android.content.Context;
import android.content.Intent;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.widget.ListView;

import com.centerm.cpay.applicationshop.R;
import com.centerm.cpay.applicationshop.adapter.CommonAppListAdapter;
import com.centerm.cpay.applicationshop.adapter.MainAppGridAdapter;
import com.centerm.cpay.applicationshop.asyncTask.AppAsyncTask;
import com.centerm.cpay.applicationshop.asyncTask.AppMatchListener;
import com.centerm.cpay.applicationshop.base.BaseDownloadFragment;
import com.centerm.cpay.applicationshop.bean.AppInfo;
import com.centerm.cpay.applicationshop.common.BroadcastAction;
import com.centerm.cpay.applicationshop.common.KeyName;
import com.centerm.cpay.applicationshop.net.ResponseHandler;
import com.centerm.cpay.applicationshop.net.cont.Method;
import com.centerm.cpay.applicationshop.net.request.GetAppListReq;
import com.centerm.cpay.applicationshop.net.response.AppListParser;
import com.centerm.cpay.applicationshop.utils.FileUtils;
import com.centerm.cpay.applicationshop.utils.ViewUtils;
import com.centerm.cpay.applicationshop.view.Top3AppViewManager;
import com.cjj.MaterialRefreshLayout;
import com.cjj.MaterialRefreshListener;

import org.json.JSONArray;

import java.util.ArrayList;
import java.util.List;

import config.Config;

/**
 * Created by linwanliang on 2016/6/24.
 */
public class TopAppFrag extends BaseDownloadFragment {
    private MaterialRefreshLayout refreshLayout;
    private Top3AppViewManager top3View;
    private CommonAppListAdapter adapter;
    private ListView listView;
    private String apkTypeId;
    private int total = -1;
    private int pageIndex = 0;
    private String DEMO_STRING;
    private String responseCache;
    public static boolean isRefresh = false;

    @Override
    public int onInitLayoutId() {
        return R.layout.main_fragment_top;
    }

    @Override
    public void onInitLocalData() {
        apkTypeId = "90002";
        if (Config.DEMO) {
            DEMO_STRING = FileUtils.readAssetsText(getActivity().getAssets(), "json/" + Method.GET_APP_LIST.replace("/", "_"));
        }
    }

    @Override
    public void onInitView(View view) {
        refreshLayout = (MaterialRefreshLayout) view.findViewById(R.id.refresh_layout);
        initRefreshView(refreshLayout);
        listView = (ListView) view.findViewById(R.id.list_v);
        initListView(listView);
    }

    @Override
    public void doThingsAfterInitView() {
        super.doThingsAfterInitView();

        //视图状态恢复
        if (isCreated) {
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    top3View.notifyDataSetChanged();
                    listView.removeHeaderView(top3View.getView());
                    listView.addHeaderView(top3View.getView());
                    listView.setAdapter(adapter);
                }
            }, Config.DELAY_LOAD_DATA);
        } else {
            if (!Config.DEMO) {
                requestAppList(true, true);
            }
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        Log.i("===onResume", isRefresh + "");
//        if (adapter != null) {
//            adapter.setResume();
//        }
        if (isRefresh) {
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    requestAppList(true, true);
                    isRefresh = false;
                }
            }, Config.DELAY_LOAD_DATA);
        }
    }

    @Override
    public void onPause() {
        super.onPause();
//        if (adapter != null) {
//            adapter.setPause();
//        }
    }

    private void initListView(ListView listView) {
        if (top3View == null) {
            top3View = new Top3AppViewManager(context);
        }
        if (adapter == null) {
            adapter = new CommonAppListAdapter(context, R.layout.common_app_list_item, new ArrayList<AppInfo>());
            listView.setAdapter(adapter);
            if (Config.DEMO) {
                AppListParser parser = new AppListParser(context, DEMO_STRING);
                List<AppInfo> allApps = parser.getAppList();
                if (allApps != null && allApps.size() >= 3) {
                    top3View.notifyDataSetChanged(allApps.subList(0, 3));
                    listView.addHeaderView(top3View.getView());
                    adapter.notifyDataSetChanged(allApps.subList(3, allApps.size()), true);
                }
            }
        }
    }

    @Override
    public void onReceiveAppStatus(Context context, Intent intent) {
        if (!isCreated) {
            LOGGER.verbose(TAG, "未初始化，不处理广播");
            return;
        }
        String action = intent.getAction();
        LOGGER.verbose(TAG, action);
        AppInfo app = (AppInfo) intent.getSerializableExtra(KeyName.APP_INFO);
        if (action.equals(Intent.ACTION_PACKAGE_ADDED)) {
            String pkgPrefix = intent.getData().getSchemeSpecificPart();
            adapter.notifyPackageInstalled(pkgPrefix);
            top3View.notifyPackageInstalled(pkgPrefix);
            return;
        } else if (action.equals(Intent.ACTION_PACKAGE_REMOVED)) {
            String packageName = intent.getData().getSchemeSpecificPart();
            if (adapter != null) {
                adapter.notifyPackageUnInstalled(packageName);
            }
            if (top3View != null && top3View.isViewExits(packageName)) {
                top3View.notifyPackageUnInstalled(packageName);
            }
            return;
        }

        if (app == null) {
            return;
        }
        if (action.equals(BroadcastAction.DOWNLOAD.ACTION_PAUSE_ALL)) {
            top3View.notifyAllPaused();
            adapter.notifyAllPaused();
        } else {
            if (top3View != null && top3View.isViewExits(app.getPackageName())) {
                top3View.updateStatus(app);
            }
            if (adapter != null) {
                adapter.notifyItemStatusChanged(app);
            }
//            if (adapter != null && adapter.viewExists(app.getPackageName())) {
//                adapter.notifyItemStatusChanged(app);
//            }
        }
    }

    private void initRefreshView(MaterialRefreshLayout view) {
        initMaterialRefreshLayout(refreshLayout);
        if (Config.DEMO) {
            view.setLoadMore(true);
        } else {
            view.setLoadMore(false);
        }
        view.setMaterialRefreshListener(new MaterialRefreshListener() {
            @Override
            public void onRefresh(final MaterialRefreshLayout materialRefreshLayout) {
                if (Config.DEMO) {
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            materialRefreshLayout.finishRefresh();
                        }
                    }, 3000);
                } else {
                    requestAppList(true, false);
                }
            }

            @Override
            public void onfinish() {
            }

            @Override
            public void onRefreshLoadMore(final MaterialRefreshLayout materialRefreshLayout) {
                if (Config.DEMO) {
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            AppListParser parser = new AppListParser(context, DEMO_STRING);
                            List<AppInfo> allApps = parser.getAppList();
                            adapter.notifyDataSetChanged(allApps, true);
                            materialRefreshLayout.finishRefreshLoadMore();
                        }
                    }, 3000);
                } else {
                    requestAppList(false, false);
                }
            }
        });
    }

    private void requestAppList(final boolean refresh, boolean showLoading) {
        int c = adapter == null ? 0 : adapter.getCount();//当前列表数量
        int pi = this.pageIndex + 1;//页码
        if (refresh) {
            pi = 1;
        } else {
            if (c >= total) {
                return;
            }
        }
        if (showLoading) {
            showLoading();
        }
        final GetAppListReq req = new GetAppListReq(context);
        req.setApkTypeId(apkTypeId);
        req.setPageIndex(pi);
        ResponseHandler handler = new ResponseHandler() {
            @Override
            public void onSuccess(String response) {
                AppListParser parser = new AppListParser(context, response);
                if (parser.hasData()) {
                    total = parser.getTotal();
                    pageIndex = req.getPageIndex();
                    localMatchApp(parser.getAppsJson(), refresh);
                } else {
                    List<AppInfo> apps = new ArrayList<>();
                    top3View.notifyDataSetChanged(apps);
                    listView.removeHeaderView(top3View.getView());
                    listView.addHeaderView(top3View.getView());
                    adapter.notifyDataSetChanged(apps, !refresh);
                    stopLoading();
                    refreshLayout.finishRefresh();
                    refreshLayout.finishRefreshLoadMore();
                    ViewUtils.showToast(context, parser.getMsg());
                }
            }

            @Override
            public void onFailure(int errorCode, String errorInfo) {
                stopLoading();
                refreshLayout.finishRefresh();
                refreshLayout.finishRefreshLoadMore();
                ViewUtils.showToast(context, errorInfo);
            }
        };
        getHttpClient().post(context, req, handler);
    }

    private void localMatchApp(JSONArray appJson, final boolean refresh) {
        AppAsyncTask task = new AppAsyncTask();
        task.setAppMatchLisenter(getActivity(), new AppMatchListener() {
            @Override
            public void onStart() {

            }

            @Override
            public void onResult(List<AppInfo> apps) {
                if (apps != null) {
                    int size = apps.size();
                    //如果是加载更多的情形，就不更新Top3视图
                    if (refresh) {
                        //更新Top3视图
                        listView.setAdapter(null);
                        top3View.notifyDataSetChanged(apps);
                        listView.removeHeaderView(top3View.getView());
                        listView.addHeaderView(top3View.getView());
                    }
                    List<AppInfo> listData = new ArrayList<>();
                    if (size > 3) {
                        listData = refresh ? apps.subList(3, size) : apps;
                    }
                    //更新列表
                    adapter.notifyDataSetChanged(listData, !refresh);
                    listView.setAdapter(adapter);
                }
                int counts = adapter == null ? 0 : adapter.getCount() + 3;
                if (counts < total) {
                    refreshLayout.setLoadMore(true);
                } else {
                    refreshLayout.setLoadMore(false);
                }
                stopLoading();
                refreshLayout.finishRefresh();
                refreshLayout.finishRefreshLoadMore();
            }
        });
        task.execute(appJson);
    }


}
