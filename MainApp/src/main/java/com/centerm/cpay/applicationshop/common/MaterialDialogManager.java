package com.centerm.cpay.applicationshop.common;

import android.content.Context;
import android.support.annotation.NonNull;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.centerm.cpay.applicationshop.R;

/**
 * Created by 王玮 on 2016/6/16.
 */
public class MaterialDialogManager {

    /**
     * 基本提示框
     */
    public static MaterialDialog.Builder getTip(Context context, int resId) {
        return new MaterialDialog.Builder(context).title(R.string.tip).backgroundColorRes(android.R.color.white)
                .titleColorRes(R.color.color_text)
                .content(resId).contentColorRes(R.color.login_text_color)
                .positiveText(R.string.ok);
    }

    /**
     * 基本说明框
     */
    public static MaterialDialog.Builder getComment(Context context, int titleResId, int contentResId) {
        return new MaterialDialog.Builder(context).title(titleResId).backgroundColorRes(android.R.color.white)
                .titleColorRes(R.color.color_text).contentLineSpacing(2)
                .content(contentResId).contentColorRes(R.color.login_text_color)
                .positiveText(R.string.close);
    }

    /**
     * 基本说明框
     */
    public static MaterialDialog.Builder getComment(Context context, int titleResId, String content) {
        return new MaterialDialog.Builder(context).title(titleResId).backgroundColorRes(android.R.color.white)
                .titleColorRes(R.color.color_text).contentLineSpacing(2)
                .content(content).contentColorRes(R.color.login_text_color)
                .positiveText(R.string.close);
    }

    /**
     * 选择框
     */
    public static MaterialDialog.Builder getChooser(Context context, int resId) {
        return new MaterialDialog.Builder(context).title(R.string.tip).backgroundColorRes(android.R.color.white)
                .titleColorRes(R.color.color_text)
                .content(resId).contentColorRes(R.color.login_text_color)
                .positiveText(R.string.ok).negativeText(R.string.no);
    }

    public static MaterialDialog getChooser(Context context, int resId, final ButtonClick click) {
        MaterialDialog.Builder builder = new MaterialDialog.Builder(context)
                .backgroundColorRes(android.R.color.white)
                .content(resId).contentColorRes(R.color.color_text)
                .positiveText(R.string.ok).negativeText(R.string.no).onPositive(new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                        click.onClick();
                    }
                });
        MaterialDialog dialog = builder.build();
        dialog.getActionButton(DialogAction.POSITIVE).setTextSize(18);
        dialog.getContentView().setTextSize(16);
        dialog.getActionButton(DialogAction.NEGATIVE).setTextSize(18);
        return dialog;
    }

    public static MaterialDialog getChooserByButton(Context context, int resId, int sureResId, final ButtonClick click) {
        MaterialDialog.Builder builder = new MaterialDialog.Builder(context)
                .backgroundColorRes(android.R.color.white)
                .content(resId).contentColorRes(R.color.color_text)
                .positiveText(sureResId).negativeText(R.string.no).onPositive(new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                        click.onClick();
                    }
                });
        MaterialDialog dialog = builder.build();
        dialog.getActionButton(DialogAction.POSITIVE).setTextSize(18);
        dialog.getContentView().setTextSize(16);
        dialog.getActionButton(DialogAction.NEGATIVE).setTextSize(18);
        return dialog;
    }

    public static MaterialDialog getChooserByTips(Context context, int resId, final ButtonClick click) {
        MaterialDialog.Builder builder = new MaterialDialog.Builder(context)
                .title(R.string.tip).backgroundColorRes(android.R.color.white)
                .titleColorRes(R.color.color_text)
                .content(resId).contentColorRes(R.color.login_text_color)
                .positiveText(R.string.ok).negativeText(R.string.no).onPositive(new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                        click.onClick();
                    }
                });
        MaterialDialog dialog = builder.build();
        dialog.getActionButton(DialogAction.POSITIVE).setTextSize(18);
        dialog.getContentView().setTextSize(16);
        dialog.getActionButton(DialogAction.NEGATIVE).setTextSize(18);
        return dialog;
    }

    public interface ButtonClick {
        public void onClick();
    }

}
