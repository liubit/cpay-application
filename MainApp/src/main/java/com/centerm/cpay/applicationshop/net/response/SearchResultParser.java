package com.centerm.cpay.applicationshop.net.response;


import android.content.Context;

import com.centerm.cpay.applicationshop.bean.AppInfo;
import com.centerm.cpay.applicationshop.download.AppInfoSync;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import static com.centerm.cpay.applicationshop.net.cont.JsonKey.apps;

/**
 * author: linwanliang</br>
 * date:2016/7/12</br>
 */
public class SearchResultParser extends AppListParser {

    private int total;

    public SearchResultParser(Context context, String response) {
        super(context, response);
        JSONObject bodyJson = getBody();
        if (bodyJson == null) {
            return;
        }
        try {
            if (bodyJson.has(apps)) {
                appsJson = bodyJson.getJSONArray(apps);
                appList = new ArrayList<>();
                for (int i = 0; i < appsJson.length(); i++) {
                    AppInfo item = AppInfo.convert(appsJson.getJSONObject(i));
                    AppInfoSync.sync(context, item);
                    appList.add(item);
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        total = getAppList() == null ? 0 : getAppList().size();
    }

    @Override
    public int getTotal() {
        return total;
    }

    /* public SearchResultParser(String response) {
        super(response);
        appList = new ArrayList<>();
        JSONObject body = getBody();
        try {
            if (body.has(JsonKey.apps) && body.get(JsonKey.apps) != null) {
                JSONArray array = body.getJSONArray(JsonKey.apps);
                if (array == null || array.length() == 0) {
                    return;
                }
                for (int i = 0; i < array.length(); i++) {
                    JSONObject item = array.getJSONObject(i);
                    AppInfo app = AppInfo.convert(item);
                    appList.add(app);
                }
                total = appList.size();
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }*/

}
