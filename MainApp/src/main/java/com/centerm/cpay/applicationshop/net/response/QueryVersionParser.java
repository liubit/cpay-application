package com.centerm.cpay.applicationshop.net.response;

import com.centerm.cpay.appcloud.remote.VersionInfo;
import com.centerm.cpay.applicationshop.net.BaseParser;
import com.centerm.cpay.applicationshop.net.cont.JsonKey;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * author: linwanliang</br>
 * date:2016/7/2</br>
 */
public class QueryVersionParser extends BaseParser {
    private int total;
    private List<VersionInfo> versionList;

    public QueryVersionParser(String response) {
        super(response);
        JSONObject body = getBody();
        try {
//            total = Integer.valueOf(body.getString(JsonKey.totalNumber));
            if (body != null && body.has(JsonKey.apps)) {
                versionList = new ArrayList<VersionInfo>();
                JSONArray array = body.getJSONArray(JsonKey.apps);
                for (int i = 0; i < array.length(); i++) {
                    JSONObject obj = array.getJSONObject(i);
                    VersionInfo item = VersionInfo.convert2(obj);
                    versionList.add(item);
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

    public int getTotal() {
        return total;
    }

    public List<VersionInfo> getVersionList() {
        return versionList;
    }
}
