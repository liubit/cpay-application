package com.centerm.cpay.applicationshop.base;

import android.annotation.TargetApi;
import android.app.Activity;
import android.app.ActivityOptions;
import android.content.Intent;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.os.Build;
import android.os.Bundle;
import android.view.View;

import com.centerm.cpay.applicationshop.db.DbHelper;
import com.centerm.cpay.applicationshop.net.DefaultHttpClient;
import com.centerm.cloudsys.sdk.common.log.Log4d;
import com.j256.ormlite.android.apptools.OpenHelperManager;

/**
 * Created by linwanliang on 2016/6/23.
 */
public abstract class BaseActivity extends Activity implements IComponentCreator {
    protected static Log4d logger = Log4d.getDefaultInstance();
    private DbHelper dbHelper;
    protected DefaultHttpClient httpClient;

    public BaseActivity() {
        httpClient = DefaultHttpClient.getInstance();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        int layoutId = onInitLayoutId();
        if (layoutId > 0) {
            setContentView(layoutId);
        }
        onInitLocalData();
        onInitView(getWindow().getDecorView());
        doThingsAfterInitView();
    }

    @Override
    public void doThingsAfterInitView() {
    }

    /**
     * 获取数据库操作帮助对象
     *
     * @return 数据库操作帮助对象
     */
    protected DbHelper getDbHelper() {
        if (dbHelper == null) {
            dbHelper = OpenHelperManager.getHelper(this, DbHelper.class);
        }
        return dbHelper;
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (dbHelper != null) {
            OpenHelperManager.releaseHelper();
            dbHelper = null;
        }
    }


    public void jumpTo(Intent intent, boolean finishSelf) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            jump_v21(intent);
        } else {
            jump(intent);
        }
        if (finishSelf) {
            finish();
        }
    }

    protected void jumpTo(Intent intent) {
        jumpTo(intent, false);
    }

    protected void jumpTo(Intent intent, View view, String sharedName) {
        jumpTo(intent, view, sharedName, false);
    }

    protected void jumpTo(Intent intent, View view, String sharedName, boolean finishSelf) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            jumpWithSharedElement(intent, view, sharedName);
        } else {
            jump(intent);
        }
        if (finishSelf) {
            finish();
        }
    }


    private void jump(Intent intent) {
        startActivity(intent);
    }


    @TargetApi(value = Build.VERSION_CODES.LOLLIPOP)
    private void jump_v21(Intent intent) {
        startActivity(intent, ActivityOptions.makeSceneTransitionAnimation(this).toBundle());
    }

    @TargetApi(value = Build.VERSION_CODES.LOLLIPOP)
    private void jumpWithSharedElement(Intent intent, View view, String sharedName) {
        startActivity(intent, ActivityOptions.makeSceneTransitionAnimation(this, view, sharedName).toBundle());
    }

    @Override
    public Resources getResources() {
        Resources res = super.getResources();
        Configuration config = new Configuration();
        config.setToDefaults();
        res.updateConfiguration(config, res.getDisplayMetrics());
        return res;
    }
}
