package com.centerm.cpay.applicationshop.web;

import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.Handler;
import android.os.Message;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.webkit.JavascriptInterface;
import android.webkit.WebView;

import com.centerm.cpay.applicationshop.R;
import com.centerm.cpay.applicationshop.activity.AppDetailActivity;
import com.centerm.cpay.applicationshop.bean.H5Bean;
import com.centerm.cpay.applicationshop.common.DialogFactory;
import com.centerm.cpay.applicationshop.common.MaterialDialogManager;
import com.centerm.cpay.applicationshop.download.AdApkDownloadManager;
import com.centerm.cpay.applicationshop.download.ApkDownloadManager;
import com.centerm.cpay.applicationshop.download.DownloadTools;
import com.centerm.cpay.applicationshop.utils.PackageUtils;
import com.centerm.cpay.applicationshop.utils.ViewUtils;
import com.centerm.cpay.applicationshop.view.LoadingDialog;
import com.centerm.cpay.applicationshop.view.MyAlertDialog;
import com.google.gson.Gson;

import java.io.File;
import java.io.IOException;
import java.util.List;

/**
 * WebView与Js交互函数
 */
public class WebView4JSFuaction {
    private Handler handler;
    private Context context;
    private WebView webView;
    private LoadingDialog loadingDialog;

    public WebView4JSFuaction(Context context, WebView webView) {
        this.context = context;
        this.webView = webView;
        initHandler();
    }

    private void initHandler() {
        handler = new Handler() {
            @Override
            public void handleMessage(Message msg) {
                String json = (String) msg.obj;
                Gson gson = new Gson();
                H5Bean jsBean = gson.fromJson(json, H5Bean.class);
                int action = jsBean.getAction();
                switch (action) {
                    case 1://打开APP
                        if (!com.centerm.cloudsys.sdk.common.utils.PackageUtils.openApp(context, jsBean.getPackageName())) {
                            jumpDetail(jsBean.getPackageName());
                        }
                        break;
                    case 2:
                        // 打开指定页面
                        PackageUtils.openActivity(
                                context, jsBean.getPackageName(), jsBean.getActivityName());
                        break;
                    case 3:
                        //跳转到URL
                        webView.loadUrl(jsBean.getUrl());
                        break;
                    case 4:
                        downloadApk(jsBean);
                        break;
                    case 5://详情页下载
                        jumpDetail(jsBean.getPackageName());
                        break;
                }
            }
        };
    }

    public void jumpDetail(String packageName) {
        Intent intent = new Intent(context, AppDetailActivity.class);
        intent.putExtra("packageName", packageName);
        context.startActivity(intent);
    }

    /**
     * 下载apk
     *
     * @param bean
     */
    private void downloadApk(H5Bean bean) {
        final String packageName = bean.getPackageName();
        final String apkUrl = bean.getUrl();
        if (TextUtils.isEmpty(apkUrl)) {
            MaterialDialogManager.getTip(context, R.string.js_download_invalid).show();
        } else if (!TextUtils.isEmpty(packageName)) {
            if (isAvilible(packageName)) {//本地已经存在
                MaterialDialogManager.getChooserByButton(context,
                        R.string.js_installed, R.string.label_open,
                        new MaterialDialogManager.ButtonClick() {
                            @Override
                            public void onClick() {
                                com.centerm.cloudsys.sdk.common.utils.PackageUtils.openApp(context, packageName);
                            }
                        }).show();
            } else {//开始下载
                AdApkDownloadManager.getInstance().setDownLoadCallBack(new AdApkDownloadManager.DownloadCallBack() {
                    @Override
                    public void onStartDownLoad() {
//                        ViewUtils.showToast(context, "开始下载");
                        loadingDialog = new LoadingDialog(context);
                        loadingDialog.getText().setText(R.string.js_downloading);
                        loadingDialog.getText().setVisibility(View.VISIBLE);
                        loadingDialog.setCanceledOnTouchOutside(true);
                        loadingDialog.show();
                    }
                    @Override
                    public void onProgress(String progress) {
                        if (loadingDialog != null) {
                            loadingDialog.getText().setText(context
                                    .getText(R.string.js_downloading_p) + progress);
                        }
                    }

                    @Override
                    public void onResult(String fileUrl) {
                        if (loadingDialog != null) {
                            loadingDialog.dismiss();
                        }
                        com.centerm.cloudsys.sdk.common.utils.PackageUtils.installApp
                                (context, fileUrl);
                    }

                    @Override
                    public void onFail(String msg) {
                        ViewUtils.showToast(context, msg);
                        if (loadingDialog != null) {
                            loadingDialog.dismiss();
                        }
                    }
                });
                boolean isDownLoad = AdApkDownloadManager.getInstance().downloadApk(context, apkUrl);
                if (!isDownLoad) {
                    MaterialDialogManager.getChooserByButton(context,
                            R.string.js_downloaded, R.string.label_install,
                            new MaterialDialogManager.ButtonClick() {
                                @Override
                                public void onClick() {
                                    com.centerm.cloudsys.sdk.common.utils.PackageUtils.installApp
                                            (context, DownloadTools.getAdFile(apkUrl).getAbsolutePath());
                                }
                            }).show();
                }
            }
        }
    }

    private boolean isAvilible(String packageName) {
        PackageManager packageManager = context.getPackageManager();
        //获取手机系统的所有APP包名，然后进行一一比较
        List<PackageInfo> pinfo = packageManager.getInstalledPackages(0);
        for (int i = 0; i < pinfo.size(); i++) {
            if (((PackageInfo) pinfo.get(i)).packageName
                    .equalsIgnoreCase(packageName))
                return true;
        }
        return false;
    }

    @JavascriptInterface
    public void doAction(String result) throws IllegalArgumentException,
            IllegalStateException, IOException {
        if (!TextUtils.isEmpty(result)) {
            Message msg = Message.obtain();
            msg.obj = result;
            handler.sendMessage(msg);
        }

    }

}
