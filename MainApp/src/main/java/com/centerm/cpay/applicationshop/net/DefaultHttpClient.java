package com.centerm.cpay.applicationshop.net;

import android.content.Context;
import android.os.RemoteException;

import com.centerm.cpay.applicationshop.MyApplication;
import com.centerm.cpay.applicationshop.net.cont.JsonKey;
import com.centerm.cpay.applicationshop.net.cont.Method;
import com.centerm.cloudsys.sdk.common.log.Log4d;
import com.centerm.cloudsys.sdk.common.utils.NetUtils;
import com.centerm.cpay.securitysuite.aidl.IVirtualPinPad;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.MySSLSocketFactory;

import org.apache.http.Header;
import org.apache.http.conn.ssl.SSLSocketFactory;
import org.apache.http.entity.StringEntity;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.security.KeyStore;
import java.util.HashSet;
import java.util.Set;

import config.Config;

/**
 * Created by linwanliang on 2016/6/21.
 *
 */
public class DefaultHttpClient {

    private final static Log4d LOGGER = Log4d.getDefaultInstance();
    private final static String TAG = "HttpClient";

    private AsyncHttpClient innerClient;
    private static DefaultHttpClient instance;
    private static boolean isHttps = true;
    private NetCacheManager cacheManager;
    private Set<String> cacheTags;

    private DefaultHttpClient() {
        innerClient = new AsyncHttpClient();
        int timeout = Config.NET.DEFAULT_TIMEOUT;
        innerClient.setTimeout(timeout);
        cacheManager = NetCacheManager.getInstance();

        cacheTags = new HashSet<>();
        cacheTags.add(Method.GET_AD_LIST);
        cacheTags.add(Method.GET_APP_TYPE_LIST);
//        cacheTags.add(Method.GET_APP_LIST);
    }


    public static DefaultHttpClient getInstance() {
        if (instance == null) {
            synchronized (DefaultHttpClient.class) {
                if (instance == null) {
                    instance = new DefaultHttpClient();
                }
            }
        }
        return instance;
    }

    public boolean post(final Context context, final BaseRequest request, final ResponseHandler handler) {
        String addr = Config.getNetAddress(context);
        if (addr == null || addr.trim().isEmpty()) {
            LOGGER.warn(TAG, request.getMethod() + "请求失败，未配置通讯地址！");
            return false;
        }
        if (context == null || request == null) {
            LOGGER.warn(TAG, "请求失败，参数错误！");
            return false;
        }
        Context appContext = context.getApplicationContext();
        if (isHttps()) {
            innerClient.setSSLSocketFactory(createSSLSocketFactory());
        } else {
            innerClient.setSSLSocketFactory(null);
        }
        AsyncHttpResponseHandler innerHandler = new AsyncHttpResponseHandler() {
            @Override
            public void onSuccess(int i, Header[] headers, byte[] bytes) {
                String response = "";
                try {
                    response = new String(bytes, Config.NET.getCharset());
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                }
                LOGGER.debug(TAG, "[请求成功]，" + request.getMethod() + "：" + i);
                LOGGER.debug(TAG, "[请求参数]，" + request.getParams().toString());
                LOGGER.debug(TAG, "[返回报文]，" + response);
                boolean mac = false;
                mac = checkMAC(context, response);
                if (handler != null) {
                    if (mac) {
                        //写入缓存
                        if (Config.CACHE && cacheTags.contains(request.getMethod())) {
                            BaseParser parser = new BaseParser(response);
                            if (parser.hasData()) {
                                cacheManager.write(context, request.getMethod(), response);
                            }
                        }
                        handler.onSuccess(response);
                    } else {
                        LOGGER.warn(TAG, "报文MAC校验失败");
                        handler.onFailure(-2, "MAC校验失败");
                    }
                } else {
                    LOGGER.warn(TAG, "接收器为空，不处理网络返回报文");
                }
            }

            @Override
            public void onFailure(int i, Header[] headers, byte[] bytes, Throwable throwable) {
                String method = request.getMethod();
                LOGGER.warn(TAG, "[请求失败]，" + method + "：" + i);
                LOGGER.debug(TAG, "[请求参数]，" + request.getParams().toString());
                //读取缓存
                if (Config.CACHE && cacheTags.contains(method)) {
                    LOGGER.debug(TAG, method + "，尝试读取缓存...");
                    String cache = cacheManager.read(context, method);
                    if (cache != null && handler != null) {
                        LOGGER.info(TAG, method + "，读取缓存成功");
                        handler.onSuccess(cache);
                        return;
                    }
                }
                if (throwable != null) {
                    LOGGER.error(TAG, "[异常信息]：" + throwable.toString());
                }
                if (handler != null) {
                    LOGGER.error(TAG, "[异常信息,错误码]：" + i);
                    handler.onFailure(i, "网络异常");
//                    handler.onFailure(i, "服务器异常，错误码" + i);
                }
            }
        };
        String params = request.getParams().toString();
        String url = addr + request.getMethod();
        LOGGER.debug(TAG, "[发出请求]：" + url);
//        LOGGER.debug(TAG, "[请求参数]：" + params);
        StringEntity entity;
        entity = new StringEntity(params, Config.NET.getCharset());
        innerClient.post(appContext, url, entity, "application/json", innerHandler);
        return true;
    }

    private SSLSocketFactory createSSLSocketFactory() {
        MySslSocketFactory sf = null;
        try {
            KeyStore trustStore = KeyStore.getInstance(KeyStore.getDefaultType());
            trustStore.load(null, null);
            sf = new MySslSocketFactory(trustStore);
            sf.setHostnameVerifier(MySSLSocketFactory.ALLOW_ALL_HOSTNAME_VERIFIER);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return sf;
    }

    public static boolean isHttps() {
        return isHttps;
    }

    public static void setUseHttps(boolean isHttps) {
        DefaultHttpClient.isHttps = isHttps;
    }

    public boolean checkMAC(Context context, String response) {
        if (Config.DEMO || !Config.CHECK_MAC) {
            return true;
        }
        try {
            JSONObject json = new JSONObject(response);
            String mac = json.getString(JsonKey.MAC);
            IVirtualPinPad pinPad = MyApplication.getMacService(context);
            if (pinPad != null) {
                try {
                    String localMac = pinPad.calculateMac(Config.MAC_KEY_INDEX, response);
                    LOGGER.debug(TAG, "LocalMac：" + localMac + "  ResponseMac：" + mac);
                    return mac.equals(localMac);
                } catch (RemoteException e) {
                    e.printStackTrace();
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return false;
    }


}
