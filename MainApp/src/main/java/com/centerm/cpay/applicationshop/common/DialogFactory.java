package com.centerm.cpay.applicationshop.common;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.widget.EditText;

import com.centerm.cpay.applicationshop.R;
import com.centerm.cpay.applicationshop.view.MyAlertDialog;

import config.Config;

/**
 * Created by linwanliang on 2016/6/22.
 * 对话框工厂
 */
public class DialogFactory {

    public static Dialog alertDialog;

    public static void showAlertWithTwoButton(Context context, String message
            , MyAlertDialog.ButtonClickListener listener) {
        if (alertDialog != null && alertDialog.isShowing()) {
            alertDialog.dismiss();
        }
        alertDialog = null;
        alertDialog = new MyAlertDialog(context);
        ((MyAlertDialog) alertDialog).setMessage(message);
        ((MyAlertDialog) alertDialog).setPositiveClick(context.getResources().getString(R.string.ok), listener);
        alertDialog.show();
    }

    public static void showAlert(Context context, String message
            , MyAlertDialog.ButtonClickListener listener) {
        if (alertDialog != null && alertDialog.isShowing()) {
            alertDialog.dismiss();
        }
        alertDialog = null;
        alertDialog = new MyAlertDialog(context);
        ((MyAlertDialog) alertDialog).setMessage(message);
        ((MyAlertDialog) alertDialog).setPositiveClick(context.getResources().getString(R.string.ok), listener);
        ((MyAlertDialog) alertDialog).setNegativeClick(context.getResources().getString(R.string.cancel), listener);
        alertDialog.show();
    }

    public static void showAlertWithTwoButton(Context context, String message, String btnMsg
            , MyAlertDialog.ButtonClickListener listener) {
        if (alertDialog != null && alertDialog.isShowing()) {
            alertDialog.dismiss();
        }
        alertDialog = null;
        alertDialog = new MyAlertDialog(context);
        ((MyAlertDialog) alertDialog).setMessage(message);
        ((MyAlertDialog) alertDialog).setPositiveClick(btnMsg, listener);
        alertDialog.show();
    }

    public static void hideAll() {
        if (alertDialog != null && alertDialog.isShowing()) {
            alertDialog.dismiss();
        }
        alertDialog = null;
    }

//    public static void showServerAddressDialog(Context context) {
//        final EditText inputServer = new EditText(context);
//        AlertDialog.Builder builder = new AlertDialog.Builder(context);
//        builder.setTitle("设置服务器地址").setIcon(android.R.drawable.ic_dialog_info).setView(inputServer)
//                .setNegativeButton("取消", null);
//        builder.setPositiveButton("确认", new DialogInterface.OnClickListener() {
//
//            public void onClick(DialogInterface dialog, int which) {
//                String address = inputServer.getText().toString();
//                Config.NET.setAddress(address);
//            }
//        });
//        builder.show();
//    }
}
