package com.centerm.cpay.applicationshop.net.cont;

/**
 * Created by linwanliang on 2016/6/22.
 */
public class Method {
    //获取应用类别列表
    public final static String GET_APP_TYPE_LIST = "/ams/apkType/query";
    //获取应用列表
    public final static String GET_APP_LIST = "/ams/app/queryList";
    //获取应用详情
    public final static String GET_APP_DETAIL = "/ams/app/query";
    //搜索应用
    public final static String SEARCH_APP = "/ams/app/search";
    //获取应用版本信息
    public final static String GET_APP_VERSIONS = "/ams/app/version";
    //获取广告列表
    public final static String GET_AD_LIST = "/ams/advert/query";
    //获取不可卸载的应用列表
    public final static String UNINSTALL_CHECK = "/ams/app/uninstallCheck";
    //获取搜索热词
    public final static String GET_HOT_WORDS = "/ams/hotWords/query";


}
