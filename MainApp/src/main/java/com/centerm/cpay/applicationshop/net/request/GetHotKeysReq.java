package com.centerm.cpay.applicationshop.net.request;

import android.content.Context;

import com.centerm.cpay.applicationshop.net.BaseRequest;
import com.centerm.cpay.applicationshop.net.cont.Method;

import org.json.JSONObject;

/**
 * author: linwanliang</br>
 * date:2016/7/12</br>
 */
public class GetHotKeysReq extends BaseRequest {
    public GetHotKeysReq(Context context) {
        super(context);
        setMethod(Method.GET_HOT_WORDS);
    }
}
