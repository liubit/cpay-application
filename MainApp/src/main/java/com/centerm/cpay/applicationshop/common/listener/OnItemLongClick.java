package com.centerm.cpay.applicationshop.common.listener;

import android.view.View;

/**
 * Created by linwanliang on 2016/6/27.
 */
public interface OnItemLongClick {
    void onLongClick(View itemView, int position);
}
