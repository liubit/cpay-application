package com.centerm.cpay.applicationshop.adapter;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.centerm.cpay.applicationshop.R;
import com.centerm.cpay.applicationshop.bean.AppInfo;
import com.centerm.cpay.applicationshop.bean.AppType;
import com.centerm.cpay.applicationshop.view.TypeAppPreviewView;
import com.nostra13.universalimageloader.core.ImageLoader;

import java.util.List;
import java.util.Map;

import config.Config;

/**
 * Created by linwanliang on 2016/6/25.
 */
public class AppTypeListAdapter extends BaseAdapter {
    private final static int RESOURCE_ID = R.layout.main_sort_list_item;
    private Context context;
    private List<AppType> typeList;
    private Map<String, List<AppInfo>> idMapApps;

    public AppTypeListAdapter(Context context, Map<String, List<AppInfo>> idMapApps, List<AppType> typeList) {
        this.context = context;
        this.idMapApps = idMapApps;
        this.typeList = typeList;
    }

    @Override
    public int getCount() {
        return context == null ? 0 : (typeList == null ? 0 : typeList.size());
    }

    @Override
    public Object getItem(int position) {
        return typeList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        AppType type = typeList.get(position);
        List<AppInfo> apps = idMapApps.get(type.getId());
        if (convertView == null) {
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(RESOURCE_ID, null);
            ViewBinder binder = ViewBinder.bind(convertView, type, apps);
            binder.reset();
        } else {
            ViewBinder binder = (ViewBinder) convertView.getTag();
            binder.reset(type, apps);
        }
        return convertView;
    }

    public void notifyDataSetChanged(List<AppType> types, Map<String, List<AppInfo>> apps) {
        if (typeList == null) {
            typeList = types;
        } else {
            typeList.clear();
            typeList.addAll(types);
        }
        if (idMapApps == null) {
            idMapApps = apps;
        } else {
            idMapApps.clear();
            idMapApps.putAll(apps);
        }
        notifyDataSetChanged();
    }

    private static class ViewBinder {
        private AppType type;
        private List<AppInfo> apps;
        private ImageView iconShow;
        private TextView nameShow;
        private TypeAppPreviewView preView;

        private ViewBinder(View view, AppType type, List<AppInfo> apps) {
            view.setTag(this);
            this.apps = apps;
            this.type = type;
            iconShow = (ImageView) view.findViewById(R.id.type_icon_show);
            nameShow = (TextView) view.findViewById(R.id.app_name_show);
            preView = (TypeAppPreviewView) view.findViewById(R.id.app_pre_view);
        }

        public static ViewBinder bind(View view, AppType type, List<AppInfo> apps) {
            ViewBinder binder = new ViewBinder(view, type, apps);
            view.setTag(binder);
            return binder;
        }

        public void reset() {
            ImageLoader.getInstance().displayImage(type.getIconUrl(), iconShow, Config.TYPE_ICON_OPTIONS);
            nameShow.setText(type.getName());
            preView.updateItems(type, apps);
        }

        public void reset(AppType type, List<AppInfo> apps) {
            this.type = type;
            this.apps = apps;
            reset();
        }
    }
}
