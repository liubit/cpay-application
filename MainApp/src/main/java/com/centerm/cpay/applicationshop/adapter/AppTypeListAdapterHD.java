package com.centerm.cpay.applicationshop.adapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.SimpleAdapter;
import android.widget.TextView;

import com.centerm.cloudsys.sdk.common.utils.MD5Utils;
import com.centerm.cpay.applicationshop.R;
import com.centerm.cpay.applicationshop.bean.AppInfo;
import com.centerm.cpay.applicationshop.bean.AppType;
import com.centerm.cpay.applicationshop.common.BroadcastAction;
import com.centerm.cpay.applicationshop.common.KeyName;
import com.nostra13.universalimageloader.core.ImageLoader;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import config.Config;

/**
 * Created by liubit on 2016/12/5.
 */

public class AppTypeListAdapterHD extends BaseAdapter {
    private final static int RESOURCE_ID = R.layout.main_sort_list_item_hd;
    private Context context;
    private List<AppType> typeList;
    private Map<String, List<AppInfo>> idMapApps;

    public AppTypeListAdapterHD(Context context, Map<String, List<AppInfo>> idMapApps, List<AppType> typeList) {
        this.context = context;
        this.idMapApps = idMapApps;
        this.typeList = typeList;
    }

    @Override
    public int getCount() {
        return context == null ? 0 : (typeList == null ? 0 : typeList.size());
    }

    @Override
    public Object getItem(int position) {
        return typeList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        AppType type = typeList.get(position);
        List<AppInfo> apps = idMapApps.get(type.getId());
        if (convertView == null) {
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(RESOURCE_ID, null);
            AppTypeListAdapterHD.ViewBinder binder = AppTypeListAdapterHD.ViewBinder.bind(convertView, type, apps, context);
            binder.reset();
        } else {
            AppTypeListAdapterHD.ViewBinder binder = (AppTypeListAdapterHD.ViewBinder) convertView.getTag();
            binder.reset(type, apps);
        }
        return convertView;
    }

    public void notifyDataSetChanged(List<AppType> types, Map<String, List<AppInfo>> apps) {
        if (typeList == null) {
            typeList = types;
        } else {
            typeList.clear();
            typeList.addAll(types);
        }
        if (idMapApps == null) {
            idMapApps = apps;
        } else {
            idMapApps.clear();
            idMapApps.putAll(apps);
        }
        notifyDataSetChanged();
    }

    private static class ViewBinder {
        private AppType type;
        private List<AppInfo> apps;
        private ImageView iconShow;
        private TextView nameShow;
        private GridView mGridViewSort;
        private SimpleAdapter simpleAdapter;

        private ViewBinder(View view, final AppType type, final List<AppInfo> apps, final Context context) {
            view.setTag(this);
            this.apps = apps;
            this.type = type;
            iconShow = (ImageView) view.findViewById(R.id.type_icon_show);
            nameShow = (TextView) view.findViewById(R.id.app_name_show);
            mGridViewSort = (GridView) view.findViewById(R.id.mGridViewSort);

            AppInfo more = new AppInfo();
            more.setName("更多");
            more.setPackageName(getMoreFlag());
            apps.add(more);

            List<HashMap<String, Object>> imagelist = new ArrayList<>();
            for(AppInfo info : apps){
                HashMap<String, Object> map = new HashMap<>();
                map.put("title", info.getName());
                imagelist.add(map);
            }

            simpleAdapter = new SimpleAdapter(context, imagelist, R.layout.main_type_app_item, new String[]{"title"},new int[]{R.id.title});
            mGridViewSort.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                    AppInfo info = apps.get(i);
                    String pkgName = info.getPackageName();
                    if (getMoreFlag().equals(pkgName)) {
                        Intent intent = new Intent();
                        intent.setAction(BroadcastAction.ACTION_OPEN_MORE_APP);
                        intent.putExtra(KeyName.APP_TYPE, type);
                        context.sendBroadcast(intent);
                    } else {
                        Intent intent = new Intent();
                        intent.setAction(BroadcastAction.ACTION_OPEN_APPDETAIL);
                        intent.putExtra(KeyName.APP_INFO, info);
                        context.sendBroadcast(intent);
                    }
                }
            });

        }

        public static AppTypeListAdapterHD.ViewBinder bind(View view, AppType type, List<AppInfo> apps,Context context) {
            AppTypeListAdapterHD.ViewBinder binder = new AppTypeListAdapterHD.ViewBinder(view, type, apps,context);
            view.setTag(binder);
            return binder;
        }

        public void reset() {
            ImageLoader.getInstance().displayImage(type.getIconUrl(), iconShow, Config.TYPE_ICON_OPTIONS);
            nameShow.setText(type.getName());
            mGridViewSort.setAdapter(simpleAdapter);
        }

        public void reset(AppType type, List<AppInfo> apps) {
            this.type = type;
            this.apps = apps;
            reset();
        }

        public String getMoreFlag() {
            return MD5Utils.getMD5Str("moremoremore");
        }
    }



}
