package com.centerm.cpay.applicationshop.activity;


import android.content.Intent;
import android.os.Handler;
import android.os.Message;
import android.view.KeyEvent;
import android.view.View;

import com.centerm.cpay.applicationshop.R;
import com.centerm.cpay.applicationshop.base.BaseActivity_v21;
import com.centerm.cpay.applicationshop.common.AppSettings;
import com.centerm.cpay.applicationshop.common.KeyName;
import com.centerm.cpay.applicationshop.net.ResponseHandler;
import com.centerm.cpay.applicationshop.net.request.GetTypeListReq;

import java.util.Timer;
import java.util.TimerTask;

import config.Config;

/**
 * Created by linwanliang on 2016/6/21.
 * 启动界面，在改界面完成一些数据初始化操作，还需要请求分类数据。
 */
public class LaunchActivity extends BaseActivity_v21 {
    private final static String TAG = LaunchActivity.class.getSimpleName();
    private final static int QUERY_DEV_MASK_INTERVAL = 800;
    private final static int MAX_QUERY_TIMES = 5;
    private final static int TOTAL_TASKS = 2;
//    private NumberProgressBar progressBar;
//    private TextView progressNotice;


    private int queryTimes = 0;//查询终端掩码次数
    private Integer finishTaskCounts = 0;//初始化任务完成的数量
    private long launchTime = 0;//开始启动的时间
    private MyHandler myHandler;
    private TimerTask devMaskTask;
    private Timer timer;
    private String typeResponse;

    @Override
    public int onInitLayoutId() {
        return R.layout.activity_launch;
    }

    @Override
    public void onInitLocalData() {
        myHandler = new MyHandler(this);
        queryTimes = 0;
        finishTaskCounts = 0;
        launchTime = System.currentTimeMillis();
        initMidLayerService();
    }

    @Override
    public void onInitView(View view) {
//        progressBar = (NumberProgressBar) view.findViewById(R.id.launcher_progressbar);
//        progressNotice = (TextView) view.findViewById(R.id.launcher_notice);
//        progressBar.setOnProgressBarListener(new OnProgressBarListener() {
//            @Override
//            public void onProgressChange(int current, int max) {
//                if (current >= 95 && current < max) {
//                    progressNotice.setText("正在启动...");
//                }
////                if (current == max) {
////                    myHandler.sendEmptyMessage(1);
////                }
//            }
//        });
    }

    @Override
    public void doThingsAfterInitView() {

//        progressNotice.setText("正在加载本地信息...");

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (devMaskTask != null) {
            devMaskTask.cancel();
            devMaskTask = null;
        }
        if (timer != null) {
            timer.cancel();
            timer = null;
        }
        System.gc();
    }

    @Override
    public boolean dispatchKeyEvent(KeyEvent event) {
        if (event.getKeyCode() == KeyEvent.KEYCODE_BACK) {
            return true;
        }
        return super.dispatchKeyEvent(event);
    }

//    public void setProgressBar(final int progress) {
//        runOnUiThread(new Runnable() {
//            @Override
//            public void run() {
//                progressBar.incrementProgressBy(progress);
//            }
//        });
//    }

    private void initMidLayerService() {
//        setProgressBar(20);
        timer = new Timer();
        //初始化终端掩码数据
        devMaskTask = new TimerTask() {
            @Override
            public void run() {
                logger.info(TAG, "正在初始化终端掩码");
                queryTimes++;
                String mask = AppSettings.getDevMask(LaunchActivity.this);
                if (mask == null) {
                    if (queryTimes >= MAX_QUERY_TIMES) {
                        logger.warn(TAG, "初始化终端掩码失败，跳过该步骤");
                        myHandler.sendEmptyMessage(2);
                        this.cancel();
                        timer.cancel();
                    } else {
                        logger.debug(TAG, "初始化终端掩码失败，尝试重新初始化...");
                    }
                } else {
                    logger.info(TAG, "初始化终端掩码成功！");
                    this.cancel();
                    timer.cancel();
                    myHandler.sendEmptyMessage(2);
                }
            }
        };
        timer.schedule(devMaskTask, 200, QUERY_DEV_MASK_INTERVAL);
    }

    private void initAppTypeList() {
//        setProgressBar(10);
        GetTypeListReq request = new GetTypeListReq(this);
        ResponseHandler handler = new ResponseHandler() {
            @Override
            public void onSuccess(String response) {
                typeResponse = response;
                //准备进入下一个界面
                goNext();
            }

            @Override
            public void onFailure(int errorCode, String errorInfo) {
                logger.error(TAG, "获取应用类型列表失败");
                goNext();
            }
        };
        if (Config.DEMO) {
            goNext();
        } else {
            httpClient.post(this, request, handler);
        }
    }

    private void goNext() {
//        setProgressBar(20);
        long now = System.currentTimeMillis();
        if (now - launchTime >= Config.MIN_LAUNCH_STAY_TIME) {
            logger.debug(TAG, "满足最小停留时间，准备去往下一个界面");
//            setProgressBar(50);
            myHandler.sendEmptyMessage(1);
        } else {
            long after = Config.MIN_LAUNCH_STAY_TIME - (now - launchTime);
            TimerTask task = new TimerTask() {
                @Override
                public void run() {
                    myHandler.sendEmptyMessage(1);
                }
            };
//            new Timer().schedule(new TimerTask() {
//                @Override
//                public void run() {
//                    setProgressBar(1);
//                    if (progressBar.getProgress() == 100) {
//                        logger.debug(TAG,"退出计时器");
//                        cancel();
//                    }
//                }
//            }, 0, after / 50);
            new Timer().schedule(task, after);
            logger.debug(TAG, "不满足最小停留时间，延迟" + after + "毫秒去往下一个界面");
        }
    }

    private void notifyStatisticians() {
        synchronized (finishTaskCounts) {
            if (++finishTaskCounts >= TOTAL_TASKS) {
                logger.debug(TAG, "初始化任务已完成");
                //初始化任务已完成，判断界面停留时间是否满足最小要求
                long now = System.currentTimeMillis();
                final Intent intent = new Intent(this, MainActivity.class);
                if (now - launchTime >= Config.MIN_LAUNCH_STAY_TIME) {
                    logger.debug(TAG, "满足最小停留时间，准备去往下一个界面");
                    myHandler.sendEmptyMessage(1);
                } else {
                    long after = Config.MIN_LAUNCH_STAY_TIME - (now - launchTime);
                    TimerTask task = new TimerTask() {
                        @Override
                        public void run() {
                            myHandler.sendEmptyMessage(1);
                        }
                    };
                    new Timer().schedule(task, after);
                    logger.debug(TAG, "不满足最小停留时间，延迟去往下一个界面");
                }
            } else {
                logger.debug(TAG, "初始化任务未完成，继续等待");
            }
        }
    }

    private class MyHandler extends Handler {

        private BaseActivity_v21 activity;

        public MyHandler(BaseActivity_v21 activity) {
            this.activity = activity;
        }

        @Override
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case 1:
                    Intent intent = new Intent(activity, MainActivity.class);
                    if (typeResponse != null) {
                        String str = typeResponse;
                        typeResponse = null;
                        intent.putExtra(KeyName.TYPE_RESPONSE, str);
                    }
//                    startActivity(intent);
//                    finish();
                    activity.jumpTo(intent, false);
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            finish();
                        }
                    }, 2000);
                    break;
                case 2:
                    initAppTypeList();
                    break;
            }
        }
    }

}
