package com.centerm.cpay.applicationshop.download;

import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.text.TextUtils;

import com.centerm.cpay.applicationshop.bean.AppInfo;
import com.centerm.cpay.applicationshop.utils.AdUtils;
import com.centerm.cpay.applicationshop.utils.CommonUtils;
import com.centerm.cpay.applicationshop.utils.FileUtils;
import com.centerm.cpay.applicationshop.utils.ViewUtils;
import com.centerm.cloudsys.sdk.common.log.Log4d;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.FileAsyncHttpResponseHandler;

import org.apache.http.Header;

import java.io.File;

import config.Config;


/**
 * author: linwanliang
 * date:2016/7/28
 * 广告下载管理器
 */
public class AdDownloadManager {
    private final static int MSG_DOWNLOAD_SUCCESS = 0x509;
    private final static int MSG_FILE_CHECK_FAILED = 0x510;
    private final static Log4d logger = Log4d.getDefaultInstance();
    private final static String TAG = AdDownloadManager.class.getSimpleName();

    private static AdDownloadManager instance;
    private AsyncHttpClient innerClient;
    private DownloadCallBack callBack;
    private int responseTimeout = DEFAULT_RESPONSE_TIMEOUT;
    private final static int DEFAULT_RESPONSE_TIMEOUT = 10 * 60 * 1000;// 超时时间10分钟

    private Handler handler;
    private int reDownloadTimes;


    public static AdDownloadManager getInstance() {
        if (instance == null) {
            instance = new AdDownloadManager();
        }
        return instance;
    }

    private AdDownloadManager() {
        innerClient = new AsyncHttpClient();
        // 设置连接超时时间（下载时间）
        innerClient.setConnectTimeout(responseTimeout);
        // 设置连接响应时间
        innerClient.setResponseTimeout(responseTimeout);
    }

    public void setDownLoadCallBack(DownloadCallBack callBack) {
        this.callBack = callBack;
    }

    public boolean downloadAd(Context context, final String url, final String md5Value) {
        if (context == null || url == null || url.trim().isEmpty()) {
            callBack.onFail("参数为空！");
            return false;
        }
        long sdFreeSize = FileUtils.getFreeDiskSpace();
        if (sdFreeSize == -1) {
            callBack.onFail("没有挂载SD卡！");
            return false;
        } else if (sdFreeSize < Config.FILE_DOWNLOADER.MIN_FREE_SIZE) {
            callBack.onFail("SD卡空间不足！");
            return false;
        }
        String fileName = DownloadTools.extractFileNameFromUrl(url);
        if (fileName == null) {
            callBack.onFail("解析文件名失败！");
            return false;
        }
        initHandler(context);
        // 不支持单文件多线程下载
        innerClient.cancelRequestsByTAG(url, true);
        final File file = DownloadTools.getAdFile(url);
        if (file.exists()) {
            unzip(file.getAbsolutePath());
            return false;
        } else {
            FileAsyncHttpResponseHandler innerHandler = new FileAsyncHttpResponseHandler(file, true) {

                @Override
                public void onCancel() {
                    callBack.onFail("取消");
                }

                @Override
                public void onStart() {
                    callBack.onStartDownLoad();//表示开始下载
                }

                @Override
                public void onProgress(long bytesWritten, long tSize) {

                }

                @Override
                public void onSuccess(int statusCode, Header[] headers, final File file) {
                    CommonUtils.FileMd5Callback callback = new CommonUtils.FileMd5Callback() {
                        @Override
                        public void gotMd5(String value) {
                            boolean checkResult = false;
                            if (file != null) {
                                logger.debug(TAG, "【" + file.getName() + "】下载完成，本地MD5：" + value + "，原MD5：" + md5Value);
                            }
                            if (!TextUtils.isEmpty(value) && (value.equals(md5Value) || value.toUpperCase().equals(md5Value) || value.toLowerCase().equals(md5Value))) {
                                checkResult = true;
                            }
                            Message msg = new Message();
                            Bundle bundle = new Bundle();
                            bundle.putString("filePath", file.getAbsolutePath());
                            bundle.putString("url", url);
                            bundle.putString("md5", md5Value);
                            msg.setData(bundle);
                            if (checkResult) {
                                msg.what = MSG_DOWNLOAD_SUCCESS;
                            } else {
                                logger.warn(TAG, "【" + file.getName() + "】文件MD5校验失败");
                                msg.what = MSG_FILE_CHECK_FAILED;
                                if (file.exists()) {
                                    file.delete();
                                }
                            }
                            if (handler != null) {
                                handler.sendMessage(msg);
                            }
                        }
                    };
                    CommonUtils.getMD5(file, callback);
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, Throwable throwable, File file) {
                    callBack.onFail("下载失败，状态码：" + statusCode);
                }
            };
            innerClient.addHeader("Range", "bytes=" + 0 + "-");//太麻烦了，文件小，就懒得断点了
            innerClient.get(context, url, innerHandler).setTag(url);
            return true;
        }
    }

    public void unzip(String url) {
        String unzipUrl = AdUtils.getUnzipDir(url);
        if (new File(unzipUrl).exists()) {
            //存在
            String finalUrl = "file://" + unzipUrl + "/index.html";
            callBack.onResult(finalUrl);
        } else {
            AdUtils.unzip(new File(url), unzipUrl, new AdUtils.ZipResultCallback() {
                @Override
                public void onUnZipFailed() {
                    callBack.onFail("解压失败");
                }

                @Override
                public void onUnZipFinished(String unzipPath) {
                    callBack.onResult("file://" + unzipPath + "/index.html");
                }
            });
        }
    }

    private void initHandler(Context context) {
        if (handler == null && context != null) {
            final Context appContext = context.getApplicationContext();
            handler = new Handler(appContext.getMainLooper()) {
                @Override
                public void handleMessage(Message msg) {
                    super.handleMessage(msg);
                    Bundle bundle = msg.getData();
                    String url = bundle == null ? null : bundle.getString("url");
                    String filePath = bundle == null ? null : bundle.getString("filePath");
                    String md5Value = bundle == null ? null : bundle.getString("md5");
                    if (bundle == null) {
                        logger.warn(TAG, "广告下载完成，校验MD5异常");
                        return;
                    }
                    switch (msg.what) {
                        case MSG_DOWNLOAD_SUCCESS:
                            unzip(filePath);
                            break;
                        case MSG_FILE_CHECK_FAILED:
                            if (reDownloadTimes++ < 5) {
                                downloadAd(appContext, url, md5Value);
                            } else {
                                if (callBack != null) {
                                    callBack.onFail("文件校验失败");
                                }
                            }
                            break;
                    }
                }
            };
        }
    }

    public interface DownloadCallBack {
        void onStartDownLoad();

        void onResult(String fileUrl);

        void onFail(String msg);
    }

}
