package com.centerm.cpay.applicationshop.net.response;

import com.centerm.cpay.applicationshop.net.BaseParser;
import com.centerm.cpay.applicationshop.net.cont.JsonKey;
import com.centerm.cpay.applicationshop.utils.JsonUtils;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * author: linwanliang</br>
 * date:2016/7/10</br>
 */
public class AppDetailParser extends BaseParser {

    private int downloadCounts;
    private String description;
    private String updateInfo;
    private String downloadUrl;
    private String iconUrl;
    private long size;
    private int version;
    private String versionName;
    private String developer;
    private String publicTime;
    private String[] previewImg;
    private String typeName;
    private String keyWords;
    private String fileMd5;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getRate() {
        return rate;
    }

    public void setRate(int rate) {
        this.rate = rate;
    }

    private String name;
    private int rate;

    public AppDetailParser(String response) {
        super(response);
        JSONObject bodys = getBody();
        JSONObject body = null;
        if (bodys == null) return;//这里要做空指针判断
        try {
            body = bodys.getJSONObject(JsonKey.app);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        if (body == null) {
            return;
        }
        name = JsonUtils.getString(body, JsonKey.name);
        rate = JsonUtils.getInt(body, JsonKey.rate);
        downloadCounts = JsonUtils.getInt(body, JsonKey.downloadCount);
        description = JsonUtils.getString(body, JsonKey.description);
        updateInfo = JsonUtils.getString(body, JsonKey.updateInfo);
        downloadUrl = JsonUtils.getString(body, JsonKey.downloadUrl);
        iconUrl = JsonUtils.getString(body, JsonKey.iconUrl);
        size = JsonUtils.getLong(body, JsonKey.size);
        version = JsonUtils.getInt(body, JsonKey.version);
        versionName = JsonUtils.getString(body, JsonKey.versionName);
        developer = JsonUtils.getString(body, JsonKey.developer);
        publicTime = JsonUtils.getString(body, JsonKey.publicTime);
        previewImg = JsonUtils.getString(body, JsonKey.previewImg).split("\\|");
//        for (int i = 0; i < previewImg.length; i++) {
//            LOGGER.warn(TAG, previewImg[i]);
//        }
        typeName = JsonUtils.getString(body, JsonKey.typeName);
        keyWords = JsonUtils.getString(body, JsonKey.keyword).replace("|", "，");
        fileMd5 = JsonUtils.getString(body, JsonKey.appMd5, "");
    }

    public String getDescription() {
        return description;
    }

    public String getDeveloper() {
        return developer;
    }

    public int getDownloadCounts() {
        return downloadCounts;
    }

    public String getDownloadUrl() {
        return downloadUrl;
    }

    public String getIconUrl() {
        return iconUrl;
    }

    public String getKeyWords() {
        return keyWords;
    }

    public String[] getPreviewImg() {
        return previewImg;
    }

    public String getPublicTime() {
        return publicTime;
    }

    public long getSize() {
        return size;
    }

    public String getTypeName() {
        return typeName;
    }

    public String getUpdateInfo() {
        return updateInfo;
    }

    public int getVersion() {
        return version;
    }

    public String getVersionName() {
        return versionName;
    }

    public String getFileMd5() {
        return fileMd5;
    }

    public void setFileMd5(String fileMd5) {
        this.fileMd5 = fileMd5;
    }
}
