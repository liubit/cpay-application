package com.centerm.cpay.applicationshop;

import android.app.Application;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Environment;
import android.os.IBinder;
import android.os.RemoteException;
import android.text.TextUtils;

import com.centerm.cpay.applicationshop.common.AppSettings;
import com.centerm.cpay.applicationshop.service.CheckUpdateService;
import com.centerm.cpay.applicationshop.utils.CommonUtils;
import com.centerm.cpay.applicationshop.utils.FileUtils;
import com.centerm.cpay.midsdk.dev.DeviceFactory;
import com.centerm.cpay.midsdk.dev.EnumDeviceType;
import com.centerm.cpay.midsdk.dev.EnumSDKType;
import com.centerm.cpay.midsdk.dev.define.ISystemService;
import com.centerm.cpay.midsdk.dev.define.pboc.EmvTag;
import com.centerm.cloudsys.sdk.common.log.Log4d;
import com.centerm.cpay.securitysuite.aidl.IVirtualPinPad;
import com.nostra13.universalimageloader.cache.disc.naming.Md5FileNameGenerator;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.assist.QueueProcessingType;

import org.xutils.x;

import java.io.File;

import config.Config;

/**
 * Created by linwanliang on 2016/6/20.
 */
public class MyApplication extends Application {

    private final static Log4d logger = Log4d.getDefaultInstance();
    private final static String TAG = MyApplication.class.getSimpleName();
    private final static String GLOBAL_TAG = "CpayAppStore";
//    private static String netAddress;
    private static IVirtualPinPad macService;
    private static ServiceConnection connection = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {
            macService = IVirtualPinPad.Stub.asInterface(service);
            if (Config.DEMO) {
              /*  try {
                    macService.loadMainKey("4A9509351367EA729CCC9B25ED2B4C8C", null);
                    macService.loadWorkKey(1, "7795B2437869E837F6E69F066AF667F4", null);
                    macService.loadWorkKey(2, "9B725F2483ADE643E97ED1104D7FF166", null);
                    macService.loadWorkKey(3, "BE628C34DD27F447D583E796DACDF443", null);
                } catch (RemoteException e) {
                    e.printStackTrace();
                }*/
            }
        }

        @Override
        public void onServiceDisconnected(ComponentName name) {
            logger.warn(TAG, "安全套件服务已断开");
            macService = null;
        }
    };

    @Override
    public void onCreate() {
        super.onCreate();
        Log4d.getDefaultInstance().setGlobalTag(GLOBAL_TAG);
        Log4d.getDefaultInstance().setLogDirectory(Environment.getExternalStorageDirectory() + Config.LOG_PATH);
        initImageLoader();
        new Thread(new Runnable() {
            @Override
            public void run() {
                obtainDevMask();
            }
        }).start();
        bindMacService(getApplicationContext());
        //
        Intent checkService = new Intent(getApplicationContext(), CheckUpdateService.class);
        startService(checkService);
        x.Ext.init(this);//初始化xutils
//        getNetAddressFromGtms();
    }

    private void obtainDevMask() {
        final DeviceFactory factory = DeviceFactory.getInstance();
        DeviceFactory.InitCallback callback = new DeviceFactory.InitCallback() {
            @Override
            public void onResult(boolean b) {
                if (b) {
                    logger.info(TAG, "中间件初始化成功");
                    ISystemService systemService = (ISystemService) factory.getDevice(EnumDeviceType.SYSTEM_SERVICE);
                    AppSettings.setDevMask(getApplicationContext(), systemService.getTerminalSn());
                } else {
                    logger.warn(TAG, "中间件初始化失败");
                }
            }
        };
        factory.init(getApplicationContext(), EnumSDKType.CPAY_SDK, callback);
    }

    private void initImageLoader() {
        ImageLoaderConfiguration config = new ImageLoaderConfiguration
                .Builder(getApplicationContext())
                .threadPriority(Thread.NORM_PRIORITY - 2)
                .denyCacheImageMultipleSizesInMemory()
                .diskCacheFileNameGenerator(new Md5FileNameGenerator())
                .tasksProcessingOrder(QueueProcessingType.LIFO)
                .build();
        ImageLoader.getInstance().init(config);
    }

    private static boolean bindMacService(Context context) {
        Intent intent = new Intent();
        intent.setAction("com.centerm.cpay.securitysuite.AIDL_SERVICE");
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            intent.setPackage("com.centerm.cpay.securitysuite");
        }
        boolean b = context.bindService(intent, connection, Context.BIND_AUTO_CREATE);
        logger.warn(TAG, "绑定安全套件结果：" + b);
        return b;
    }

    public static IVirtualPinPad getMacService(Context context) {
        if (macService == null) {
            bindMacService(context);
        }
        return macService;
    }

    public static boolean is960F(){
        return Build.VERSION.SDK_INT==Build.VERSION_CODES.JELLY_BEAN_MR1;
    }

//    private void getNetAddressFromGtms() {
//        try {
//            Context otherAppContext = createPackageContext("com.centerm.cpay.gtms", Context.CONTEXT_IGNORE_SECURITY);
//            SharedPreferences sharedPreferences = otherAppContext.getSharedPreferences("OtherCommonParams", Context.MODE_WORLD_READABLE | Context.MODE_MULTI_PROCESS);
//            String ip = sharedPreferences.getString("shopIp", "");
//            String port = sharedPreferences.getString("shopPort", "");
//            logger.warn(TAG, "@@@@@@@@@@获取到的IP: " + ip + "    端口号：" + port);
//            if (!TextUtils.isEmpty(ip)) {
//                netAddress = "http://" + ip + ":" + port;
//                logger.warn(TAG, netAddress);
//            }
//        } catch (PackageManager.NameNotFoundException e) {
//            e.printStackTrace();
//        }
//    }
//
//    public static String getNetAddress() {
//        return netAddress;
//    }
}
