package com.centerm.cpay.applicationshop.view;

import android.annotation.TargetApi;
import android.content.Context;
import android.graphics.Color;
import android.os.Build;
import android.util.AttributeSet;
import android.view.Gravity;
import android.view.WindowManager;
import android.widget.HorizontalScrollView;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.centerm.cpay.applicationshop.R;
import com.centerm.cloudsys.sdk.common.log.Log4d;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;

import config.Config;

/**
 * @author: linwanliang</br>
 * @date:2016/7/1</br>
 */
public class ScreenShotGallery extends HorizontalScrollView {
    private DisplayImageOptions options = new DisplayImageOptions.Builder()
            .showImageOnLoading(Config.DEFAULT_APP_SCREENSHOT)
            .showImageForEmptyUri(Config.DEFAULT_APP_SCREENSHOT)
            .showImageOnFail(Config.DEFAULT_APP_SCREENSHOT)
            .cacheInMemory(false)
            .cacheOnDisk(true)
            .build();
    private Log4d logger;
    private String TAG;


    private int itemSpacing = 30;
    private int itemWidth = -1;
    private Context context;

    public ScreenShotGallery(Context context) {
        super(context);
        init(context);
    }

    public ScreenShotGallery(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context);
    }

    public ScreenShotGallery(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context);
    }

    @TargetApi(value = Build.VERSION_CODES.LOLLIPOP)
    public ScreenShotGallery(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        init(context);
    }

    private void init(Context context) {
        logger = Log4d.getDefaultInstance();
        TAG = getClass().getSimpleName();
        this.context = context;
        itemWidth = context.getResources().getDimensionPixelSize(R.dimen.detail_screenshot_min_width);
        itemSpacing = context.getResources().getDimensionPixelSize(R.dimen.detail_screenshot_spacing);
    }

    public void loadImage(String[] urlArray) {
        if (urlArray == null || urlArray.length == 0) {
            return;
        }
        //初始化容器
        LinearLayout container = null;
        if (getChildCount() == 1) {
            if (getChildAt(0) instanceof LinearLayout) {
                container = (LinearLayout) getChildAt(0);
            } else {
                removeAllViews();
            }
        } else {
            removeAllViews();
        }
        if (container == null) {
            container = new LinearLayout(context);
            container.setOrientation(LinearLayout.HORIZONTAL);
        }
        container.removeAllViews();
        addView(container, new LinearLayout.LayoutParams(-1, -1));
        //获取需要显示的图片数量
        int size = urlArray.length;
        if (size == 1) {
            //如果是一张图片，就居中显示
            ImageView item = createItem(urlArray[0]);
            container.addView(item);
            container.setOrientation(LinearLayout.VERTICAL);
            container.setGravity(Gravity.CENTER_HORIZONTAL);
        } else if (size == 2) {
            //如果是两张图片，并排居中显示
            container.setOrientation(LinearLayout.VERTICAL);
            container.setGravity(Gravity.CENTER_HORIZONTAL);
            LinearLayout innerLayout = new LinearLayout(context);
            innerLayout.setOrientation(LinearLayout.HORIZONTAL);
            innerLayout.addView(createItem(urlArray[0]));
            ImageView item2 = createItem(urlArray[1]);
            innerLayout.addView(item2);
            LinearLayout.LayoutParams params = (LinearLayout.LayoutParams) item2.getLayoutParams();
            params.leftMargin = itemSpacing;
            item2.setLayoutParams(params);
            container.addView(innerLayout);
        } else {
            for (int i = 0; i < urlArray.length; i++) {
                ImageView item = createItem(urlArray[i]);
                container.addView(item);
                if (i != size - 1) {
                    LinearLayout.LayoutParams params = (LinearLayout.LayoutParams) item.getLayoutParams();
                    params.rightMargin = itemSpacing;
                    item.setLayoutParams(params);
                }
            }
            postDelayed(new Runnable() {
                @Override
                public void run() {
                    smoothScrollTo(getAutoScrollX(), 0);
                }
            }, 500);
        }
    }

    private ImageView createItem(String url) {
        ImageView item = new ImageView(context);
        item.setScaleType(ImageView.ScaleType.FIT_XY);
        item.setBackgroundResource(Config.DEFAULT_APP_SCREENSHOT);
        //TODO 吃完饭回来搞！
        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(itemWidth, -1);
        params.setMargins(0, 0, 0, 10);
        item.setLayoutParams(params);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            item.setElevation(10);
//            item.setTranslationZ(10);
        }
        ImageLoader.getInstance().displayImage(url, item, options);
        return item;
    }

    private int getAutoScrollX() {
        WindowManager wm = (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);
        int width = wm.getDefaultDisplay().getWidth();
        int oldX = getPaddingLeft() + itemWidth + itemSpacing;
        int newX = (width - itemWidth) / 2;
        return oldX - newX;
    }


}
