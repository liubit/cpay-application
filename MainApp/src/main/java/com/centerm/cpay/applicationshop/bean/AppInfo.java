package com.centerm.cpay.applicationshop.bean;

import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.graphics.drawable.Drawable;

import com.centerm.cpay.applicationshop.net.cont.JsonKey;
import com.centerm.cpay.applicationshop.utils.JsonUtils;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

import org.json.JSONObject;

import java.io.Serializable;

/**
 * Created by linwanliang on 2016/6/21.
 */
@DatabaseTable(tableName = "tb_download_app")
public class AppInfo implements Serializable, Cloneable {

    @DatabaseField(id = true)
    private String packageName;//包名
    @DatabaseField
    private String screenType;//1为横屏，2为竖屏
    @DatabaseField
    private String name;//应用名称
    @DatabaseField
    private float rate;//推荐星级
    @DatabaseField
    private int downloadCount;//下载数
    @DatabaseField
    private String packageUrl;//下载地址
    @DatabaseField
    private String iconUrl;//图标地址
    @DatabaseField
    private long size;//大小
    @DatabaseField
    private int version;//版本号
    @DatabaseField
    private String versionName;//版本名
    @DatabaseField
    private String description;//简介
    @DatabaseField
    private String updateInfo;//更新信息
    @DatabaseField
    private String developer;//开发者
    @DatabaseField
    private String upTime;//上架时间
    @DatabaseField
    private String previewImage;//截图
    @DatabaseField
    private String keyWords;//关键词
    @DatabaseField
    private int status;//当前状态
    @DatabaseField
    private String typeName;//类型名称
    @DatabaseField
    private String fileMd5;//文件MD5


    private AppStatus statusBeforeDownload;//开始下载前的状态
    private Drawable iconDrawable;//图标对象，用于存储本地应用的图标对象
    private DownloadExtra downloadExtra = new DownloadExtra();

//    @DatabaseField
//    private int progress;//下载进度


    public AppInfo() {
        super();
    }

    public AppInfo(String packageName) {
        this.packageName = packageName;
    }

    public int getDownloadCount() {
        return downloadCount;
    }

    public void setDownloadCount(int downloadCount) {
        this.downloadCount = downloadCount;
    }


    public String getIconUrl() {
        return iconUrl;
    }

    public void setIconUrl(String iconUrl) {
        this.iconUrl = iconUrl;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPackageName() {
        return packageName;
    }

    public void setPackageName(String packageName) {
        this.packageName = packageName;
    }

    public String getPackageUrl() {
        return packageUrl;
    }

    public void setPackageUrl(String packageUrl) {
        this.packageUrl = packageUrl;
    }

    public float getRate() {
        return rate;
    }

    public void setRate(float rate) {
        this.rate = rate;
    }

    public long getSize() {
        return size;
    }

    public void setSize(long size) {
        this.size = size;
    }

    public String getScreenType() {
        return screenType;
    }

    public void setScreenType(String screenType) {
        this.screenType = screenType;
    }

    public int getVersion() {
        return version;
    }

    public void setVersion(int version) {
        this.version = version;
    }

    public String getVersionName() {
        return versionName;
    }

    public void setVersionName(String versionName) {
        this.versionName = versionName;
    }

    public AppStatus getStatus() {
        for (AppStatus item : AppStatus.values()) {
            if (item.ordinal() == status) {
                return item;
            }
        }
        return AppStatus.CAN_DOWNLOAD;
    }

    public void setStatus(AppStatus newS) {
        if (newS != null) {
            this.status = newS.ordinal();
        } else {
            this.status = AppStatus.CAN_DOWNLOAD.ordinal();
        }
    }

    public Drawable getIconDrawable() {
        return iconDrawable;
    }

    public void setIconDrawable(Drawable iconDrawable) {
        this.iconDrawable = iconDrawable;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void setUpdateInfo(String updateInfo) {
        this.updateInfo = updateInfo;
    }

    public String getDescription() {
        return description;
    }

    public String getUpdateInfo() {
        return updateInfo;
    }

    public String getDeveloper() {
        return developer;
    }

    public void setDeveloper(String developer) {
        this.developer = developer;
    }

    public String getUpTime() {
        return upTime;
    }

    public void setUpTime(String upTime) {
        this.upTime = upTime;
    }

    public String getKeyWords() {
        return keyWords;
    }

    public void setKeyWords(String keyWords) {
        this.keyWords = keyWords;
    }

    public String getPreviewImage() {
        return previewImage;
    }

    public void setPreviewImage(String previewImage) {
        this.previewImage = previewImage;
    }

    public DownloadExtra getDownloadExtra() {
        return downloadExtra;
    }

    public void setDownloadExtra(DownloadExtra downloadExtra) {
        this.downloadExtra = downloadExtra;
    }

    public void setStatusBeforeDownload() {
        this.statusBeforeDownload = getStatus();
    }

    public AppStatus getStatusBeforeDownload() {
        return statusBeforeDownload;
    }

    public String getFileMd5() {
        return fileMd5;
    }

    public void setFileMd5(String fileMd5) {
        this.fileMd5 = fileMd5;
    }

    public static AppInfo convert(Context context, ResolveInfo info) throws PackageManager.NameNotFoundException {
        if (info == null || context == null) {
            return null;
        }
        AppInfo app = new AppInfo();
        PackageManager pManager = context.getPackageManager();
        // 图标
        app.setIconDrawable(info.loadIcon(pManager));
        // 包名
        String pkgName = info.activityInfo.packageName;
        app.setPackageName(pkgName);
        PackageInfo pkgInfo = pManager.getPackageInfo(pkgName, 0);
        // 首次安装时间
//        app.setFirstInstallTime(pkgInfo.firstInstallTime);
        // 版本名
        app.setVersionName(pkgInfo.versionName);
        // 版本号
        app.setVersion(pkgInfo.versionCode);
        // 应用名
        app.setName((String) info.loadLabel(pManager));
        return app;
    }

    public static AppInfo convert(JSONObject json) {
        if (json == null) {
            return null;
        }
        AppInfo app = new AppInfo();
        app.setPackageName(JsonUtils.getString(json, JsonKey.packageName));
        app.setName(JsonUtils.getString(json, JsonKey.name));
        app.setRate(JsonUtils.getFloat(json, JsonKey.rate));
        app.setDownloadCount(JsonUtils.getInt(json, JsonKey.downloadCount));
        app.setPackageUrl(JsonUtils.getString(json, JsonKey.downloadUrl));
        app.setIconUrl(JsonUtils.getString(json, JsonKey.iconUrl));
        app.setSize(JsonUtils.getLong(json, JsonKey.size));
        app.setVersion(JsonUtils.getInt(json, JsonKey.version));
        app.setVersionName(JsonUtils.getString(json, JsonKey.versionName));
        app.setScreenType(JsonUtils.getString(json, JsonKey.screenType));
        app.setDescription(JsonUtils.getString(json, JsonKey.description));
        app.setUpdateInfo(JsonUtils.getString(json, JsonKey.updateInfo));
        app.setDeveloper(JsonUtils.getString(json, JsonKey.developer));
        app.setUpTime(JsonUtils.getString(json, JsonKey.upTime));
        app.setPreviewImage(JsonUtils.getString(json, JsonKey.previewImg));
        app.setKeyWords(JsonUtils.getString(json, JsonKey.keyword));
        app.setTypeName(JsonUtils.getString(json, JsonKey.appType));
        app.setFileMd5(JsonUtils.getString(json, JsonKey.appMd5));
        return app;
    }

    @Override
    public String toString() {
        return "AppInfo{" +
                "description='" + description + '\'' +
                ", packageName='" + packageName + '\'' +
                ", screenType='" + screenType + '\'' +
                ", name='" + name + '\'' +
                ", rate='" + rate + '\'' +
                ", downloadCount='" + downloadCount + '\'' +
                ", downloadUrl='" + packageUrl + '\'' +
                ", iconUrl='" + iconUrl + '\'' +
                ", iconDrawable=" + iconDrawable +
                ", size=" + size +
                ", version='" + version + '\'' +
                ", versionName='" + versionName + '\'' +
                ", updateInfo='" + updateInfo + '\'' +
                ", developer='" + developer + '\'' +
                ", upTime='" + upTime + '\'' +
                ", previewImage='" + previewImage + '\'' +
                ", keyword='" + keyWords + '\'' +
                ", status=" + status +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        AppInfo appInfo = (AppInfo) o;

        return packageName != null ? packageName.equals(appInfo.packageName) : appInfo.packageName == null;

    }

    @Override
    public int hashCode() {
        return packageName != null ? packageName.hashCode() : 0;
    }

    public String toString2() {
        return super.toString();
    }

    @Override
    public AppInfo clone() throws CloneNotSupportedException {
        return (AppInfo) super.clone();
    }

    public String getTypeName() {
        return typeName;
    }

    public void setTypeName(String typeName) {
        this.typeName = typeName;
    }

    public enum AppStatus {

        CAN_DOWNLOAD(10, "下载"),//可下载，即本地未安装该应用，下一步是WAIT_DOWNLOAD

        WAIT_DOWNLOAD(11, "等待"),//等待下载，已存在下载队列中，下一步是DOWLOADING或PAUSE

        DOWNLOADING(12, "正在下载"),//下载中,下一步是PAUSE_DOWNLOAD或INSTALLING

        PAUSE_DOWNLOAD(13, "暂停"),//暂停下载。可能是用户手动暂停，也可能是程序退出时自动暂停所有下载任务，下一步是WAIT_DOWNLOAD

        DOWNLOAD_FAILED(14, "下载失败"),//下载失败，下一步是WAIT_DOWNLOAD

        INSTALLING(15, "正在安装"),//安装中，下一步是CAN_OPEN

        WAIT_INSTALL(16, "安装"),//等待安装。正常情况下，应用下载完成后自动完成静默安装，这种状态出现在异常情况下，下一步是INSTALLING

        CAN_UPDATE(17, "更新"),//可更新，即本地已安装应用，服务端存在新版本，下一步是WAIT_DOWNLOAD

        CAN_OPEN(18, "打开"),//可打开，即本地已安装应用，服务端不存在新版本

        CHECKING_MD5(20, "校验中");//文件完整性校验，下一步是WAIT_INSTALL

        private int code;
        private String text;

        AppStatus(int c, String t) {
            this.code = c;
            this.text = t;
        }

        public int getCode() {
            return code;
        }

        public String getText() {
            return text;
        }

        public static AppStatus ordinalMapObj(int ordinal) {
            for (AppStatus item : AppStatus.values()) {
                if (item.ordinal() == ordinal) {
                    return item;
                }
            }
            return null;
        }
    }

    public static class DownloadExtra implements Serializable {
        private String speed = "0B/S";
        private int progress;

        public DownloadExtra() {
        }

        public DownloadExtra(int progress, String speed) {
            this.progress = progress;
            this.speed = speed;
        }

        public int getProgress() {
            return progress;
        }

        public void setProgress(int progress) {
            this.progress = progress;
        }

        public String getSpeed() {
            return speed;
        }

        public void setSpeed(String speed) {
            this.speed = speed;
        }
    }
}
