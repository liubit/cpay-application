package com.centerm.cpay.applicationshop.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Handler;
import android.support.v7.app.ActionBar;
import android.view.View;
import android.widget.Button;
import android.widget.ListView;

import com.centerm.cpay.applicationshop.R;
import com.centerm.cpay.applicationshop.adapter.CommonAppListAdapter;
import com.centerm.cpay.applicationshop.base.BaseDownloadActivity;
import com.centerm.cpay.applicationshop.bean.AppInfo;
import com.centerm.cpay.applicationshop.common.BroadcastAction;
import com.centerm.cpay.applicationshop.common.KeyName;
import com.centerm.cpay.applicationshop.download.ApkDownloadManager;
import com.centerm.cpay.applicationshop.download.AppInfoSync;
import com.centerm.cpay.applicationshop.utils.ViewUtils;
import com.centerm.cloudsys.sdk.common.utils.StringUtils;
import com.j256.ormlite.dao.Dao;

import java.sql.SQLException;
import java.util.List;

import config.Config;

/**
 * Created by linwanliang on 2016/6/27.
 */
public class DownloadManageActivity extends BaseDownloadActivity implements View.OnClickListener {

    private ActionBar titleBar;
    private CommonAppListAdapter adapter;
    private List<AppInfo> appList;
    private String addPkgName = "";


    @Override
    public void onReceiveAppStatus(Context context, Intent intent) {
        String action = intent.getAction();
        if (action.equals(BroadcastAction.DOWNLOAD.ACTION_PAUSE_ALL)) {
            return;
        }
        AppInfo app = (AppInfo) intent.getSerializableExtra(KeyName.APP_INFO);
        if (adapter != null) {
            if (action.equals(BroadcastAction.DOWNLOAD.ACTION_CANCELED)) {
                //如果是取消下载，删除该item
                int size = adapter.notifyItemRemoved(app.getPackageName());
                checkAppListHasTask(size);
            } else if (action.equals("android.intent.action.PACKAGE_ADDED")) {
                addPkgName = intent.getData().getSchemeSpecificPart();
                if (adapter.isViewVisible()) {
                    adapter.notifyItemRemoved(addPkgName);
                }
            } else {
                //更新显示状态
                adapter.notifyItemStatusChanged(app);
            }
        }
    }

    private void onResumeCheckPkgAdd() {
        if (!StringUtils.isStrNull(addPkgName)) {
            adapter.notifyItemRemoved(addPkgName);
            addPkgName = "";
        }
    }

    @Override
    public int onInitLayoutId() {
        return R.layout.activity_download_manage;
    }

    @Override
    public void onInitLocalData() {
        isReceivePackageChanged = true;
    }

    @Override
    public void onInitView(View view) {
//        if (ApkDownloadManager.getInstance().hasTask()) {
//            titleBar = initBarsCustomView(R.string.title_download_manage, R.string.all_pause, this);
//        } else {
//            titleBar = initBarsCustomView(R.string.title_download_manage, R.string.all_resume, this);
//        }
        initUsualToolbar(R.string.title_download_manage);

        ListView listView = (ListView) findViewById(R.id.list_v);
        initListView(listView);
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (adapter != null) {
            adapter.setResume();
            onResumeCheckPkgAdd();
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (adapter != null) {
            adapter.setPause();
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    private void initListView(final ListView view) {
        setRightBtnEnable(titleBar, View.GONE);
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                Dao<AppInfo, String> dao = getDbHelper().getAppDao();
                try {
                    appList = dao.queryForAll();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
                if (appList != null && appList.size() > 0) {
                    for (int i = 0; i < appList.size(); i++) {
                        AppInfoSync.syncWithDownloadPool(appList.get(i));
                    }
                    adapter = new CommonAppListAdapter(context, R.layout.manage_download_list_item, appList);
                    view.setAdapter(adapter);
                    checkAppListHasTask(appList.size());
                } else {
                    checkAppListHasTask(0);
                }
            }
        }, Config.DELAY_LOAD_DATA);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            //顶栏的操作按钮
            case R.id.operate_btn:
                if (appList == null || appList.size() == 0) {
                    ViewUtils.showToast(context, "下载列表无应用");
                    return;
                }
                Button btn = (Button) v;
                String btnText = ((Button) v).getText().toString();
                if (btnText.equals(getResources().getString(R.string.all_pause))) {
                    btn.setText(R.string.all_resume);
                    ApkDownloadManager.getInstance().pauseAll(context);
                    adapter.notifyAllPaused();
                } else {
                    btn.setText(R.string.all_pause);
                    resumeTasksInOrder();
                }
                break;
        }
    }

    private void resumeTasksInOrder() {
        showLoading();
        new Thread(new Runnable() {
            @Override
            public void run() {
                final ApkDownloadManager manager = ApkDownloadManager.getInstance();
                for (int i = 0; i < appList.size(); i++) {
                    final int index = i;
                    AppInfo item = appList.get(i);
                    if (item.getStatus().equals(AppInfo.AppStatus.PAUSE_DOWNLOAD)) {
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                manager.start(context, appList.get(index));
                            }
                        });
                        try {
                            Thread.sleep(1000);
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                    }

                }
                stopLoading();
            }
        }).start();
    }

    private void checkAppListHasTask(int size) {
//        if (size == 0) {
//            setRightBtnEnable(titleBar, View.GONE);
//        } else {
//            setRightBtnEnable(titleBar, View.VISIBLE);
//        }
    }


}
