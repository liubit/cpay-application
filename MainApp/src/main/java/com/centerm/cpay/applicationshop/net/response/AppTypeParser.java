package com.centerm.cpay.applicationshop.net.response;

import com.centerm.cpay.applicationshop.bean.AppInfo;
import com.centerm.cpay.applicationshop.bean.AppType;
import com.centerm.cpay.applicationshop.net.BaseParser;
import com.centerm.cpay.applicationshop.net.cont.JsonKey;
import com.centerm.cpay.applicationshop.utils.JsonUtils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import config.Config;

/**
 * @author linwanliang
 * @date 2016-06-21
 */
public class AppTypeParser extends BaseParser {
    private List<AppType> typeList;
    private Map<String, List<AppInfo>> idMapApps;

    public AppTypeParser(String response) {
        super(response);
        JSONObject body = getBody();
        if (body != null) {
            try {
                JSONArray typeArray = body.getJSONArray(JsonKey.types);
                typeList = new ArrayList<>();
                idMapApps = new HashMap<>();
                for (int i = 0; i < typeArray.length(); i++) {
                    JSONObject json = typeArray.getJSONObject(i);
                    AppType item = new AppType(json.getString(JsonKey.apkTypeId), json.getString(JsonKey.apkTypeName));
                    item.setIconUrl(json.getString(JsonKey.iconUrl));
                    typeList.add(item);
                    JSONArray array = json.getJSONArray(JsonKey.apps);
                    List<AppInfo> appList = new ArrayList<AppInfo>();
                    int appCounts = array.length() > Config.DEFAULT_SORT_APP_COUNTS ? Config.DEFAULT_SORT_APP_COUNTS : array.length();
                    for (int j = 0; j < appCounts; j++) {
                        JSONObject appItem = array.getJSONObject(j);
                        AppInfo appInfo = AppInfo.convert(appItem);
                        appList.add(appInfo);
                    }
                    idMapApps.put(item.getId(), appList);
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    public List<AppType> getTypeList() {
        return typeList;
    }

    public Map<String, List<AppInfo>> getIdMapApps() {
        return idMapApps;
    }
}
