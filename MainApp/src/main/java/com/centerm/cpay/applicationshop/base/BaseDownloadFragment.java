package com.centerm.cpay.applicationshop.base;

import android.os.Handler;
import android.view.MotionEvent;
import android.view.View;

import com.centerm.cpay.applicationshop.download.AppStatusReceiver;
import com.cjj.MaterialRefreshLayout;

/**
 * Created by linwanliang on 2016/6/25.
 */
public abstract class BaseDownloadFragment extends BaseFragment implements AppStatusReceiver {
//    private boolean receiveProgressFlag;

    @Override
    public void onResume() {
        super.onResume();
    }

    public void initMaterialRefreshLayout(final MaterialRefreshLayout mRefreshLayout){
        mRefreshLayout.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                if(motionEvent.getAction()==MotionEvent.ACTION_UP){
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            if(!mRefreshLayout.isRefreshing()){
                                mRefreshLayout.finishRefresh();
                            }
                        }
                    },100);
                }
                return false;
            }
        });
    }

    /*
    protected void pauseReceiveProgress() {
        this.receiveProgressFlag = false;
    }

    protected void resumeReceiveProgress() {
        this.receiveProgressFlag = true;
    }*/

}
