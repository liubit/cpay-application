package com.centerm.cpay.applicationshop.activity;

import android.view.View;
import android.widget.TextView;

import com.centerm.cpay.applicationshop.R;
import com.centerm.cpay.applicationshop.base.BaseActivity_v21;
import com.centerm.cpay.applicationshop.utils.PackageUtils;

import config.Config;

/**
 * author: linwanliang</br>
 * date:2016/7/2</br>
 */
public class AboutActivity extends BaseActivity_v21 {
    TextView copyRightChnShow, copyRightEnShow;

    @Override
    public int onInitLayoutId() {
        return R.layout.activity_about;
    }

    @Override
    public void onInitLocalData() {

    }

    @Override
    public void onInitView(View view) {
        initUsualToolbar(R.string.title_about);
        String versionName = PackageUtils.getInstalledVersionName(this, getPackageName());
        TextView versionShow = (TextView) findViewById(R.id.app_version_show);
        versionShow.setText(versionName);
        copyRightChnShow = (TextView) findViewById(R.id.copyright_chn_show);
        copyRightEnShow = (TextView) findViewById(R.id.copyright_eng_show);
        String channel = Config.getChannelName(this);
        if (channel.equals("jpos")) {
            copyRightChnShow.setText(getText(R.string.jpos_copyright_chn));
            copyRightEnShow.setText(getText(R.string.jpos_copyright_eng));
        } else if (channel.equals("centerm")) {
            copyRightChnShow.setText(getText(R.string.centerm_copyright_chn));
            copyRightEnShow.setText(getText(R.string.centerm_copyright_eng));
        }

    }

//    @Override
//    public boolean onKeyUp(int keyCode, KeyEvent event) {
//        switch (keyCode) {
//            case KeyEvent.KEYCODE_MENU:
//                DialogFactory.showServerAddressDialog(context);
//                break;
//        }
//        return super.onKeyUp(keyCode, event);
//    }
}
