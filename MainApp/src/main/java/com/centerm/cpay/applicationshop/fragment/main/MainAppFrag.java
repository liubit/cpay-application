package com.centerm.cpay.applicationshop.fragment.main;

import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.RequiresApi;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;

import com.centerm.cpay.applicationshop.R;
import com.centerm.cpay.applicationshop.adapter.MainAppGridAdapter;
import com.centerm.cpay.applicationshop.asyncTask.AppAsyncTask;
import com.centerm.cpay.applicationshop.asyncTask.AppMatchListener;
import com.centerm.cpay.applicationshop.base.BaseDownloadFragment;
import com.centerm.cpay.applicationshop.bean.Advertisement;
import com.centerm.cpay.applicationshop.bean.AppInfo;
import com.centerm.cpay.applicationshop.common.BroadcastAction;
import com.centerm.cpay.applicationshop.common.KeyName;
import com.centerm.cpay.applicationshop.net.ResponseHandler;
import com.centerm.cpay.applicationshop.net.request.GetAdListReq;
import com.centerm.cpay.applicationshop.net.request.GetAppListReq;
import com.centerm.cpay.applicationshop.net.response.AdListParser;
import com.centerm.cpay.applicationshop.net.response.AppListParser;
import com.centerm.cpay.applicationshop.utils.ViewUtils;
import com.centerm.cpay.applicationshop.view.AtMostGridView;
import com.cjj.MaterialRefreshLayout;
import com.cjj.MaterialRefreshListener;
import com.daimajia.slider.library.Indicators.PagerIndicator;
import com.daimajia.slider.library.SliderLayout;
import com.daimajia.slider.library.SliderTypes.BaseSliderView;
import com.daimajia.slider.library.SliderTypes.DefaultSliderView;
import com.daimajia.slider.library.Tricks.ViewPagerEx;

import org.json.JSONArray;

import java.util.ArrayList;
import java.util.List;

import config.Config;

/**
 * Created by 王玮 on 2016/8/1.
 */
public class MainAppFrag extends BaseDownloadFragment implements BaseSliderView.OnSliderClickListener, ViewPagerEx.OnPageChangeListener {
    private MaterialRefreshLayout refreshLayout;
    private MainAppGridAdapter adapter;
    private AtMostGridView gridView;
    private SliderLayout sliderLayout;
    private PagerIndicator indicator;
    private String apkTypeId;
    private int total = -1;
    private int pageIndex = 0;
    private List<Advertisement> adCollection;

    public static boolean isRefresh = false;

    @Override
    public int onInitLayoutId() {
        return R.layout.fragment_main;
    }

    @Override
    public void onInitLocalData() {
        apkTypeId = "90001";
        adCollection = new ArrayList<>();
    }

    @Override
    public void onInitView(View view) {
        gridView = (AtMostGridView) view.findViewById(R.id.grid_v);
        initGridView();
        refreshLayout = (MaterialRefreshLayout) view.findViewById(R.id.refresh_layout);
        initRefreshView(refreshLayout);
        sliderLayout = (SliderLayout) view.findViewById(R.id.slider_layout);
        indicator = (PagerIndicator) view.findViewById(R.id.page_indicator);
        initAdView();
    }

    @Override
    public void doThingsAfterInitView() {
        if (isCreated) {
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    gridView.setAdapter(adapter);
                    refreshAdView();
                }
            }, Config.DELAY_LOAD_DATA);
        } else {
            requestAdList();
            requestAppList(true, true);
        }
    }

    private void initGridView() {
        gridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                if (!canPerformClick()) {
                    return;
                }
                AppInfo info = ((MainAppGridAdapter.ViewBinder) view.getTag()).getBindedData();
                if (info.getPackageName() == null) {
                    return;
                }
                Intent intent = new Intent();
                intent.setAction(BroadcastAction.ACTION_OPEN_APPDETAIL);
                intent.putExtra(KeyName.APP_INFO, info);
                context.sendBroadcast(intent);
            }
        });
        gridView.setFocusable(false);

        if(Build.VERSION.SDK_INT==Build.VERSION_CODES.JELLY_BEAN_MR1){
            gridView.setNumColumns(7);
        }

    }

    private void initRefreshView(MaterialRefreshLayout view) {
        view.setMaterialRefreshListener(new MaterialRefreshListener() {
            @Override
            public void onRefresh(final MaterialRefreshLayout materialRefreshLayout) {
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        requestAppList(true, false);
                        requestAdList();
                    }
                }, Config.DELAY_LOAD_DATA);
            }

            @Override
            public void onfinish() {
            }

            @Override
            public void onRefreshLoadMore(final MaterialRefreshLayout materialRefreshLayout) {
                requestAppList(false, false);
            }
        });

    }

    private void initAdView() {
        sliderLayout.setPresetIndicator(SliderLayout.PresetIndicators.Center_Bottom);
        sliderLayout.setDuration(Config.AD_SCROLL_TIME);
        sliderLayout.addOnPageChangeListener(this);
        if (indicator != null) {
            sliderLayout.setCustomIndicator(indicator);
        }
    }

    private void requestAdList() {
        GetAdListReq req = new GetAdListReq(context);
        ResponseHandler handler = new ResponseHandler() {
            @Override
            public void onSuccess(String response) {
                AdListParser parser = new AdListParser(response);
                if (parser.getAdList() != null) {
                    adCollection = parser.getAdList();
                    refreshAdView();
                } else {
                    adCollection.clear();
                    sliderLayout.removeAllSliders();
                    sliderLayout.setVisibility(View.INVISIBLE);
//                    refreshAdView();
                }
            }

            @Override
            public void onFailure(int errorCode, String errorInfo) {
                adCollection.clear();
                refreshAdView();
            }
        };
        getHttpClient().post(context, req, handler);
    }

    private void refreshAdView() {
        sliderLayout.setVisibility(View.VISIBLE);
        sliderLayout.removeAllSliders();
        for (Advertisement item : adCollection) {
            DefaultSliderView sliderView = new DefaultSliderView(context);
            Bundle bundle = new Bundle();
            bundle.putSerializable("item", item);
            sliderView.bundle(bundle);//配置广告参数
            if (item.getDrawableId() != -1) {
                sliderView.image(item.getDrawableId());
            } else {
//                sliderView.image(item.getImgUrl()).error(R.drawable.ad_error)
//                        .empty(R.drawable.ad_loading);
                sliderView.image(R.drawable.ad03);
            }
            sliderView.setOnSliderClickListener(this);
            sliderLayout.addSlider(sliderView);
        }
    }

    private void requestAppList(final boolean refresh, boolean showLoading) {
        int c = adapter == null ? 0 : adapter.getCount();//当前列表数量
        int pi = this.pageIndex + 1;//页码
        if (refresh) {
            pi = 1;
        } else {
            if (c >= total) {
                return;
            }
        }
        if (showLoading) {
            showLoading();
        }
        final GetAppListReq req = new GetAppListReq(context);
        req.setApkTypeId(apkTypeId);
        req.setPageIndex(pi);
        ResponseHandler handler = new ResponseHandler() {
            @Override
            public void onSuccess(String response) {
                AppListParser parser = new AppListParser(context, response);
                if (parser.hasData()) {
                    total = parser.getTotal();
                    pageIndex = req.getPageIndex();
                    localMatchApp(parser.getAppsJson(), refresh);
                } else {
                    if (adapter == null) {
                        adapter = new MainAppGridAdapter(context, new ArrayList<AppInfo>(), gridView.getNumColumns());
                        adapter.setResourceId(R.layout.main_app_grid_item);
                        gridView.setAdapter(adapter);
                    } else {
                        gridView.setAdapter(adapter);
                        adapter.notifyDataChanged(new ArrayList<AppInfo>(), !refresh);
                    }

//                    adapter.notifyDataChanged(new ArrayList<AppInfo>(), !refresh);
                    stopLoading();
                    refreshLayout.finishRefresh();
                    refreshLayout.finishRefreshLoadMore();
                    ViewUtils.showToast(context, parser.getMsg());
                }
            }

            @Override
            public void onFailure(int errorCode, String errorInfo) {
                stopLoading();
                refreshLayout.finishRefresh();
                refreshLayout.finishRefreshLoadMore();
                ViewUtils.showToast(context, errorInfo);
            }
        };
        getHttpClient().post(context, req, handler);
    }

    private void localMatchApp(JSONArray appJson, final boolean refresh) {
        AppAsyncTask task = new AppAsyncTask();
        task.setAppMatchLisenter(getActivity(), new AppMatchListener() {
            @Override
            public void onStart() {

            }

            @Override
            public void onResult(List<AppInfo> apps) {
                if (adapter == null) {
                    adapter = new MainAppGridAdapter(context, apps, gridView.getNumColumns());
                    adapter.setResourceId(R.layout.main_app_grid_item);
                    gridView.setAdapter(adapter);
                } else {
                    gridView.setAdapter(adapter);
                    adapter.notifyDataChanged(apps, !refresh);
                }
                int counts = adapter == null ? 0 : adapter.getCount();
                if (counts < total) {
                    refreshLayout.setLoadMore(true);
                } else {
                    refreshLayout.setLoadMore(false);
                }
                stopLoading();
                refreshLayout.finishRefresh();
                refreshLayout.finishRefreshLoadMore();
            }
        });
        task.execute(appJson);
    }

    @Override
    public void onStart() {
        sliderLayout.startAutoCycle();
        super.onStart();
    }

    @Override
    public void onStop() {
        sliderLayout.stopAutoCycle();
        super.onStop();
    }

    @Override
    public void onReceiveAppStatus(final Context context, Intent intent) {
        if (!isCreated || adapter == null) {
            LOGGER.verbose(TAG, "未初始化，不处理广播");
            return;
        }
        String action = intent.getAction();
//        LOGGER.verbose(TAG, action);
        if (action.equals(BroadcastAction.DOWNLOAD.ACTION_PAUSE_ALL)) {
            adapter.notifyAllPaused();
        } else if (action.equals(Intent.ACTION_PACKAGE_ADDED)) {
            String pkgPrefix = intent.getData().getSchemeSpecificPart();
            adapter.notifyPackageInstalled(pkgPrefix);
        } else if (action.equals(Intent.ACTION_PACKAGE_REMOVED)) {
            String pkgPrefix = intent.getData().getSchemeSpecificPart();
            adapter.notifyPackageUnInstalled(pkgPrefix);
        } else {
            AppInfo app = (AppInfo) intent.getSerializableExtra(KeyName.APP_INFO);
            adapter.notifyItemStatusChanged(
                    app.getPackageName(),
                    app.getStatus(),
                    app.getDownloadExtra().getProgress());
        }
    }

    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

    }

    @Override
    public void onPageSelected(int position) {

    }

    @Override
    public void onPageScrollStateChanged(int state) {

    }

    @Override
    public void onSliderClick(BaseSliderView slider) {
        Bundle bundle = slider.getBundle();//获取点击的slider广告参数
        Advertisement item = (Advertisement) bundle.getSerializable("item");
        if (item != null) {
            makeAd(item);
        }
    }

    private void makeAd(Advertisement item) {
        String type = item.getType();
        if (!TextUtils.isEmpty(type)) {
            if (type.equals("2") || type.equals("4")) {
                //网页类
                Intent intent = new Intent();
                intent.setAction(BroadcastAction.ACTION_WEB_ACTIVITY);
                intent.putExtra("ad", item);
                context.sendBroadcast(intent);
            } else if (type.equals("3")) {
                //TODO 流媒体
            }
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        LOGGER.warn(TAG, "[应用总数量：]" + total);
        int counts = adapter == null ? 0 : adapter.getCount();
        LOGGER.warn(TAG, "[当前应用总数量：]" + counts);
        if (counts < total) {
            refreshLayout.setLoadMore(true);
        } else {
            refreshLayout.setLoadMore(false);
        }
        if (isRefresh) {
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    requestAppList(true, false);
                    isRefresh = false;
                }
            }, Config.DELAY_LOAD_DATA);
        }
    }
}
