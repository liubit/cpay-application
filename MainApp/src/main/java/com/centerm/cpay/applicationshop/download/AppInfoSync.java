package com.centerm.cpay.applicationshop.download;


import android.content.Context;

import com.centerm.cpay.applicationshop.bean.AppInfo;
import com.centerm.cpay.applicationshop.db.DbHelper;
import com.centerm.cpay.applicationshop.utils.PackageUtils;
import com.centerm.cloudsys.sdk.common.log.Log4d;
import com.j256.ormlite.android.apptools.OpenHelperManager;
import com.j256.ormlite.dao.Dao;

import java.sql.SQLException;
import java.util.Map;

/**
 * author: linwanliang</br>
 * date:2016/7/14</br>
 */
public class AppInfoSync {

    private static String TAG = AppInfoSync.class.getSimpleName();
    private static Log4d LOGGER = Log4d.getDefaultInstance();

    public static boolean syncWithDownloadPool(AppInfo app) {
        if (app == null) {
            return false;
        }
        ApkDownloadManager manager = ApkDownloadManager.getInstance();
        String pkgName = app.getPackageName();
        int progress = manager.checkAppProgress(pkgName);
        AppInfo.AppStatus status = manager.checkAppStatus(pkgName);
        if (progress != -1 && status != null) {
            LOGGER.debug(TAG, "[" + app.getName() + "]Compare with DownloadPool：Status = " + status + "  Progress = " + progress);
            app.getDownloadExtra().setProgress(progress);
            app.setStatus(status);
            return true;
        }
        return false;
    }

    public static boolean syncWithLocal(Context context, AppInfo app) {
        if (context == null || app == null) {
            return false;
        }
        Map<String, AppInfo> localAppMap = PackageUtils.pkgMapLocalApp(context);
        if (localAppMap.containsKey(app.getPackageName())) {
            AppInfo local = localAppMap.get(app.getPackageName());
            if (local.getVersion() < app.getVersion()) {
                //设置成可更新状态
                app.setStatus(AppInfo.AppStatus.CAN_UPDATE);
            } else {
                //设置成可打开状态
                app.setStatus(AppInfo.AppStatus.CAN_OPEN);
            }
            app.setStatusBeforeDownload();
            LOGGER.debug(TAG, "[" + app.getName() + "]Compare with Local：Status = " +
                    app.getStatus());
            return true;
        }
        LOGGER.warn(TAG, "[" + app.getName() + "]Compare with Local：Failed");
        return false;
    }

    public static boolean syncWithDb(DbHelper dbHelper, AppInfo app) {
        if (dbHelper == null || app == null) {
            return false;
        }
        Dao<AppInfo, String> appDao = dbHelper.getAppDao();
        try {
            AppInfo dbItem = appDao.queryForId(app.getPackageName());
            if (dbItem != null) {
                String appName = app.getName();
                AppInfo.AppStatus status = dbItem.getStatus();
                if (status.equals(AppInfo.AppStatus.DOWNLOADING)
                        || status.equals(AppInfo.AppStatus.WAIT_DOWNLOAD)) {
                    dbItem.setStatus(AppInfo.AppStatus.PAUSE_DOWNLOAD);
                    appDao.update(dbItem);
                }
                //如果不跟数据库的packageUrl进行同步，可能会在详情页出现点击无法安装的情况
                //还有可能出现的一种情况是，断点续传实际上是在旧的文件上添加新文件的字节码
                String lastUrl = dbItem.getPackageUrl();
                String newUrl = app.getPackageUrl();
                app.setPackageUrl(lastUrl);
                LOGGER.debug(TAG, "[" + appName + "]WAIT_INSTALL,NewDownloadUrl = " + newUrl + " ,LastDownloadUrl = " + lastUrl);
                LOGGER.info(TAG, "[" + appName + "]Compare with Db and Keep LastDownloadUrl");
                app.setStatus(dbItem.getStatus());
                LOGGER.debug(TAG, "[" + app.getName() + "]Compare with Db：Status = " +
                        app.getStatus());
                return true;
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return false;
    }

    /**
     * 应用信息同步。
     * 三重同步方式，比对顺序为：下载池-->数据库-->本地应用；
     * 其中某个比对渠道比对成功的话，不再进行下一个渠道的同步
     *
     * @param context
     * @param app
     */
    public static void sync(Context context, AppInfo app) {
        //应用信息三重同步，比对顺序为：下载池-->数据库-->本地应用
        if (!AppInfoSync.syncWithDownloadPool(app)) {
            DbHelper dbHelper = OpenHelperManager.getHelper(context, DbHelper.class);
            if (!AppInfoSync.syncWithDb(dbHelper, app)) {
                AppInfoSync.syncWithLocal(context, app);
            }
        }
//        if (!AppInfoSync.syncWithLocal(context, app)) {
//            if (!AppInfoSync.syncWithDownloadPool(app)) {
//                DbHelper dbHelper = OpenHelperManager.getHelper(context, DbHelper.class);
//                AppInfoSync.syncWithDb(dbHelper, app);
//            }
//        }
    }

    /**
     * 应用信息同步。
     * 同步逻辑：先与本地应用信息同步，如果当前应用为可更新状态
     *
     * @param context
     * @param app
     */
    public static void sync2(Context context, AppInfo app) {

    }

}
