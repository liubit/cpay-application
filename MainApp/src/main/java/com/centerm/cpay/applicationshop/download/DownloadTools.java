package com.centerm.cpay.applicationshop.download;

import android.os.Environment;
import android.util.Log;

import com.centerm.cpay.applicationshop.bean.AppInfo;
import com.centerm.cloudsys.sdk.common.utils.StringUtils;

import java.io.File;

import config.Config;

/**
 * 下载辅助工具
 */
public class DownloadTools {


    /**
     * 从下载地址中提取出需要下载的文件名
     *
     * @param url
     * @return
     */
    public static String extractFileNameFromUrl(String url) {
        if (url == null) {
            return null;
        }
        String[] strs = url.split("/");
        if (strs == null || strs.length == 0) {
            return null;
        } else {
            return strs[strs.length - 1];
        }
    }

    /**
     * 获取百分比
     *
     * @param size
     * @param totalSize
     * @return
     */
    public static int getPercent(long size, long totalSize) {
        int percent = totalSize == 0 ? 0 : (int) (((double) size / totalSize * 100));
        return percent;
    }

    public static File getApkFile(AppInfo app) {
        if (app == null) {
            return null;
        }
        String fileName = extractFileNameFromUrl(app.getPackageUrl());
        String path = Config.FILE_DOWNLOADER.APK_PATH;
        String filePath = Environment.getExternalStorageDirectory().toString() + path + fileName;
        Log.e("要删除的路径地址", filePath);
        File file = new File(filePath);
        return file;
    }

    public static File getApkFiles(AppInfo app) {
        if (app == null) {
            return null;
        }
        String fileName = extractFileNameFromUrl(app.getPackageUrl());
        String path = Config.FILE_DOWNLOADER.APK_PATH;
        String filePath = Environment.getExternalStorageDirectory().toString() + path + fileName;
        Log.e("要删除的路径地址", filePath);
        File file = new File(filePath);
        if (!file.exists()) {
            file = new File(filePath + ".tmp");
            Log.e("是否存在", file.exists() + "");
            Log.e("添加后要删除的路径地址", filePath + ".tmp");
        }
        return file;
    }

    public static File getAdFile(String url) {
        if (url == null || StringUtils.isStrNull(url)) {
            return null;
        }
        String fileName = extractFileNameFromUrl(url);
        String path = Config.FILE_DOWNLOADER.AD_PATH;
        String filePath = Environment.getExternalStorageDirectory().toString() + path + fileName;
        return new File(filePath);
    }

}
