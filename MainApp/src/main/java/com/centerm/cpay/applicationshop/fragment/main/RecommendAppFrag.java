package com.centerm.cpay.applicationshop.fragment.main;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.animation.DecelerateInterpolator;
import android.widget.AdapterView;
import android.widget.GridView;

import com.centerm.cpay.applicationshop.R;
import com.centerm.cpay.applicationshop.activity.WebActivity;
import com.centerm.cpay.applicationshop.adapter.AdBannerAdapter;
import com.centerm.cpay.applicationshop.adapter.MainAppGridAdapter;
import com.centerm.cpay.applicationshop.base.BaseDownloadFragment;
import com.centerm.cpay.applicationshop.bean.Advertisement;
import com.centerm.cpay.applicationshop.bean.AppInfo;
import com.centerm.cpay.applicationshop.common.BroadcastAction;
import com.centerm.cpay.applicationshop.common.KeyName;
import com.centerm.cpay.applicationshop.net.ResponseHandler;
import com.centerm.cpay.applicationshop.net.cont.Method;
import com.centerm.cpay.applicationshop.net.request.GetAdListReq;
import com.centerm.cpay.applicationshop.net.request.GetAppListReq;
import com.centerm.cpay.applicationshop.net.response.AdListParser;
import com.centerm.cpay.applicationshop.net.response.AppListParser;
import com.centerm.cpay.applicationshop.utils.FileUtils;
import com.centerm.cpay.applicationshop.utils.ViewUtils;
import com.centerm.cpay.applicationshop.view.HeaderGridView;
import com.centerm.cpay.applicationshop.view.banner.CBViewHolderCreator;
import com.centerm.cpay.applicationshop.view.banner.ConvenientBanner;
import com.centerm.cpay.applicationshop.view.banner.transform.DepthPageTransformer;
import com.centerm.cpay.applicationshop.view.banner.transform.ZoomInTransformer;
import com.centerm.cloudsys.sdk.common.utils.StringUtils;
import com.cjj.MaterialRefreshLayout;
import com.cjj.MaterialRefreshListener;
import com.daimajia.slider.library.Indicators.PagerIndicator;
import com.daimajia.slider.library.SliderLayout;
import com.daimajia.slider.library.SliderTypes.BaseSliderView;
import com.daimajia.slider.library.SliderTypes.DefaultSliderView;
import com.daimajia.slider.library.Tricks.ViewPagerEx;

import java.util.ArrayList;
import java.util.List;

import config.Config;

/**
 * Created by linwanliang on 2016/6/24.
 * 推荐应用列表界面
 */
public class RecommendAppFrag extends BaseDownloadFragment
        implements BaseSliderView.OnSliderClickListener, ViewPagerEx.OnPageChangeListener {
    private MaterialRefreshLayout refreshLayout;
    private SliderLayout sliderLayout;
    private PagerIndicator indicator;
    private MainAppGridAdapter adapter;
    private HeaderGridView gridView;
    private String apkTypeId;
    private int total = -1;
    private int pageIndex = 0;
    private final List<Advertisement> defaultAdCollection = new ArrayList<>();
    private List<Advertisement> adCollection;

    private String DEMO_STRING;

    @Override
    public int onInitLayoutId() {
        return R.layout.main_fragment_recommend;
    }

    @Override
    public void onInitLocalData() {
        if (Config.DEMO) {
            DEMO_STRING = FileUtils.readAssetsText(getActivity().getAssets(), "json/" + Method.GET_APP_LIST.replace("/", "_"));
        }
        apkTypeId = "90001";
        Advertisement ad1 = new Advertisement(R.drawable.ad01);
        Advertisement ad2 = new Advertisement(R.drawable.ad02);
        Advertisement ad3 = new Advertisement(R.drawable.ad03);
        defaultAdCollection.add(ad1);
        defaultAdCollection.add(ad2);
        defaultAdCollection.add(ad3);
        adCollection = new ArrayList<>();
    }

    @Override
    public void onInitView(View view) {
        gridView = (HeaderGridView) view.findViewById(R.id.grid_v);
        initGridView(gridView);
        refreshLayout = (MaterialRefreshLayout) view.findViewById(R.id.refresh_layout);
        initRefreshView(refreshLayout);
        initAdView();
    }

    @Override
    public void doThingsAfterInitView() {
        //将广告视图添加到头部
//        refreshAdView(true);
        if (isCreated) {
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    gridView.setAdapter(adapter);
                }
            }, Config.DELAY_LOAD_DATA);
        } else {
//            if (Config.DEMO) {
//                showLoading();
//                AppListParser parser = new AppListParser(getActivity(), DEMO_STRING);
//                this.total = parser.getTotal();
//                adapter = new MainAppGridAdapter(context, parser.getAppList(), gridView.getNumColumns());
//                adapter.setResourceId(R.layout.main_app_grid_item);
//                gridView.setAdapter(adapter);
//                new Handler().postDelayed(new Runnable() {
//                    @Override
//                    public void run() {
//                        stopLoading();
//                    }
//                }, 2000);
//            }
//            else {
            requestAdList(true);
            requestAppList(true, true);
//            }
        }
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    private void initRefreshView(MaterialRefreshLayout view) {
        if (Config.DEMO) {
            view.setLoadMore(true);
        } else {
            view.setLoadMore(false);
        }
        view.setMaterialRefreshListener(new MaterialRefreshListener() {
            @Override
            public void onRefresh(final MaterialRefreshLayout materialRefreshLayout) {
                if (Config.DEMO) {
                    materialRefreshLayout.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            materialRefreshLayout.finishRefresh();
                        }
                    }, 3000);
                } else {
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            requestAppList(true, false);
                            requestAdList(true);
                        }
                    }, Config.DELAY_LOAD_DATA);
                }
            }

            @Override
            public void onfinish() {
            }

            @Override
            public void onRefreshLoadMore(final MaterialRefreshLayout materialRefreshLayout) {
//                if (Config.DEMO) {
//                    materialRefreshLayout.postDelayed(new Runnable() {
//                        @Override
//                        public void run() {
//                            AppListParser parser = new AppListParser(getActivity(), DEMO_STRING);
//                            adapter.notifyDataChanged(parser.getAppList(), true);
//                            materialRefreshLayout.finishRefreshLoadMore();
//                        }
//                    }, 3000);
//                } else {
                requestAppList(false, false);
//                }
            }
        });
    }

    private void initGridView(final GridView gridView) {
        gridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                if (!canPerformClick()) {
                    return;
                }
                AppInfo info = ((MainAppGridAdapter.ViewBinder) view.getTag()).getBindedData();
                if (info.getPackageName() == null) {
                    return;
                }
                Intent intent = new Intent();
                intent.setAction(BroadcastAction.ACTION_OPEN_APPDETAIL);
//                AppDetailActivity.data = info;
                intent.putExtra(KeyName.APP_INFO, info);
                context.sendBroadcast(intent);
            }
        });
    }

    private void initAdView() {
        if (gridView == null) {
            return;
        }
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate(R.layout.main_ad_layout, null);
        sliderLayout = (SliderLayout) view.findViewById(R.id.slider_layout);
        indicator = (PagerIndicator) view.findViewById(R.id.page_indicator);
        sliderLayout.setPresetIndicator(SliderLayout.PresetIndicators.Center_Bottom);
        sliderLayout.setDuration(Config.AD_SCROLL_TIME);
        sliderLayout.addOnPageChangeListener(this);
        if (indicator != null) {
            sliderLayout.setCustomIndicator(indicator);
        }
        gridView.addHeaderView(view);
    }

    private void refreshAdView(boolean showDefault) {
        if (Config.DEMO || adCollection == null || adCollection.size() == 0) {
            if (showDefault) {
                if (adCollection == null) {
                    adCollection = new ArrayList<>();
                }
                adCollection.clear();
                adCollection.addAll(defaultAdCollection);
            }
        }
        sliderLayout.removeAllSliders();
        for (Advertisement item : adCollection) {
            DefaultSliderView sliderView = new DefaultSliderView(context);
            Bundle bundle = new Bundle();
            bundle.putSerializable("item", item);
            sliderView.bundle(bundle);//配置广告参数
            if (item.getDrawableId() != -1) {
                sliderView.image(item.getDrawableId());
            } else {
                sliderView.image(item.getImgUrl()).error(R.drawable.default_ad_img)
                        .empty(R.drawable.default_ad_img);
            }
            sliderView.setOnSliderClickListener(this);
            sliderLayout.addSlider(sliderView);
        }
//        sliderLayout.startAutoCycle();
    }

    private void requestAppList(final boolean refresh, boolean showLoading) {
        int c = adapter == null ? 0 : adapter.getCount();//当前列表数量
        int pi = this.pageIndex + 1;//页码
        if (refresh) {
            pi = 1;
        } else {
            if (c >= total) {
                return;
            }
        }
        if (showLoading) {
            showLoading();
        }
        final GetAppListReq req = new GetAppListReq(context);
        req.setApkTypeId(apkTypeId);
        req.setPageIndex(pi);
        ResponseHandler handler = new ResponseHandler() {
            @Override
            public void onSuccess(String response) {
                stopLoading();
                refreshLayout.finishRefresh();
                refreshLayout.finishRefreshLoadMore();
                AppListParser parser = new AppListParser(context, response);
                if (parser.hasData()) {
                    total = parser.getTotal();
                    pageIndex = req.getPageIndex();
                    if (adapter == null) {
                        adapter = new MainAppGridAdapter(context, parser.getAppList(), gridView.getNumColumns());
                        adapter.setResourceId(R.layout.main_app_grid_item);
                        gridView.setAdapter(adapter);
                    } else {
                        gridView.setAdapter(adapter);
                        adapter.notifyDataChanged(parser.getAppList(), !refresh);
                    }
                    int counts = adapter == null ? 0 : adapter.getCount();
                    if (counts < total) {
                        refreshLayout.setLoadMore(true);
                    } else {
                        refreshLayout.setLoadMore(false);
                    }
                } else {
                    ViewUtils.showToast(context, parser.getMsg());
                }
            }

            @Override
            public void onFailure(int errorCode, String errorInfo) {
                stopLoading();
                refreshLayout.finishRefresh();
                refreshLayout.finishRefreshLoadMore();
                ViewUtils.showToast(context, errorInfo);
            }
        };
        getHttpClient().post(context, req, handler);
    }

    private void requestAdList(final boolean defaultIfFailed) {
        GetAdListReq req = new GetAdListReq(context);
        ResponseHandler handler = new ResponseHandler() {
            @Override
            public void onSuccess(String response) {
                AdListParser parser = new AdListParser(response);
                adCollection.clear();
                if (parser.getAdList() != null) {
                    adCollection.addAll(parser.getAdList());
                    refreshAdView(true);
                }
            }

            @Override
            public void onFailure(int errorCode, String errorInfo) {
                adCollection.clear();
                refreshAdView(true);
            }
        };
        getHttpClient().post(context, req, handler);
    }

    private void makeAd(Advertisement item) {
        String type = item.getType();
        if (!TextUtils.isEmpty(type)) {
            if (type.equals("2") || type.equals("4")) {
                //网页类
                Intent intent = new Intent();
                intent.setAction(BroadcastAction.ACTION_WEB_ACTIVITY);
                intent.putExtra("ad", item);
                context.sendBroadcast(intent);
            } else if (type.equals("3")) {
                //TODO 流媒体
            }
        }
    }


    @Override
    public void onStart() {
        sliderLayout.startAutoCycle();
        super.onStart();
    }

    @Override
    public void onStop() {
        sliderLayout.stopAutoCycle();
        super.onStop();
    }

    @Override
    public void onPause() {
        super.onPause();
    }

    @Override
    public void onSliderClick(BaseSliderView slider) {
        Bundle bundle = slider.getBundle();//获取点击的slider广告参数
        Advertisement item = (Advertisement) bundle.getSerializable("item");
        if (item != null) {
            makeAd(item);
        }
    }

    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

    }

    @Override
    public void onPageSelected(int position) {

    }

    @Override
    public void onPageScrollStateChanged(int state) {

    }


    @Override
    public void onReceiveAppStatus(final Context context, Intent intent) {
        if (!isCreated || adapter == null) {
            LOGGER.verbose(TAG, "未初始化，不处理广播");
            return;
        }
        String action = intent.getAction();
        if (action.equals(BroadcastAction.DOWNLOAD.ACTION_PAUSE_ALL)) {
            adapter.notifyAllPaused();
        } else if (action.equals(Intent.ACTION_PACKAGE_ADDED)) {
            String pkgPrefix = intent.getData().getSchemeSpecificPart();
            adapter.notifyPackageInstalled(pkgPrefix);
        } else {
            AppInfo app = (AppInfo) intent.getSerializableExtra(KeyName.APP_INFO);
            adapter.notifyItemStatusChanged(
                    app.getPackageName(),
                    app.getStatus(),
                    app.getDownloadExtra().getProgress());
        }
    }
}