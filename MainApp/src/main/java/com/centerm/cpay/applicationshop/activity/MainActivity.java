package com.centerm.cpay.applicationshop.activity;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.res.Configuration;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.widget.LinearLayout;

import com.centerm.cpay.applicationshop.MyApplication;
import com.centerm.cpay.applicationshop.R;
import com.centerm.cpay.applicationshop.adapter.MainPagerAdapter;
import com.centerm.cpay.applicationshop.base.BaseDownloadActivity;
import com.centerm.cpay.applicationshop.bean.AppType;
import com.centerm.cpay.applicationshop.common.BroadcastAction;
import com.centerm.cpay.applicationshop.common.DialogFactory;
import com.centerm.cpay.applicationshop.common.KeyName;
import com.centerm.cpay.applicationshop.common.MaterialDialogManager;
import com.centerm.cpay.applicationshop.download.ApkDownloadManager;
import com.centerm.cpay.applicationshop.fragment.main.MainAppFrag;
import com.centerm.cpay.applicationshop.fragment.main.MainAppFragHD;
import com.centerm.cpay.applicationshop.fragment.main.SortAppFrag;
import com.centerm.cpay.applicationshop.fragment.main.TopAppFrag;
import com.centerm.cpay.applicationshop.utils.ViewUtils;
import com.centerm.cpay.midsdk.dev.DeviceFactory;

import java.io.File;
import java.io.Serializable;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;

/**
 * 主界面
 */
public class MainActivity extends BaseDownloadActivity implements View.OnClickListener {
    private String[] TAB_NAMES = new String[]{"推荐", "分类", "排行"};
    private MyReceiver receiver;
    private ViewPager pager;
    private List<Fragment> fragmentList;

    @Override
    public int onInitLayoutId() {
        return R.layout.activity_main;
    }

    @Override
    public void onInitLocalData() {
        TAB_NAMES = getResources().getStringArray(R.array.main_tab);
        isReceivePackageChanged = true;
        String typeRes = getIntent().getStringExtra(KeyName.TYPE_RESPONSE);
        fragmentList = new ArrayList<>();
        if(MyApplication.is960F()){
            fragmentList.add(new MainAppFragHD());
        }else {
            fragmentList.add(new MainAppFrag());
        }

//        fragmentList.add(new RecommendAppFrag());
        SortAppFrag sortFrag = new SortAppFrag();
        Bundle bundle = new Bundle();
        bundle.putString(KeyName.TYPE_RESPONSE, typeRes);
        sortFrag.setArguments(bundle);
        fragmentList.add(sortFrag);
        fragmentList.add(new TopAppFrag());
    }

    @Override
    public void onInitView(View view) {
        pager = (ViewPager) findViewById(R.id.view_pager);
        TabLayout tabLayout = (TabLayout) findViewById(R.id.tab_layout);
        initTabLayout(tabLayout, pager);
        findViewById(R.id.search_btn).setOnClickListener(this);
        findViewById(R.id.manage_btn).setOnClickListener(this);

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 100) {
            logger.warn(TAG, "系统安装程序返回结果码 " + resultCode);
        }
    }

    @Override
    public void doThingsAfterInitView() {
        super.doThingsAfterInitView();
        receiver = new MyReceiver();
        IntentFilter filter = new IntentFilter();
        filter.addAction(BroadcastAction.ACTION_OPEN_APPDETAIL);
        filter.addAction(BroadcastAction.ACTION_OPEN_MORE_APP);
        filter.addAction(BroadcastAction.ACTION_WEB_ACTIVITY);
        registerReceiver(receiver, filter);
        if (this.getResources().getConfiguration().orientation == Configuration.ORIENTATION_LANDSCAPE) {
            logger.error(TAG, "landscape");
        } else if (this.getResources().getConfiguration().orientation == Configuration.ORIENTATION_PORTRAIT) {
            logger.error(TAG, "portrait");
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        DeviceFactory.getInstance().release();
        if (receiver != null) {
            unregisterReceiver(receiver);
        }
        ApkDownloadManager.getInstance().destroyPauseAll(context);
    }

    @Override
    public void onReceiveAppStatus(Context context, Intent intent) {
        String action = intent.getAction();
        logger.debug(TAG, "receive：" + action);
        if (action.equals(Intent.ACTION_PACKAGE_ADDED)) {
            String pkgPrefix = intent.getData().getSchemeSpecificPart();
            ApkDownloadManager.getInstance().onPackageInstalled(context, pkgPrefix);
        }
        //分发给所有的Fragmgent去处理
        if (fragmentList.get(2) instanceof TopAppFrag) {
            ((TopAppFrag) fragmentList.get(2)).onReceiveAppStatus(context, intent);
        }
        if (fragmentList.get(0) instanceof MainAppFrag) {
            ((MainAppFrag) fragmentList.get(0)).onReceiveAppStatus(context, intent);
        }
        /*for (int i = 0; i < fragmentList.size(); i++) {
            Fragment frag = fragmentList.get(i);
            if (frag instanceof AppStatusReceiver) {
                ((AppStatusReceiver) frag).onReceiveAppStatus(context, intent);
            }
        }*/
/*
        Fragment current = fragmentList.get(pager.getCurrentItem());
        if (fragmentList.get(2) instanceof TopAppFrag) {
            ((TopAppFrag) fragmentList.get(2)).onReceiveAppStatus(context, intent);
        }
        if (current instanceof AppStatusReceiver && !(current instanceof TopAppFrag)) {
            ((AppStatusReceiver) current).onReceiveAppStatus(context, intent);
        }*/
    }

    private void initTabLayout(final TabLayout tabLayout, final ViewPager pager) {
        MainPagerAdapter adapter = new MainPagerAdapter(getSupportFragmentManager(), fragmentList);
        pager.setAdapter(adapter);

        tabLayout.setupWithViewPager(pager);
        tabLayout.setTabMode(TabLayout.MODE_FIXED);
        for (int i = 0; i < TAB_NAMES.length; i++) {
            TabLayout.Tab tab = tabLayout.getTabAt(i);
            tab.setText(TAB_NAMES[i]);
        }
        if(Build.VERSION.SDK_INT==Build.VERSION_CODES.JELLY_BEAN_MR1) {
            Class<?> mTabLayout = tabLayout.getClass();
            try {
                Field tabStrip = mTabLayout.getDeclaredField("mTabStrip");
                tabStrip.setAccessible(true);
                LinearLayout ll_tab = (LinearLayout) tabStrip.get(tabLayout);
                for (int i = 0; i < ll_tab.getChildCount(); i++) {
                    View child = ll_tab.getChildAt(i);
                    //child.setPadding(0, 0, 0, 0);
                    LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(0, LinearLayout.LayoutParams.MATCH_PARENT, 1);
                    params.leftMargin = (ViewUtils.dip2px(this, 20f));
                    params.rightMargin = (ViewUtils.dip2px(this, 20f));
                    child.setLayoutParams(params);
                    child.invalidate();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void onClick(View v) {
        Intent intent = null;
        switch (v.getId()) {
            case R.id.search_btn:
                intent = new Intent(this, SearchActivity.class);
                jumpTo(intent, v, getResources().getString(R.string.trans_name_search));
                break;
            case R.id.manage_btn:
                intent = new Intent(this, ManageEntryActivity.class);
                jumpTo(intent);
                break;
        }
    }

    private class MyReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            logger.verbose(TAG, "RECEIVE  :    " + action);
            if (BroadcastAction.ACTION_OPEN_APPDETAIL.equals(action)) {
                Intent i = new Intent(context, AppDetailActivity.class);
                i.putExtra(KeyName.APP_INFO, intent.getSerializableExtra(KeyName.APP_INFO));
                jumpTo(i);
            } else if (BroadcastAction.ACTION_OPEN_MORE_APP.equals(action)) {
                Serializable s = intent.getSerializableExtra(KeyName.APP_TYPE);
                if (s != null) {
                    AppType type = (AppType) s;
                    Intent i = new Intent(context, MoreTypeAppActivity.class);
                    i.putExtra(KeyName.APP_TYPE, type);
                    jumpTo(i);
                } else {
                    logger.warn(TAG, "未携带有效数据集，无法跳转更多页面");
                }
            } else if (BroadcastAction.ACTION_INSTALL_APP.equals(action)) {
                String apkPath = intent.getStringExtra(KeyName.APK_PATH);
                File file = new File(apkPath);
                if (apkPath.endsWith(".apk") && file.exists()) {
                    Intent i = new Intent("android.intent.action.VIEW");
                    i.setFlags(276824064);
                    i.setDataAndType(Uri.fromFile(new File(apkPath)), "application/vnd.android.package-archive");
                    startActivityForResult(intent, 100);
                } else {
                    logger.warn(TAG, "[安装失败] " + apkPath);
                }
            } else if (BroadcastAction.ACTION_WEB_ACTIVITY.equals(action)) {
                Intent i = new Intent(context, WebActivity.class);
                i.putExtra("ad", intent.getSerializableExtra("ad"));
                jumpTo(i);
            } else if (action.equals(Intent.ACTION_PACKAGE_REMOVED)) {
                logger.warn(TAG, "[卸载应用]");
                MainAppFrag.isRefresh = true;
                TopAppFrag.isRefresh = true;
                SortAppFrag.isRefresh = true;
            }
        }
    }

    @Override
    public void onBackPressed() {
        MaterialDialogManager.getChooser(this, R.string.p_close, new MaterialDialogManager.ButtonClick() {
            @Override
            public void onClick() {
                finish();
            }
        }).show();
    }

    public interface PageChangeListener {
        void onPageSelected();
    }

    @Override
    protected void onPause() {
        DialogFactory.hideAll();
        super.onPause();
    }
}
