package com.centerm.cpay.applicationshop.bean;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

/**
 * author: linwanliang</br>
 * date:2016/7/5</br>
 */
@DatabaseTable(tableName = "tb_search_key")
public class SearchKey {
    @DatabaseField(generatedId = true)
    private int id;
    @DatabaseField(unique = true)
    private String key;

    public SearchKey() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }
}
