package com.centerm.cpay.applicationshop.utils;


import android.app.Activity;
import android.content.ActivityNotFoundException;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ApplicationInfo;
import android.content.pm.IPackageStatsObserver;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageStats;
import android.content.pm.ResolveInfo;
import android.net.Uri;
import android.os.ConditionVariable;
import android.os.Handler;
import android.os.Message;
import android.os.RemoteException;

import com.centerm.cpay.applicationshop.bean.AppInfo;
import com.centerm.cloudsys.sdk.common.log.Log4d;

import java.io.File;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import config.Config;

public class PackageUtils extends com.centerm.cloudsys.sdk.common.utils.PackageUtils {

    private static final String TAG = PackageUtils.class.getSimpleName();

    /**
     * 获取非系统应用列表，获取过程可能比较耗时，因此采用回调方式。
     *
     * @param context  Context
     * @param callback 回调方法
     */
    public static void obtainNonSystemAppList(final Context context, final ObtainAppInfoCallback callback) {
        if (callback == null) {
            return;
        }
        if (context == null) {
            callback.onResult(null);
            return;
        }
        final Handler handler = new Handler(context.getMainLooper()) {
            @Override
            public void handleMessage(Message msg) {
                callback.onResult((List<AppInfo>) msg.obj);
            }
        };
        new Thread(new Runnable() {
            @Override
            public void run() {
                List<ResolveInfo> resolveInfoList;
                List<ResolveInfo> nonSysResolveInfoList = new ArrayList<ResolveInfo>();
                List<AppInfo> appList = new ArrayList<AppInfo>();
                PackageManager pManager = context.getPackageManager();
                Intent intent = new Intent(Intent.ACTION_MAIN);
                intent.addCategory(Intent.CATEGORY_LAUNCHER);
                resolveInfoList = pManager.queryIntentActivities(intent, 0);
                Collections.sort(resolveInfoList, new ResolveInfo.DisplayNameComparator(pManager));
                for (ResolveInfo app : resolveInfoList) {
                    String pkgName = app.activityInfo.packageName;
                    try {
                        PackageInfo pkgInfo = pManager.getPackageInfo(pkgName, 0);
                        if ((pkgInfo.applicationInfo.flags & ApplicationInfo.FLAG_SYSTEM) == 0) {
                            final AppInfo appItem = AppInfo.convert(context, app);
                            //过滤掉部分应用
                            if (Config.MASK_APP_SETS.contains(appItem.getPackageName())) {
                                continue;
                            }
                            //利用反射机制去获取包大小
                            final ConditionVariable cv = new ConditionVariable();
                            getPkgSize(context, appItem.getPackageName(), new IPackageStatsObserver.Stub() {

                                @Override
                                public void onGetStatsCompleted(PackageStats pStats, boolean succeeded) throws RemoteException {
                                    if (succeeded) {
                                        appItem.setSize(pStats.codeSize);
                                    }
                                    cv.open();
                                }
                            });
                            cv.block();
                            appList.add(appItem);
                            nonSysResolveInfoList.add(app);
                        }
                    } catch (PackageManager.NameNotFoundException e) {
                        e.printStackTrace();
                    }
                }
                Message msg = new Message();
                msg.obj = appList;
                handler.sendMessage(msg);
            }
        }).start();
    }

    public static Map<String, AppInfo> pkgMapLocalApp(Context context) {
        Map<String, AppInfo> localAppMap = new HashMap<String, AppInfo>();
        if (context == null) {
            return null;
        }
        List<ResolveInfo> resolveInfoList;
        PackageManager pManager = context.getPackageManager();
        Intent intent = new Intent(Intent.ACTION_MAIN);
        intent.addCategory(Intent.CATEGORY_LAUNCHER);
        resolveInfoList = pManager.queryIntentActivities(intent, 0);
        Collections.sort(resolveInfoList, new ResolveInfo.DisplayNameComparator(pManager));
        for (ResolveInfo app : resolveInfoList) {
            try {
                AppInfo bean = AppInfo.convert(context, app);
                localAppMap.put(bean.getPackageName(), bean);
            } catch (PackageManager.NameNotFoundException e) {
                e.printStackTrace();
            }
        }
        return localAppMap;
    }


    /**
     * 利用反射机制获取应用占用空间大小
     *
     * @param context  Context
     * @param pkgName  包名
     * @param observer 观察者
     */
    public static void getPkgSize(Context context, String pkgName, IPackageStatsObserver observer) {
        Method method = null;
        PackageManager pm = context.getPackageManager();
        try {
            method = pm.getClass().getMethod("getPackageSizeInfo", String.class,
                    IPackageStatsObserver.class);
            method.invoke(pm, pkgName, observer);
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }
    }

    public static boolean installApp2(Activity activity, String apkPath) {
        Log4d logger = Log4d.getDefaultInstance();
        File file = new File(apkPath);
        if (apkPath.endsWith(".apk") && file.exists()) {
            Intent intent = new Intent("android.intent.action.VIEW");
            intent.setFlags(276824064);
            intent.setDataAndType(Uri.fromFile(new File(apkPath)), "application/vnd.android.package-archive");
            activity.startActivityForResult(intent, 100);
            return true;
        } else {
            logger.warn(TAG, "[安装失败] " + apkPath);
            return false;
        }
    }

    /**
     * 获取应用信息列表的回调方法
     */
    public interface ObtainAppInfoCallback {
        void onResult(List<AppInfo> appList);
    }

    /**
     * 打开指定包名的应用
     *
     * @param context
     * @param packageName
     */
    public static boolean openActivity(Context context, String packageName, String activityUrl) {
        Intent intent = new Intent();
        ComponentName component = new ComponentName(packageName, activityUrl);
        intent.setComponent(component);
        try {
            context.startActivity(intent);
            return true;
        } catch (ActivityNotFoundException e) {
            return false;
        }
    }
}
