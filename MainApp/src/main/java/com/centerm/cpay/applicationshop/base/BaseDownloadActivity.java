package com.centerm.cpay.applicationshop.base;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.os.Handler;

import com.centerm.cpay.applicationshop.common.BroadcastAction;
import com.centerm.cpay.applicationshop.download.AppStatusReceiver;

/**
 * Created by linwanliang on 2016/6/21.
 * 可接收下载状态的基础Activity
 */
public abstract class BaseDownloadActivity extends BaseActivity_v21 implements AppStatusReceiver {

    private BroadcastReceiver receiver;
    private boolean isRegister;
    private String lastAction;
    protected boolean isReceivePackageChanged;
    private boolean receiveProgressFlag = true;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                register();
            }
        }, 1000);
    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
        unregister();
    }

    private void register() {
        IntentFilter filter = new IntentFilter();
        filter.addAction(BroadcastAction.DOWNLOAD.ACTION_START);
        filter.addAction(BroadcastAction.DOWNLOAD.ACTION_PAUSE);
        filter.addAction(BroadcastAction.DOWNLOAD.ACTION_PAUSE_ALL);
        filter.addAction(BroadcastAction.DOWNLOAD.ACTION_PROGRESS);
        filter.addAction(BroadcastAction.DOWNLOAD.ACTION_SUCCESS);
        filter.addAction(BroadcastAction.DOWNLOAD.ACTION_CANCELED);
        filter.addAction(BroadcastAction.DOWNLOAD.ACTION_FAILED);
        filter.addAction(BroadcastAction.INSTALL.ACTION_SUCCESS);
        filter.addAction(BroadcastAction.INSTALL.ACTION_FAILED);
        receiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                if (canDiaptchStatusEvent(intent.getAction())) {
                    onReceiveAppStatus(context, intent);
                }
            }
        };
        //应用安装广播，静默安装需要反注册掉该Action
        if (isReceivePackageChanged) {
            registerPackageAddedReceiver();
        }
        registerReceiver(receiver, filter);
        isRegister = true;
    }

    private void registerPackageAddedReceiver() {
        IntentFilter filter = new IntentFilter();
        filter.addAction(Intent.ACTION_PACKAGE_ADDED);
        filter.addAction(Intent.ACTION_PACKAGE_REMOVED);
        filter.addDataScheme("package");
        registerReceiver(receiver, filter);
    }

    private void unregister() {
        if (isRegister && receiver != null) {
            unregisterReceiver(receiver);
            isRegister = false;
        }
    }

    private boolean canDiaptchStatusEvent(String action) {
        if (Intent.ACTION_PACKAGE_ADDED.equals(action)) {
            return true;
        }
        return receiveProgressFlag;
    }

    protected void pauseReceiveProgress() {
        this.receiveProgressFlag = false;
    }

    protected void resumeReceiveProgress() {
        this.receiveProgressFlag = true;
    }
}
