package com.centerm.cpay.applicationshop.bean;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

import java.io.Serializable;

/**
 * Created by linwanliang on 2016/6/22.
 */
@DatabaseTable(tableName = "tb_ad")
public class Advertisement implements Serializable {

    @DatabaseField(generatedId = true)
    private int id;
    @DatabaseField
    private String imgUrl;//预览图片地址
    @DatabaseField
    private String type;//类型。0-普通图片广告，1-html5广告，2-流媒体广告
    @DatabaseField
    private String fileUrl;//文件地址。html5广告和流媒体广告必选字段，广告文件的下载地址

    private String fileMd5;

    private int drawableId = -1;

    public Advertisement() {
    }

    public Advertisement(int drawaleId) {
        this.type = "0";
        this.drawableId = drawaleId;
    }

    public Advertisement(String type, String imgUrl, String fileUrl) {
        this.type = type;
        this.imgUrl = imgUrl;
        this.fileUrl = fileUrl;
    }

    public Advertisement(String type, String imgUrl, String fileUrl, String fileMd5) {
        this(type, imgUrl, fileUrl);
        this.fileMd5 = fileMd5;
    }

    public String getFileUrl() {
        return fileUrl;
    }

    public void setFileUrl(String fileUrl) {
        this.fileUrl = fileUrl;
    }

    public String getImgUrl() {
        return imgUrl;
    }

    public void setImgUrl(String imgUrl) {
        this.imgUrl = imgUrl;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public int getDrawableId() {
        return drawableId;
    }

    public void setDrawableId(int drawableId) {
        this.drawableId = drawableId;
    }

    public String getFileMd5() {
        return fileMd5;
    }

    public void setFileMd5(String fileMd5) {
        this.fileMd5 = fileMd5;
    }

    @Override
    public String toString() {
        return "Advertisement{" +
                "fileUrl='" + fileUrl + '\'' +
                ", imgUrl='" + imgUrl + '\'' +
                ", type='" + type + '\'' +
                '}';
    }
}
