package com.centerm.cpay.applicationshop.net.request;

import android.content.Context;

import com.centerm.cpay.applicationshop.net.BaseRequest;
import com.centerm.cpay.applicationshop.net.cont.JsonKey;
import com.centerm.cpay.applicationshop.net.cont.Method;

import org.json.JSONException;
import org.json.JSONObject;

import config.Config;

/**
 * author: linwanliang</br>
 * date:2016/7/11</br>
 */
public class SearchAppReq extends BaseRequest {
    public SearchAppReq(Context context) {
        super(context);
        setMethod(Method.SEARCH_APP);
        JSONObject body = new JSONObject();
        try {
            body.put(JsonKey.screenType, Config.SCREEN_TYPE);
            body.put(JsonKey.pageSize, Config.DEFAULT_APP_PAGE_SIZE);
            body.put(JsonKey.pageIndex, 1);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        addBody(body);
    }

    public void setKeyword(String keyword) {
        try {
            getBody().put(JsonKey.keyword, keyword);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

}
