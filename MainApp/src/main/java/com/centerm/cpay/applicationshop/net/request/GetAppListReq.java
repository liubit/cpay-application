package com.centerm.cpay.applicationshop.net.request;

import android.content.Context;

import com.centerm.cpay.applicationshop.net.BaseRequest;
import com.centerm.cpay.applicationshop.net.cont.Method;

import org.json.JSONException;
import org.json.JSONObject;

import config.Config;

import static com.centerm.cpay.applicationshop.net.cont.JsonKey.*;

/**
 * author: linwanliang</br>
 * date:2016/6/29</br>
 */
public class GetAppListReq extends BaseRequest {
    private int pi = -1;

    public GetAppListReq(Context context) {
        super(context);
        setMethod(Method.GET_APP_LIST);
        JSONObject body = new JSONObject();
        try {
            body.put(pageSize, Config.DEFAULT_APP_PAGE_SIZE);
            body.put(screenType, Config.SCREEN_TYPE);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        addBody(body);
    }

    public void setApkTypeId(String id) {
        try {
            getBody().put(apkTypeId, id);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public void setPageIndex(int index) {
        this.pi = index;
        try {
            getBody().put(pageIndex, "" + index);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public int getPageIndex() {
        return pi;
    }


}
