package com.centerm.cpay.applicationshop.view;

import android.content.Context;
import android.content.Intent;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RatingBar;
import android.widget.TextView;

import com.centerm.cpay.applicationshop.R;
import com.centerm.cpay.applicationshop.bean.AppInfo;
import com.centerm.cpay.applicationshop.common.BroadcastAction;
import com.centerm.cpay.applicationshop.common.KeyName;
import com.centerm.cpay.applicationshop.utils.AppViewHelper;
import com.centerm.cloudsys.sdk.common.utils.FileUtils;
import com.nostra13.universalimageloader.core.ImageLoader;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import config.Config;


public class Top3AppViewManager implements View.OnClickListener {

    private final static int LAYOUT_ID = R.layout.main_top3_view;
    private Context context;
    private View view;
    private Map<String, View> idMapView = new HashMap<>();
    private long lastClick;
    private List<AppInfo> appList = new ArrayList<>();


    public Top3AppViewManager(Context context) {
        this.context = context;
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        view = inflater.inflate(LAYOUT_ID, null);
    }

    public boolean notifyDataSetChanged(List<AppInfo> data) {
        if (data == null) {
            return false;
        }
        appList.clear();
        appList.addAll(data);
        return notifyDataSetChanged();
    }

    /**
     * 这里要做一件事情，就是过滤掉掉出排行的应用，不然掉出排行的应用还在下载，会更新这个视图。
     */
    private void filterAppsOutOfTopView(List<AppInfo> apps) {
        List<String> filterApps = new ArrayList<>();
        if (idMapView != null && idMapView.size() > 0) {
            Set<String> keys = idMapView.keySet();
            for (Iterator<String> it = keys.iterator(); it.hasNext(); ) {
                String pkgName = it.next();//获取下载任务里面的包名，如果这个包名不在排行，则去掉
                if (!isIntoTopView(apps, pkgName)) {
//                    idMapView.remove(pkgName);
                    filterApps.add(pkgName);
                    Log.w("过滤", "开始过滤排行的app" + pkgName);
                }
            }
            for (String filter : filterApps) {
                idMapView.remove(filter);
            }
        }
    }

    private boolean isIntoTopView(List<AppInfo> apps, String packageName) {
        for (AppInfo app : apps) {
            if (app.getPackageName().equals(packageName))
                return true;
        }
        return false;
    }

    public boolean notifyDataSetChanged() {
        List<View> itemViewList = new ArrayList<>();
        itemViewList.add(view.findViewById(R.id.top1_view));
        itemViewList.add(view.findViewById(R.id.top2_view));
        itemViewList.add(view.findViewById(R.id.top3_view));
        int size = appList.size() > 3 ? 3 : appList.size();
        List<AppInfo> validData = appList.subList(0, size);
        if (validData == null || validData.size() == 0) {
            idMapView.clear();//应用都没了，还下载个蛋蛋
        } else {
            filterAppsOutOfTopView(validData);//先将已有的下载池里面的app并且不再排行上的过滤
        }
        for (int i = 0; i < validData.size(); i++) {
            String pkgName = validData.get(i).getPackageName();
            View view = itemViewList.get(i);
            idMapView.put(pkgName, view);
            updateItem(view, validData.get(i));
        }
        if (size < 3) {
            for (int i = size; i < itemViewList.size(); i++) {
                View view = itemViewList.get(i);
                updateItem(view, new AppInfo());
            }
        }
        return true;
    }

    public boolean notifyPackageInstalled(String pkgPrefix) {
        if (idMapView != null) {
            Set<String> keys = idMapView.keySet();
            for (Iterator<String> it = keys.iterator(); it.hasNext(); ) {
                String pkgName = it.next();
                if (pkgName.startsWith(pkgPrefix)) {
                    View view = idMapView.get(pkgName);
                    AppInfo app = (AppInfo) view.getTag();
                    app.setStatus(AppInfo.AppStatus.CAN_OPEN);
                    updateItem(view, app);
                    return true;
                }
            }
        }
        return false;
    }

    public boolean notifyPackageUnInstalled(String pkgPrefix) {
        if (idMapView != null) {
            Set<String> keys = idMapView.keySet();
            for (Iterator<String> it = keys.iterator(); it.hasNext(); ) {
                String pkgName = it.next();
                if (pkgName.startsWith(pkgPrefix)) {
                    View view = idMapView.get(pkgName);
                    AppInfo app = (AppInfo) view.getTag();
                    app.setStatus(AppInfo.AppStatus.CAN_DOWNLOAD);
                    updateItem(view, app);
                    return true;
                }
            }
        }
        return false;
    }


    private void updateItem(View view, AppInfo app) {
        String pkgName = app.getPackageName();
        ImageView iconShow = (ImageView) view.findViewById(R.id.app_icon_show);
        ImageLoader.getInstance().displayImage(app.getIconUrl(), iconShow, Config.APP_ICON_OPTIONS);
        ProgressBar progressBar = (ProgressBar) view.findViewById(R.id.app_progress);
        TextView nameShow = (TextView) view.findViewById(R.id.app_name_show);
        nameShow.setText(app.getName());
        TextView sizeShow = (TextView) view.findViewById(R.id.app_size_show);
        sizeShow.setText(FileUtils.formatSize(app.getSize()));
        RatingBar ratingShow = (RatingBar) view.findViewById(R.id.rating_bar);
        ratingShow.setRating(app.getRate());
        TextView operateBtn = (TextView) view.findViewById(R.id.operate_btn);
        operateBtn.setTag(app);
        view.setTag(app);
        if (TextUtils.isEmpty(pkgName)) {
            operateBtn.setVisibility(view.INVISIBLE);
            sizeShow.setText("");
            operateBtn.setOnClickListener(null);
            view.setOnClickListener(null);
        } else {
            view.setOnClickListener(this);
            operateBtn.setOnClickListener(this);
            operateBtn.setVisibility(View.VISIBLE);
            int progress = app.getDownloadExtra() == null ? -1 : app.getDownloadExtra().getProgress();
            AppViewHelper.updateStatus(operateBtn, progressBar, app.getStatus(), progress, AppViewHelper.BUTTON_STYLE1);
        }
    }


    @Override
    public void onClick(View v) {
        AppInfo info = (AppInfo) v.getTag();
        if (v.getId() == R.id.operate_btn) {
            ProgressBar bar = (ProgressBar) idMapView.get(info.getPackageName()).findViewById(R.id.app_progress);
            AppViewHelper.onOperate(context, info, (TextView) v, bar, AppViewHelper.BUTTON_STYLE1);
        } else {
            if (!canPerformClick()) {
                return;
            }
            Intent intent = new Intent(BroadcastAction.ACTION_OPEN_APPDETAIL);
            intent.putExtra(KeyName.APP_INFO, info);
            context.sendBroadcast(intent);
        }
    }

    public boolean isViewExits(String pkgName) {
        return idMapView == null ? false : idMapView.containsKey(pkgName);
    }

    public void updateStatus(AppInfo info) {
        if (info == null || !isViewExits(info.getPackageName())) {
            return;
        }
        int progress = info.getDownloadExtra() == null ? -1 : info.getDownloadExtra().getProgress();
        View view = idMapView.get(info.getPackageName());
        AppInfo data = (AppInfo) view.getTag();
        data.getDownloadExtra().setProgress(progress);
        data.setStatus(info.getStatus());
        TextView operateBtn = (TextView) view.findViewById(R.id.operate_btn);
        ProgressBar progressBar = (ProgressBar) view.findViewById(R.id.app_progress);
        AppViewHelper.updateStatus(operateBtn, progressBar, info.getStatus(), progress, AppViewHelper.BUTTON_STYLE1);
    }

    /**
     * 暂停所有下载中的任务
     */
    public void notifyAllPaused() {
        if (idMapView != null) {
            for (Iterator<String> it = idMapView.keySet().iterator(); it.hasNext(); ) {
                String id = it.next();
                View view = idMapView.get(id);
                AppInfo data = (AppInfo) view.getTag();
                if (data.getStatus().equals(AppInfo.AppStatus.DOWNLOADING)
                        || data.getStatus().equals(AppInfo.AppStatus.WAIT_DOWNLOAD)) {
                    TextView operateBtn = (TextView) view.findViewById(R.id.operate_btn);
                    data.setStatus(AppInfo.AppStatus.PAUSE_DOWNLOAD);
                    operateBtn.setText(data.getStatus().getText());
                }
            }
        }
    }


    public View getView() {
        return view;
    }


    private boolean canPerformClick() {
        long now = System.currentTimeMillis();
        if (now - lastClick >= 1000) {
            lastClick = now;
            return true;
        }
        return false;
    }

    public List<AppInfo> getData() {
        return appList;
    }
}
