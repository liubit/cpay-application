package com.centerm.cpay.applicationshop.net.request;

import android.content.Context;

import com.centerm.cpay.applicationshop.net.BaseRequest;
import com.centerm.cpay.applicationshop.net.cont.JsonKey;
import com.centerm.cpay.applicationshop.net.cont.Method;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * author: linwanliang</br>
 * date:2016/7/10</br>
 */
public class GetAppDetailReq extends BaseRequest {
    public GetAppDetailReq(Context context, String pkgName) {
        super(context);
        setMethod(Method.GET_APP_DETAIL);
        JSONObject body = new JSONObject();
        try {
            body.put(JsonKey.appCode, pkgName);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        addBody(body);
    }
}
