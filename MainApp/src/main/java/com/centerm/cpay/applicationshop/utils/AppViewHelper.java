package com.centerm.cpay.applicationshop.utils;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.centerm.cpay.applicationshop.R;
import com.centerm.cpay.applicationshop.bean.AppInfo;
import com.centerm.cpay.applicationshop.download.ApkDownloadManager;

import java.io.File;

/**
 * author: linwanliang</br>
 * date:2016/7/7</br>
 */
public class AppViewHelper {

    public final static int BUTTON_STYLE1 = 1;
    public final static int BUTTON_STYLE2 = 2;

    public static void onOperate(final Context context, final AppInfo data, TextView operateBtn, ProgressBar progressBar, int btnStyle) {
        AppInfo.AppStatus status = data.getStatus();
//        operateBtn.setText("1111");
        switch (status) {
            case CAN_OPEN:
                com.centerm.cloudsys.sdk.common.utils.PackageUtils.openApp(context, data.getPackageName());
                break;
            case WAIT_DOWNLOAD:
            case DOWNLOADING:
//                Log.i("yvone", "暂停");
                data.setStatus(AppInfo.AppStatus.PAUSE_DOWNLOAD);
                updateStatus(operateBtn, progressBar, AppInfo.AppStatus.PAUSE_DOWNLOAD, -1, btnStyle);
                ApkDownloadManager.getInstance().pause(context, data);
                break;
            case DOWNLOAD_FAILED:
            case CAN_DOWNLOAD:
            case CAN_UPDATE:
            case PAUSE_DOWNLOAD:
//                Log.i("yvone", "继续");
                data.setStatus(AppInfo.AppStatus.WAIT_DOWNLOAD);
                updateStatus(operateBtn, progressBar, AppInfo.AppStatus.WAIT_DOWNLOAD, -1, btnStyle);
//                new Handler().postDelayed(new Runnable() {
//                    @Override
//                    public void run() {
                ApkDownloadManager.getInstance().start(context, data);
//                    }
//                }, 1000);
                break;
            case WAIT_INSTALL:
                PackageUtils.installApp(context, FileUtils.getApkFilePath(data.getPackageUrl()));
                /*Intent intent = new Intent();
                intent.setAction(BroadcastAction.ACTION_INSTALL_APP);
                context.sendBroadcast(intent);*/
                break;
            case INSTALLING:
                break;
        }
    }


    public static void updateStatus(TextView operateBtn, ProgressBar progressBar, AppInfo.AppStatus status, int progress, int btnStyle) {
        if (operateBtn == null || progressBar == null) {
            return;
        }
        int btnEnabled = R.drawable.selector_btn_bg;
        int btnDisabled = R.drawable.btn_bg_disable;
        int btnTextEnabled = R.color.colorPrimary;
        int btnTextDisabled = R.color.colorText4;
        if (btnStyle == BUTTON_STYLE2) {
            btnEnabled = R.drawable.selector_btn_solid_blue;
            btnDisabled = R.drawable.btn_bg_gray;
            btnTextEnabled = btnTextDisabled = R.color.colorTextPrimary;
        }
        Context context = operateBtn.getContext();
        switch (status) {
            case WAIT_DOWNLOAD:
                operateBtn.setBackgroundResource(btnDisabled);
                operateBtn.setTextColor(context.getResources().getColor(btnTextDisabled));
                operateBtn.setEnabled(false);
                operateBtn.setText(R.string.label_wait);
                progressBar.setVisibility(View.INVISIBLE);
//                progressBar.setVisibility(View.VISIBLE);
//                progressBar.setIndeterminate(true);
                break;
            case CAN_DOWNLOAD:
                operateBtn.setBackgroundResource(btnEnabled);
                operateBtn.setTextColor(context.getResources().getColor(btnTextEnabled));
                operateBtn.setEnabled(true);
                operateBtn.setText(R.string.label_download);
                progressBar.setVisibility(View.INVISIBLE);
                break;
            case DOWNLOADING:
//                operateBtn.setText("正在下载");
                operateBtn.setText(R.string.label_pause);
                progressBar.setVisibility(View.VISIBLE);
                operateBtn.setBackgroundResource(btnEnabled);
                operateBtn.setEnabled(true);
                operateBtn.setTextColor(context.getResources().getColor(btnTextEnabled));
                progressBar.setProgress(progress);
                progressBar.setIndeterminate(false);
                break;
            case PAUSE_DOWNLOAD:
                operateBtn.setTextColor(context.getResources().getColor(btnTextEnabled));
                operateBtn.setBackgroundResource(btnEnabled);
                operateBtn.setEnabled(true);
//                operateBtn.setText("暂停");
                operateBtn.setText(R.string.label_resume);
                progressBar.setVisibility(View.VISIBLE);
                progressBar.setIndeterminate(false);
                break;
            case WAIT_INSTALL:
                operateBtn.setTextColor(context.getResources().getColor(btnTextEnabled));
                operateBtn.setBackgroundResource(btnEnabled);
                operateBtn.setEnabled(true);
                operateBtn.setText(R.string.label_install);
                progressBar.setProgress(0);
                progressBar.setVisibility(View.INVISIBLE);
                break;
            case INSTALLING:
                operateBtn.setText(R.string.label_installing);
                operateBtn.setBackgroundResource(btnDisabled);
                operateBtn.setTextColor(context.getResources().getColor(btnTextDisabled));
                operateBtn.setEnabled(false);
                progressBar.setVisibility(View.INVISIBLE);

//                progressBar.setProgress(100);
//                progressBar.setVisibility(View.VISIBLE);
//                progressBar.setIndeterminate(true);
                break;
            case CAN_UPDATE:
                operateBtn.setText(R.string.label_update);
                progressBar.setProgress(0);
                progressBar.setVisibility(View.INVISIBLE);
                break;
            case CAN_OPEN:
                operateBtn.setBackgroundResource(btnEnabled);
                operateBtn.setTextColor(context.getResources().getColor(btnTextEnabled));
                operateBtn.setText(R.string.label_open);
                operateBtn.setEnabled(true);
                progressBar.setIndeterminate(false);
                progressBar.setProgress(0);
                progressBar.setVisibility(View.INVISIBLE);
                break;
            case DOWNLOAD_FAILED:
                operateBtn.setText(R.string.label_download_failed);
                operateBtn.setEnabled(true);
                operateBtn.setBackgroundResource(btnEnabled);
                operateBtn.setTextColor(context.getResources().getColor(btnTextEnabled));
                progressBar.setVisibility(View.INVISIBLE);
                break;
        }
    }

    public static void updateSpeedShow(final AppInfo app,
                                       final TextView speedShow) {
        if (app == null || speedShow == null) {
            return;
        }
        switch (app.getStatus()) {
            case WAIT_DOWNLOAD:
                speedShow.setText("0B/s");
                break;
            case DOWNLOADING:
                speedShow.setText(app.getDownloadExtra().getSpeed());
                break;
            case PAUSE_DOWNLOAD:
                speedShow.setText(R.string.label2_pause);
                break;
            case INSTALLING:
                speedShow.setText(R.string.label2_installing);
                break;
            case WAIT_INSTALL:
                speedShow.setText(R.string.label_download_complete);
                break;
            default:
                speedShow.setText("");
                break;
        }

    }

}
