package com.centerm.cpay.applicationshop.asyncTask;

import com.centerm.cpay.applicationshop.bean.AppInfo;

import java.util.List;

/**
 * Created by 王玮 on 2016/8/5.
 */
public interface AppMatchListener {
    public void onStart();

    public void onResult(List<AppInfo> apps);
}
