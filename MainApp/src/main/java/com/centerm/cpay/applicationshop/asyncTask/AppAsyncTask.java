package com.centerm.cpay.applicationshop.asyncTask;

import android.content.Context;
import android.os.AsyncTask;

import com.centerm.cpay.applicationshop.bean.AppInfo;
import com.centerm.cpay.applicationshop.download.AppInfoSync;

import org.json.JSONArray;
import org.json.JSONException;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by 王玮 on 2016/8/5.
 */
public class AppAsyncTask extends AsyncTask<Object, Integer, List<AppInfo>> {
    private List<AppInfo> appList;
    AppMatchListener listener;
    Context context;

    public void setAppMatchLisenter(Context context, AppMatchListener listener) {
        this.context = context;
        this.listener = listener;
    }

    @Override
    protected List<AppInfo> doInBackground(Object... params) {
        JSONArray appsJson = (JSONArray) params[0];
        appList = new ArrayList<>();
        for (int i = 0; i < appsJson.length(); i++) {
            try {
                AppInfo item = AppInfo.convert(appsJson.getJSONObject(i));
                AppInfoSync.sync(context, item);
                appList.add(item);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        return appList;
    }

    @Override
    protected void onPostExecute(List<AppInfo> appInfos) {
        listener.onResult(appList);
    }
}
