package com.centerm.cpay.applicationshop.db;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;

import com.centerm.cpay.applicationshop.bean.Advertisement;
import com.centerm.cpay.applicationshop.bean.AppInfo;
import com.centerm.cpay.applicationshop.bean.AppType;
import com.centerm.cpay.applicationshop.bean.CacheData;
import com.centerm.cloudsys.sdk.common.log.Log4d;
import com.j256.ormlite.android.apptools.OrmLiteSqliteOpenHelper;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.support.ConnectionSource;
import com.j256.ormlite.table.TableUtils;

import java.sql.SQLException;

import config.Config;

/**
 * Created by linwanliang on 2016/6/21.
 * 数据库帮助类，基于OrmLite建立的
 */
public class DbHelper extends OrmLiteSqliteOpenHelper {
    private final static Log4d LOGGER = Log4d.getDefaultInstance();
    private final static String TAG = DbHelper.class.getSimpleName();


    private Dao<AppInfo, String> appDao;
    private Dao<Advertisement, Integer> adDao;
    //    private Dao<AppType, String> appTypeDao;
    private Dao<CacheData, String> netCacheDao;

    public DbHelper(Context context) {
        super(context, Config.DB_NAME, null, Config.DB_VERSION);
    }


    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase, ConnectionSource connectionSource) {
        try {
            TableUtils.createTableIfNotExists(connectionSource, AppInfo.class);
//            TableUtils.createTableIfNotExists(connectionSource, Advertisement.class);
//            TableUtils.createTableIfNotExists(connectionSource, AppType.class);
//            TableUtils.createTableIfNotExists(connectionSource, CacheData.class);
            LOGGER.info(TAG, "创建数据库成功");
        } catch (SQLException e) {
            e.printStackTrace();
            LOGGER.error(TAG, "创建数据库失败");
        }
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, ConnectionSource connectionSource, int i, int i1) {
        try {
            TableUtils.dropTable(connectionSource, AppInfo.class, true);
//            TableUtils.dropTable(connectionSource, Advertisement.class, true);
//            TableUtils.dropTable(connectionSource, AppType.class, true);
//            TableUtils.dropTable(connectionSource, CacheData.class, true);
            onCreate(sqLiteDatabase, connectionSource);
            LOGGER.info(TAG, "升级数据库成功");
        } catch (SQLException e) {
            e.printStackTrace();
            LOGGER.error(TAG, "升级数据库失败");
        }
    }

    /**
     * 获取下载应用列表的数据库操作对象
     *
     * @return
     */
    public synchronized Dao<AppInfo, String> getAppDao() {
        if (appDao == null) {
            try {
                appDao = getDao(AppInfo.class);
            } catch (SQLException e) {
                LOGGER.warn(TAG, "获取下载列表数据库操作对象失败");
            }
        }
        return appDao;
    }

    /**
     * 获取广告列表的数据库操作对象
     *
     * @return
     */
    public synchronized Dao<Advertisement, Integer> getAdDao() {
        if (adDao == null) {
            try {
                adDao = getDao(Advertisement.class);
            } catch (SQLException e) {
                LOGGER.warn(TAG, "获取广告数据库操作对象失败");
            }
        }
        return adDao;
    }

   /* public synchronized Dao<AppType, String> getAppTypeDao() {
        if (appTypeDao == null) {
            try {
                appTypeDao = getDao(AppType.class);
            } catch (SQLException e) {
                LOGGER.warn(TAG, "获取应用类型数据库操作对象失败");
            }
        }
        return appTypeDao;
    }*/

    public synchronized Dao<CacheData, String> getNetCacheDao() {
        if (netCacheDao == null) {
            try {
                netCacheDao = getDao(CacheData.class);
            } catch (SQLException e) {
                LOGGER.warn(TAG, "获取网络缓存数据库操作对象失败");
            }
        }
        return netCacheDao;
    }

}
