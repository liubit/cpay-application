package com.centerm.cpay.applicationshop.utils;

import org.json.JSONObject;

/**
 * author: linwanliang</br>
 * date:2016/6/29</br>
 */
public class JsonUtils {
    public static String getString(JSONObject json, String key, String replace) {
        String value = replace;
        try {
            value = json.getString(key);
        } catch (Exception e) {
//            e.printStackTrace();
        }
        return value;
    }

    public static String getString(JSONObject json, String key) {
        return JsonUtils.getString(json, key, "");
    }

    public static long getLong(JSONObject json, String key) {
        String value = getString(json, key);
        long lValue = 0;
        try {
            lValue = Long.valueOf(value);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return lValue;
    }

    public static int getInt(JSONObject json, String key) {
        String value = getString(json, key);
        int lValue = 0;
        try {
            lValue = Integer.valueOf(value);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return lValue;
    }

    public static float getFloat(JSONObject json, String key) {
        String value = getString(json, key);
        float lValue = 0;
        try {
            lValue = Float.valueOf(value);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return lValue;
    }

}
