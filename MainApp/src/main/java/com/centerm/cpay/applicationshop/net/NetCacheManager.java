package com.centerm.cpay.applicationshop.net;

import android.content.Context;

import com.centerm.cpay.applicationshop.utils.FileUtils;
import com.centerm.cloudsys.sdk.common.log.Log4d;
import com.centerm.cloudsys.sdk.common.utils.MD5Utils;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

import config.Config;

/**
 * author: linwanliang</br>
 * date:2016/7/8</br>
 */
public class NetCacheManager {
    private static NetCacheManager instance;
    private long validity = Config.NET.DEFAULT_CACHE_VALIDITY;//-1代表不过期
    private Log4d logger = Log4d.getDefaultInstance();
    private String tag = NetCacheManager.class.getSimpleName();

    private NetCacheManager() {
    }

    public static NetCacheManager getInstance() {
        if (instance == null) {
            instance = new NetCacheManager();
        }
        return instance;
    }

    public void write(Context context, String tag, String content) {
        long now = System.currentTimeMillis();
        String dir = getCacheDir(context);
        File dirFile = new File(dir);
        if (!dirFile.exists()) {
            dirFile.mkdirs();
        }
        String fileName = getFileName(tag);
        File file = new File(dir + File.separator + fileName);
        FileWriter writer = null;
        BufferedWriter bufW = null;
        try {
            writer = new FileWriter(file);
            bufW = new BufferedWriter(writer);
            bufW.write("time:" + now);
            bufW.newLine();
            bufW.write(content);
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (bufW != null) {
                try {
                    bufW.close();
                } catch (IOException e) {
                    e.printStackTrace();
                } finally {
                    if (writer != null) {
                        try {
                            writer.close();
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                }
            }
        }
    }

    public String read(Context context, String tag) {
        String dir = getCacheDir(context);
        String fileName = getFileName(tag);
        boolean deleteFlag = false;
        File file = new File(dir + File.separator + fileName);
        if (!file.exists()) {
            return null;
        }
        FileReader reader = null;
        BufferedReader bufR = null;
        StringBuilder sBuilder = new StringBuilder();
        try {
            reader = new FileReader(file);
            bufR = new BufferedReader(reader);
            String line = null;
            while ((line = bufR.readLine()) != null) {
                if (line.startsWith("time:")) {
                    //检查时间戳
                    long time = -1;
                    time = Long.valueOf(line.replace("time:", ""));
                    long now = System.currentTimeMillis();
                    if (validity != -1 && (now - time) > validity) {
                        logger.warn(tag, "缓存文件已过期，删除缓存");
                        //删除标识
                        deleteFlag = true;
                        sBuilder = null;
                        break;
                    }
                    continue;
                }
                sBuilder.append(line);
            }
            return sBuilder.toString();
        } catch (Exception e) {
            e.printStackTrace();
            sBuilder = null;
        } finally {
            if (bufR != null) {
                try {
                    bufR.close();
                } catch (IOException e) {
                    e.printStackTrace();
                } finally {
                    if (reader != null) {
                        try {
                            reader.close();
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                }
            }
            if (deleteFlag && file.exists()) {
                file.delete();
            }
        }
        return sBuilder == null ? null : sBuilder.toString();
    }


    private String getFileName(String tag) {
        return MD5Utils.getMD5Str(tag);
    }

    public String getCacheDir(Context context) {
        return FileUtils.getCacheDir(context) + File.separator + "net";
    }

    public long getValidity() {
        return validity;
    }

    public void setValidity(long validity) {
        this.validity = validity;
    }
}
