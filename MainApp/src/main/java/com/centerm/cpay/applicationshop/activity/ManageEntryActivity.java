package com.centerm.cpay.applicationshop.activity;

import android.content.Intent;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.centerm.cpay.applicationshop.R;
import com.centerm.cpay.applicationshop.base.BaseActivity_v21;
import com.centerm.cpay.applicationshop.common.listener.OnItemClick;
import com.centerm.cpay.applicationshop.common.listener.OnItemLongClick;
import com.centerm.cpay.applicationshop.view.DividerItemDecoration;

/**
 * Created by linwanliang on 2016/6/22.
 */
public class ManageEntryActivity extends BaseActivity_v21 {
    private final static int[] ICONS = new int[]{R.drawable.download, R.drawable.del, R.drawable.setting};
    private String[] ITEM_NAMES = new String[]{"应用下载11", "应用卸载", "设置"};

    @Override
    public int onInitLayoutId() {
        return R.layout.activity_manage_entry;
    }

    @Override
    public void onInitLocalData() {

    }

    @Override
    public void onInitView(View view) {
        ITEM_NAMES = getResources().getStringArray(R.array.managment_entries);
        RecyclerView recyclerView = (RecyclerView) findViewById(R.id.recycle_v);
        initRecyclerView(recyclerView);

        initUsualToolbar(R.string.title_managment);
    }

    private void initRecyclerView(RecyclerView view) {
        view.setLayoutManager(new LinearLayoutManager(context));
        view.setItemAnimator(new DefaultItemAnimator());
        ItemShowAdapter adapter = new ItemShowAdapter();
        adapter.setItemClickListener(new OnItemClick() {
            @Override
            public void onClick(View itemView, int position) {
//                Toast.makeText(context,""+position,Toast.LENGTH_SHORT).show();
                Intent intent = null;
                switch (position) {
                    case 0:
                        intent = new Intent(context, DownloadManageActivity.class);
                        break;
                    case 1:
                        intent = new Intent(context, AppManageActivity.class);
                        break;
                    case 2:
                        intent = new Intent(context, SetupActivity.class);
                        break;
                }
                if (intent != null) {
                    jumpTo(intent);
                }
            }
        });
        view.setAdapter(adapter);
        view.addItemDecoration(new DividerItemDecoration(context, DividerItemDecoration.VERTICAL_LIST));
    }

    private class ItemShowAdapter extends RecyclerView.Adapter<ItemShowAdapter.ItemViewHolder> {

        private OnItemClick itemClick;
        private OnItemLongClick itemLongClick;


        @Override
        public ItemShowAdapter.ItemViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View view = LayoutInflater.from(context).inflate(R.layout.manage_entry_item, parent, false);
            ItemViewHolder viewHolder = new ItemViewHolder(view);
            return viewHolder;
        }

        @Override
        public void onBindViewHolder(ItemShowAdapter.ItemViewHolder holder, int position) {
            holder.iconShow.setImageResource(ICONS[position]);
            holder.nameShow.setText(ITEM_NAMES[position]);
            holder.position = position;
        }

        @Override
        public int getItemCount() {
            return ITEM_NAMES.length;
        }

        public void setItemClickListener(OnItemClick listener) {
            this.itemClick = listener;
        }

        public void setItemLongClick(OnItemLongClick listener) {
            this.itemLongClick = itemLongClick;
        }

        class ItemViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener, View.OnLongClickListener {

            int position;
            ImageView iconShow;
            TextView nameShow;

            public ItemViewHolder(View itemView) {
                super(itemView);
                iconShow = (ImageView) itemView.findViewById(R.id.item_icon_show);
                nameShow = (TextView) itemView.findViewById(R.id.item_name_show);
                itemView.setOnClickListener(this);
                itemView.setOnLongClickListener(this);
            }


            @Override
            public void onClick(View v) {
                if (itemClick != null) {
                    itemClick.onClick(v, position);
                }
            }

            @Override
            public boolean onLongClick(View v) {
                if (itemLongClick != null) {
                    itemLongClick.onLongClick(v, position);
                    return true;
                }
                return false;
            }
        }
    }

}
