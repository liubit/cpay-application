package com.centerm.cpay.applicationshop.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import java.util.List;

/**
 * author: linwanliang</br>
 * date:2016/7/4</br>
 */
public class MainPagerAdapter extends FragmentPagerAdapter {
    private List<Fragment> fragList;

    public MainPagerAdapter(FragmentManager fm, List<Fragment> fragList) {
        super(fm);
        this.fragList = fragList;
    }

    @Override
    public Fragment getItem(int position) {
        return fragList == null ? null : fragList.get(position);
    }

    @Override
    public int getCount() {
        return fragList == null ? 0 : fragList.size();
    }
}
