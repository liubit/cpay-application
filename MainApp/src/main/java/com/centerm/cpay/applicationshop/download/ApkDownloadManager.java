package com.centerm.cpay.applicationshop.download;

import java.io.File;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.http.Header;
import org.xutils.common.Callback;
import org.xutils.http.RequestParams;
import org.xutils.x;

import android.content.Context;
import android.content.Intent;
import android.os.Handler;
import android.os.Message;
import android.text.TextUtils;

import com.centerm.cpay.applicationshop.R;
import com.centerm.cpay.applicationshop.bean.AppInfo;
import com.centerm.cpay.applicationshop.common.BroadcastAction;
import com.centerm.cpay.applicationshop.common.KeyName;
import com.centerm.cpay.applicationshop.db.DbHelper;
import com.centerm.cpay.applicationshop.download.xutils.ProgressCallBack;
import com.centerm.cpay.applicationshop.utils.CommonUtils;
import com.centerm.cpay.applicationshop.utils.FileUtils;
import com.centerm.cpay.applicationshop.utils.PackageUtils;
import com.centerm.cpay.applicationshop.utils.ViewUtils;
import com.centerm.cloudsys.sdk.common.log.Log4d;
import com.j256.ormlite.android.apptools.OpenHelperManager;
import com.j256.ormlite.dao.Dao;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.FileAsyncHttpResponseHandler;

import config.Config;

/**
 * 基于AsyncHttpClient的文件下载管理器。
 * 这里维护着应用下载列表当中各个应用的状态，包含内存数据和数据库。
 */
public class ApkDownloadManager {
    private final static int MSG_DOWNLOAD_SUCCESS = 0X765;
    private final static int MSG_FILE_CHECK_FAILED = 0X766;


    private Context context;
    private final static Log4d logger = Log4d.getDefaultInstance();
    private final static String TAG = ApkDownloadManager.class.getSimpleName();
    private final static int DELAY_DOWNLOAD_TIME = 3 * 1000;
    private final static int DEFAULT_MAX_CONNECTIONS = 6;// 同时下载的文件个数
    private final static int DEFAULT_RESPONSE_TIMEOUT = 10 * 60 * 1000;// 超时时间10分钟
    private final static long PROGRESS_SEND_INTERVAL = 1000;// 进度发送时间间隔，频繁发送进度值引起频繁更新UI，会影响界面的流畅性

    private static long sendControlTime = 0;//用来控制所有下载任务进度发送的时间值，标识上次发送进度值的时间点
    private static ApkDownloadManager client;
    private AsyncHttpClient innerClient;
    private int responseTimeout = DEFAULT_RESPONSE_TIMEOUT;
    private boolean isFinish;
    private int maxTasks = DEFAULT_MAX_CONNECTIONS;
    private LinkedHashMap<String, AppInfo> idMapTask = new LinkedHashMap<>();//当前下载任务池，从开始下载到完成安装的状态.
    //下载队列线程管理

    private Handler handler;
    private Map<String, Callback.Cancelable> downloadTools;

    private ApkDownloadManager() {
//        innerClient = new AsyncHttpClient();
        innerClient = new AsyncHttpClient(true, maxTasks);
        // 设置连接超时时间（下载时间）
        innerClient.setConnectTimeout(responseTimeout);
        // 设置连接响应时间
        innerClient.setResponseTimeout(responseTimeout);
        innerClient.setMaxRetriesAndTimeout(0, responseTimeout);
        downloadTools = new HashMap<>();
    }

    public static ApkDownloadManager getInstance() {
        if (client == null) {
            synchronized (ApkDownloadManager.class) {
                if (client == null) {
                    client = new ApkDownloadManager();
                }
            }
        }
        client.isFinish = false;
        return client;
    }

    /**
     * 更新所有的数据模型
     *
     * @param context
     * @param app
     */
    private void updateDataModel(Context context, AppInfo app) {
        //更新任务池
        AppInfo.AppStatus status = app.getStatus();
        switch (status) {
            case CAN_OPEN:
            case CAN_DOWNLOAD:
                remove(app, true);
                break;
            default:
                push(app);
                break;
        }
        //更新数据库
        refreshDb(context, app);
    }

    public boolean start(final Context context, final AppInfo appInfo) {
        final Context appContext = context.getApplicationContext();
        final String packageName = appInfo.getPackageName();
        final String name = appInfo.getName();
        String url = appInfo.getPackageUrl();
        logger.warn(TAG, "【下载地址：】" + url);
        long sdFreeSize = FileUtils.getFreeDiskSpace();
        if (context == null || url == null || url.trim().isEmpty()) {
            logDownloadWarnning(name, "参数为空！");
            return false;
        }
        if (sdFreeSize == -1) {
            logDownloadWarnning(name, "没有挂载SD卡！");
            return false;
        } else if (sdFreeSize < Config.FILE_DOWNLOADER.MIN_FREE_SIZE) {
            logDownloadWarnning(name, "SD卡空间不足！");
            return false;
        }
        String fileName = DownloadTools.extractFileNameFromUrl(url);
        if (fileName == null) {
            logDownloadWarnning(name, "解析文件名失败！");
            return false;
        }
        initHandler(context);
        File file = DownloadTools.getApkFile(appInfo);
        if (file.exists()) {
            logDownloadWarnning(TAG, "准备下载的文件已存在，可直接安装");
            appInfo.setStatus(AppInfo.AppStatus.WAIT_INSTALL);
            updateDataModel(context, appInfo);
            //发送广播
            context.sendBroadcast(createIntent(BroadcastAction.DOWNLOAD.ACTION_SUCCESS, appInfo));
            return false;
        }
        final RequestParams requestParams = new RequestParams(url);
        requestParams.setAutoResume(true);
        requestParams.setCancelFast(true);
        requestParams.setLoadingUpdateMaxTimeSpan(1000);
        requestParams.setSaveFilePath(file.getAbsolutePath());
        final Callback.Cancelable cancelable = x.http().get(requestParams, new ProgressCallBack() {
            long lastSize = 0;//上一次报告进度时，文件的大小
            long lastTime = 0;//上一次报告进度的时间

            @Override
            public void onStarted() {
                logger.debug(TAG, name + "，加入下载队列");
                appInfo.setStatus(AppInfo.AppStatus.WAIT_DOWNLOAD);
                updateDataModel(context, appInfo);
                appContext.sendBroadcast(createIntent(BroadcastAction.DOWNLOAD.ACTION_START, appInfo));
            }

            @Override
            public void onLoading(long total, long current, boolean isDownloading) {
                long now = System.currentTimeMillis();
//                if (now - lastTime < 1000) {
//                    return;
//                }
                String speed;
                long actualInterval = lastTime == 0 ? PROGRESS_SEND_INTERVAL : (now - lastTime);
                if (lastSize == 0) {
                    speed = FileUtils.formatSize((long) (current / (actualInterval / 1000.00))) + "/s";
                } else {
                    speed = FileUtils
                            .formatSize((long) ((current - lastSize) / (actualInterval / 1000.00)))
                            + "/s";
                }
                int progress = (int) (current * 100 / total);
                appInfo.getDownloadExtra().setProgress(progress);
                appInfo.getDownloadExtra().setSpeed(speed);
                if (!appInfo.getStatus().equals(AppInfo.AppStatus.DOWNLOADING)) {
                    appInfo.setStatus(AppInfo.AppStatus.DOWNLOADING);
                    updateDataModel(context, appInfo);
                } else {
                    //性能考虑，这里只更新任务池的数据，不更新数据库
                    push(appInfo);
                }
                appContext.sendBroadcast(createIntent(BroadcastAction.DOWNLOAD.ACTION_PROGRESS, appInfo));
                lastSize = current;
                lastTime = now;
                sendControlTime = now;
            }

            @Override
            public void onProgress(int progress, boolean isDownloading) {

            }

            @Override
            public void onSuccess(final File file) {
                logger.debug(TAG, name + "，下载成功");
                downloadTools.remove(packageName);
                final CommonUtils.FileMd5Callback md5Callback = new CommonUtils.FileMd5Callback() {
                    @Override
                    public void gotMd5(String value) {
                        boolean checkResult = false;
                        String md5 = appInfo.getFileMd5();
                        logger.debug(TAG, "【" + name + "】下载完成，本地MD5：" + value + "，原MD5：" + md5);
                        if (!TextUtils.isEmpty(value) && (value.equals(md5) || value.toUpperCase().equals(md5) || value.toLowerCase().equals(md5))) {
                            checkResult = true;
                        }
                        Message msg = new Message();
                        msg.obj = appInfo;
                        if (checkResult) {
                            //更新应用状态
                            appInfo.setStatus(AppInfo.AppStatus.WAIT_INSTALL);
                            updateDataModel(context, appInfo);
                            if (handler != null) {
                                handler.postDelayed(new Runnable() {
                                    @Override
                                    public void run() {
                                        //再次发送广播
                                        appContext.sendBroadcast(createIntent(BroadcastAction.DOWNLOAD.ACTION_SUCCESS, appInfo));
                                    }
                                }, 2000);
                            }
                            //发送广播
                            appContext.sendBroadcast(createIntent(BroadcastAction.DOWNLOAD.ACTION_SUCCESS, appInfo));
                            msg.what = MSG_DOWNLOAD_SUCCESS;
                        } else {
                            logger.warn(TAG, "【" + name + "】文件MD5校验失败");
                            deleteFile(name, file);
                            //停止下载应用，并删除文件
                            //更新应用状态
                            appInfo.setStatus(AppInfo.AppStatus.DOWNLOAD_FAILED);
                            updateDataModel(context, appInfo);
                            //发送广播
                            appContext.sendBroadcast(createIntent(BroadcastAction.DOWNLOAD.ACTION_FAILED, appInfo));
                            msg.what = MSG_FILE_CHECK_FAILED;
                        }
                        if (handler != null) {
                            handler.sendMessage(msg);
                        }
                    }
                };
                logger.warn(TAG, "【" + name + "下载完成】" + file.getAbsolutePath());
                //这里可能需要延时一下， sd卡目录的文件可能还没准备好就拿去算md5值，会出现校验失败的情况
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        CommonUtils.getMD5(file, md5Callback);
                    }
                }, 500);
            }

            @Override
            public void onError(Throwable ex, boolean isOnCallback) {
                logger.warn(TAG, name + "，下载失败：" + ex.getMessage());
                if (ex.getMessage().equals("Bad Request")) {
                    downloadTools.put(packageName, x.http().get(requestParams, this));
                    download(context, appInfo);
                }
                ex.printStackTrace();
                //更新应用状态
                appInfo.setStatus(AppInfo.AppStatus.DOWNLOAD_FAILED);
                updateDataModel(context, appInfo);
                //发送广播
                appContext.sendBroadcast(createIntent(BroadcastAction.DOWNLOAD.ACTION_FAILED, appInfo));
            }
        });
        downloadTools.put(packageName, cancelable);
        appInfo.setStatus(AppInfo.AppStatus.WAIT_DOWNLOAD);
        updateDataModel(context, appInfo);
        return true;
    }

    private void download(Context context, AppInfo appInfo) {
        appInfo.setStatus(AppInfo.AppStatus.WAIT_DOWNLOAD);
        updateDataModel(context, appInfo);
    }

    /**
     * 开始下载
     *
     * @param context Context
     * @param appInfo 应用实体类
     * @return 成功加入下载队列返回true，否则返回false
     */
    public boolean startS(final Context context, final AppInfo appInfo) {
        final String packageName = appInfo.getPackageName();
        final String name = appInfo.getName();
        final String url = appInfo.getPackageUrl();
        final long totalSize = appInfo.getSize();

        if (context == null || url == null || url.trim().isEmpty()) {
            logDownloadWarnning(name, "参数为空！");
            return false;
        }
        initHandler(context);
        final Context appContext = context.getApplicationContext();
        long sdFreeSize = FileUtils.getFreeDiskSpace();
        if (sdFreeSize == -1) {
            logDownloadWarnning(name, "没有挂载SD卡！");
            return false;
        } else if (sdFreeSize < Config.FILE_DOWNLOADER.MIN_FREE_SIZE) {
            logDownloadWarnning(name, "SD卡空间不足！");
            return false;
        }
        String fileName = DownloadTools.extractFileNameFromUrl(url);
        if (fileName == null) {
            logDownloadWarnning(name, "解析文件名失败！");
            return false;
        }
        // 不支持单文件多线程下载
        innerClient.cancelRequestsByTAG(url, true);
        final File file = DownloadTools.getApkFile(appInfo);
        long fileLength = 0;
        if (file.exists()) {
            // 如果文件存在，获取已存在的文件的大小
            fileLength = file.length();
        }
        if (fileLength > totalSize) {
            logDownloadWarnning(name, "已存在文件大小异常，删除文件并重新开始下载");
            stop(context, appInfo);
            if (file.exists()) {
                file.delete();
            }
            if (!isFinish) {
                start(context, appInfo);
            }
            return true;
        } else if (fileLength == totalSize) {
            logDownloadWarnning(TAG, "准备下载的文件已存在，可直接安装");
            appInfo.setStatus(AppInfo.AppStatus.WAIT_INSTALL);
            updateDataModel(context, appInfo);
            //发送广播
            context.sendBroadcast(createIntent(BroadcastAction.DOWNLOAD.ACTION_SUCCESS, appInfo));
            return false;
        }
        final long startLength = fileLength;//断点续传开始点
        final long copyTotalSize = totalSize;//总大小
        FileAsyncHttpResponseHandler innerHandler = new FileAsyncHttpResponseHandler(file, true) {
            long lastSize = 0;//上一次报告进度时，文件的大小
            long currentSize = startLength;//当前文件的大小
            long lastTime = 0;//上一次报告进度的时间

            @Override
            public void onCancel() {
                logger.debug(TAG, packageName + "，下载暂停或取消");
                //因为这边不确定是暂停还是取消下载
                //故这里不发送广播给外部
            }

            @Override
            public void onStart() {
                //该回调方法代表任务已加入下载队列，并不意味着开始下载
                logger.debug(TAG, name + "，加入下载队列");
                appInfo.setStatus(AppInfo.AppStatus.WAIT_DOWNLOAD);
                updateDataModel(context, appInfo);
                appContext.sendBroadcast(createIntent(BroadcastAction.DOWNLOAD.ACTION_START, appInfo));
            }

            @Override
            public void onProgress(long bytesWritten, long tSize) {
                currentSize = startLength + bytesWritten;
                //计算下载进度
                int progress = DownloadTools.getPercent(currentSize, copyTotalSize);
                if (progress == 100 || canReportProgress()) {
                    long now = System.currentTimeMillis();
                    if (progress > 100) {
                        exceptionOnDownload(null);
                        return;
                    }
                    //计算下载速度
                    String speed;
                    long actualInterval = lastTime == 0 ? PROGRESS_SEND_INTERVAL : (now - lastTime);
                    if (lastSize == 0) {
                        speed = FileUtils.formatSize((long) (bytesWritten / (actualInterval / 1000.00))) + "/s";
                    } else {
                        speed = FileUtils
                                .formatSize((long) ((currentSize - lastSize) / (actualInterval / 1000.00)))
                                + "/s";
                    }
                    appInfo.getDownloadExtra().setProgress(progress);
                    appInfo.getDownloadExtra().setSpeed(speed);
                    if (!appInfo.getStatus().equals(AppInfo.AppStatus.DOWNLOADING)) {
                        appInfo.setStatus(AppInfo.AppStatus.DOWNLOADING);
                        updateDataModel(context, appInfo);
                    } else {
                        //性能考虑，这里只更新任务池的数据，不更新数据库
                        push(appInfo);
                    }
                    appContext.sendBroadcast(createIntent(BroadcastAction.DOWNLOAD.ACTION_PROGRESS, appInfo));
                    lastSize = currentSize;
                    lastTime = now;
                    sendControlTime = now;
                    logger.debug(TAG, name + "，已下载：" + progress + "%，下载速度：" + speed);
                }
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, final File file) {
                logger.debug(TAG, name + "，下载成功");
                final CommonUtils.FileMd5Callback md5Callback = new CommonUtils.FileMd5Callback() {
                    @Override
                    public void gotMd5(String value) {
                        boolean checkResult = false;
                        String md5 = appInfo.getFileMd5();
                        logger.debug(TAG, "【" + name + "】下载完成，本地MD5：" + value + "，原MD5：" + md5);
                        if (!TextUtils.isEmpty(value) && (value.equals(md5) || value.toUpperCase().equals(md5) || value.toLowerCase().equals(md5))) {
                            checkResult = true;
                        }
                        Message msg = new Message();
                        msg.obj = appInfo;
                        if (checkResult) {
                            //更新应用状态
                            appInfo.setStatus(AppInfo.AppStatus.WAIT_INSTALL);
                            updateDataModel(context, appInfo);
                            if (handler != null) {
                                handler.postDelayed(new Runnable() {
                                    @Override
                                    public void run() {
                                        //再次发送广播
                                        appContext.sendBroadcast(createIntent(BroadcastAction.DOWNLOAD.ACTION_SUCCESS, appInfo));
                                    }
                                }, 2000);
                            }
                            //发送广播
                            appContext.sendBroadcast(createIntent(BroadcastAction.DOWNLOAD.ACTION_SUCCESS, appInfo));
                            msg.what = MSG_DOWNLOAD_SUCCESS;
                        } else {
                            logger.warn(TAG, "【" + name + "】文件MD5校验失败");
                            deleteFile(name, file);
                            //停止下载应用，并删除文件
                            //更新应用状态
                            appInfo.setStatus(AppInfo.AppStatus.DOWNLOAD_FAILED);
                            updateDataModel(context, appInfo);
                            //发送广播
                            appContext.sendBroadcast(createIntent(BroadcastAction.DOWNLOAD.ACTION_FAILED, appInfo));
                            msg.what = MSG_FILE_CHECK_FAILED;
                        }
                        if (handler != null) {
                            handler.sendMessage(msg);
                        }
                    }
                };
                logger.warn(TAG, "【" + name + "下载完成】" + file.getAbsolutePath());
                //这里可能需要延时一下， sd卡目录的文件可能还没准备好就拿去算md5值，会出现校验失败的情况
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        CommonUtils.getMD5(file, md5Callback);
                    }
                }, 500);


                //加入静默安装队列
//                            silentInstall(context, appInfo);
//                            SilentInstallManager.getInstance(context).push(appInfo);
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, File file) {
                logger.warn(TAG, name + "，下载失败，状态码：" + statusCode);
                //更新应用状态
                appInfo.setStatus(AppInfo.AppStatus.DOWNLOAD_FAILED);
                updateDataModel(context, appInfo);
                //发送广播
                appContext.sendBroadcast(createIntent(BroadcastAction.DOWNLOAD.ACTION_FAILED, appInfo));
            }

            private void exceptionOnDownload(final Exception e) {
                stop(context, appInfo);
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        file.delete();
                        onFailure(-1, null, e, file);
                    }
                }, 1000);
            }
        };
        appInfo.setStatus(AppInfo.AppStatus.WAIT_DOWNLOAD);
        updateDataModel(context, appInfo);
        innerClient.addHeader("Range", "bytes=" + startLength + "-");
        innerClient.get(context, url, innerHandler).setTag(url);
        logger.info(TAG, "[" + packageName + "]发起下载请求" + url);
        logger.info(TAG, "[" + packageName + "]断点下载起始点：" + startLength);
        return true;
    }

    private Intent createIntent(String action, AppInfo app) {
        Intent intent = new Intent();
        intent.setAction(action);
        intent.putExtra(KeyName.APP_INFO, app);
        return intent;
    }

    /**
     * 暂停下载，不会删除已下载的文件
     *
     * @param context
     * @param appInfo
     * @return
     */
    public boolean pause(final Context context, final AppInfo appInfo) {
        final String url = appInfo.getPackageUrl();
        if (url == null) {
            logger.warn(TAG, "暂停任务失败，url为空");
            return false;
        }
        logger.info(TAG, "[" + appInfo.getName() + "]暂停下载");
//        new Thread(new Runnable() {
//            @Override
//            public void run() {
        Context appContext = context.getApplicationContext();
//                innerClient.cancelRequestsByTAG(url, true);
        if (downloadTools.containsKey(appInfo.getPackageName())) {
            downloadTools.get(appInfo.getPackageName()).cancel();
        }
        //更改应用状态
        appInfo.setStatus(AppInfo.AppStatus.PAUSE_DOWNLOAD);
        updateDataModel(context, appInfo);
        //发送广播
        appContext.sendBroadcast(createIntent(BroadcastAction.DOWNLOAD.ACTION_PAUSE, appInfo));
//            }
//        }).start();
        return true;
    }

    /**
     * 停止下载，会删除已下载的文件
     *
     * @param context
     * @param appInfo
     * @return
     */
    public boolean stop(Context context, AppInfo appInfo) {
        String packageName = appInfo.getPackageName();
        String url = appInfo.getPackageUrl();
        if (url == null || context == null) {
            logger.warn(TAG, "停止任务失败，url为空");
            return false;
        }
        logger.info(TAG, "[" + appInfo.getName() + "]取消下载");
        Context appContext = context.getApplicationContext();
        if (downloadTools.containsKey(packageName)) {
            downloadTools.get(packageName).cancel();
        }
        downloadTools.remove(packageName);
//        innerClient.cancelRequestsByTAG(url, true);
        //更改应用状态
        if (PackageUtils.getInstalledVersionName(context, packageName) != null) {
            appInfo.setStatus(AppInfo.AppStatus.CAN_OPEN);
        } else {
            appInfo.setStatus(AppInfo.AppStatus.CAN_DOWNLOAD);
        }
        updateDataModel(context, appInfo);
        //发送广播
        appContext.sendBroadcast(createIntent(BroadcastAction.DOWNLOAD.ACTION_CANCELED, appInfo));
        return true;
    }


    /**
     * 异步删除指定文件
     *
     * @param name
     * @param file
     */
    private void deleteFile(final String name, final File file) {
        if (file != null && file.exists()) {
            new Thread(new Runnable() {
                @Override
                public void run() {
                    boolean result = file.delete();
                    logger.info(TAG, name + "，删除安装包结果：" + result);
                }
            }).start();
        } else {
            logger.warn(TAG, name + "，安装包不存在，删除失败");
        }
    }


    /**
     * 暂停所有的下载任务
     */
    public void pauseAll(Context context) {
        innerClient.cancelAllRequests(true);
        isFinish = true;
        Intent intent = new Intent();
        intent.setAction(BroadcastAction.DOWNLOAD.ACTION_PAUSE_ALL);
        context.sendBroadcast(intent);
    }

    /**
     * 暂停所有的下载任务
     */
    public void destroyPauseAll(final Context context) {
        innerClient.cancelAllRequests(true);
        isFinish = true;
        new Thread(new Runnable() {
            @Override
            public void run() {
                pauseAll2ModifyDbModel(context);
            }
        }).start();
    }

    private void pauseAll2ModifyDbModel(Context context) {
        DbHelper dbHelper = OpenHelperManager.getHelper(context, DbHelper.class);
        Dao<AppInfo, String> appDao = dbHelper.getAppDao();
        try {
            List<AppInfo> appList = appDao.queryForAll();
            for (AppInfo item : appList) {
                if (item.getStatus() == AppInfo.AppStatus.DOWNLOADING || item.getStatus()
                        == AppInfo.AppStatus.WAIT_DOWNLOAD) {
                    if (downloadTools.containsKey(item.getPackageName())) {
                        downloadTools.get(item.getPackageName()).cancel();
                    }
                    logger.warn(TAG, item.getName() + "【暂停下载】");
                    item.setStatus(AppInfo.AppStatus.PAUSE_DOWNLOAD);
                    updateDataModel(context, item);
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    /**
     * 检测处于下载管理器中的APP状态
     *
     * @param pkgName
     * @return
     */
    public AppInfo.AppStatus checkAppStatus(String pkgName) {
        if (idMapTask.containsKey(pkgName)) {
            return idMapTask.get(pkgName).getStatus();
        }
        return null;
    }

    /**
     * 检测处于下载管理器中的应用进度
     *
     * @param pkgName
     * @return
     */
    public int checkAppProgress(String pkgName) {
        if (idMapTask.containsKey(pkgName)) {
            return idMapTask.get(pkgName).getDownloadExtra().getProgress();
        }
        return -1;
    }

    /**
     * 应用安装成功后，需要回调该方法来更新任务池以及数据库。
     * 由于应用内会有多个Activity会去接收应用安装的广播，如果都去调用该方法，势必会造成性能浪费；
     * MainActivity在程序运行过程中，是始终存在的，因此该方法只需要在MainActivity中调用即可。
     *
     * @param pkgPrefix
     * @return
     */
    public boolean onPackageInstalled(Context context, String pkgPrefix) {
        Set<String> keys = idMapTask.keySet();
        for (Iterator<String> it = keys.iterator(); it.hasNext(); ) {
            String pkgName = it.next();
            if (pkgName.startsWith(pkgPrefix)) {
                AppInfo task = idMapTask.get(pkgName);
                //更改状态为可打开
                task.setStatus(AppInfo.AppStatus.CAN_OPEN);
                ViewUtils.showToast(context, "【" + task.getName() + "】" + context.getResources().getString(R.string.tips_install_complete));
                updateDataModel(context, task);
                return true;
            }
        }
        logger.info(TAG, "应用安装成功，准备更新任务池失败，未找到对应应用-" + pkgPrefix);
        DbHelper dbHelper = OpenHelperManager.getHelper(context, DbHelper.class);
        Dao<AppInfo, String> appDao = dbHelper.getAppDao();
        try {
            if (appDao.queryForId(pkgPrefix) != null) {
                appDao.deleteById(pkgPrefix);
            } else {
                List<AppInfo> appList = appDao.queryForAll();
                for (AppInfo item : appList) {
                    if (item.getPackageName().startsWith(pkgPrefix)) {
                        appDao.delete(item);
                        return true;
                    }
                }
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }
        return false;
    }

    public boolean hasTask() {
        return idMapTask.size() > 0 ? true : false;
    }

    /**
     * 更新APP状态池，该方法仅供静默安装管理器调用{@link SilentInstallManager}
     *
     * @param pkgName
     * @param status
     * @param progress
     */
    void updateAppStatusPool(String pkgName, AppInfo.AppStatus status, int progress) {
        if (idMapTask.containsKey(pkgName)) {
            AppInfo task = idMapTask.get(pkgName);
            task.setStatus(status);
            task.getDownloadExtra().setProgress(progress);
        } else {
            logger.warn(TAG, "更新任务池失败，未找到该应用-" + pkgName);
        }
    }

    /**
     * 判断是否可以报告进度值
     *
     * @return
     */
    private boolean canReportProgress() {
        long now = System.currentTimeMillis();
        return (now - sendControlTime) >= PROGRESS_SEND_INTERVAL;
    }


    /**
     * 加入数据库下载队列中
     *
     * @param context Context
     * @param app     应用信息
     */
    private void refreshDb(Context context, AppInfo app) {
        DbHelper dbHelper = OpenHelperManager.getHelper(context, DbHelper.class);
        Dao<AppInfo, String> appDao = dbHelper.getAppDao();
        try {
            if (app.getStatus().equals(AppInfo.AppStatus.CAN_OPEN)
                    || app.getStatus().equals(AppInfo.AppStatus.CAN_DOWNLOAD)) {
                appDao.deleteById(app.getPackageName());
            } else {
                appDao.createOrUpdate(app);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    /**
     * 入任务池
     *
     * @param task
     */
    private void push(AppInfo task) {
        if (task != null) {
            String pkgName = task.getPackageName();
            idMapTask.put(pkgName, task);
        }
    }

    /**
     * 出任务池
     *
     * @param task
     * @param deleteApk
     */
    private void remove(AppInfo task, boolean deleteApk) {
        if (task != null) {
            String pkgName = task.getPackageName();
            idMapTask.remove(pkgName);
            if (deleteApk) {
                File file = DownloadTools.getApkFiles(task);
                deleteFile(task.getPackageName(), file);
            }
        }
    }


    private void logDownloadWarnning(String packageName, String message) {
        logger.warn(TAG, "[" + packageName + "]下载失败，" + message);
    }

    private void initHandler(final Context context) {
        if (handler == null && context != null) {
            final Context appContext = context.getApplicationContext();
            handler = new Handler(context.getApplicationContext().getMainLooper()) {
                @Override
                public void handleMessage(Message msg) {
                    super.handleMessage(msg);
                    AppInfo app = (AppInfo) msg.obj;
                    switch (msg.what) {
                        case MSG_DOWNLOAD_SUCCESS:
                            ViewUtils.showToast(appContext, "【" + app.getName() + "】" + context.getResources().getString(R.string.tips_download_complete));
                            break;
                        case MSG_FILE_CHECK_FAILED:
                            ViewUtils.showToast(appContext, "【" + app.getName() + "】" + context.getResources().getString(R.string.tips_file_verify_failed));
                            break;
                    }
                }
            };
        }
    }

}
