package com.centerm.cpay.applicationshop.net.response;

import android.content.Context;

import com.centerm.cpay.applicationshop.bean.AppInfo;
import com.centerm.cpay.applicationshop.db.DbHelper;
import com.centerm.cpay.applicationshop.download.AppInfoSync;
import com.centerm.cpay.applicationshop.net.BaseParser;
import com.centerm.cpay.applicationshop.net.cont.JsonKey;
import com.j256.ormlite.android.apptools.OpenHelperManager;

import static com.centerm.cpay.applicationshop.net.cont.JsonKey.*;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * author: linwanliang</br>
 * date:2016/6/29</br>
 */
public class AppListParser extends BaseParser {
    protected JSONArray appsJson;
    public static final String[] JSON_KEYS = new String[]{packageName, name, rate, downloadCount
            , downloadUrl, iconUrl, size, version, versionName, appType, description, updateInfo
            , developer, upTime, previewImg, keyword, appMd5};

    private int total;
    protected List<AppInfo> appList;

    public AppListParser(Context context, String response) {
        super(response);
        JSONObject bodyJson = getBody();
        if (bodyJson == null) {
            return;
        }
        try {
            if (bodyJson.has(JsonKey.totalNumber)) {
                total = Integer.valueOf(bodyJson.getString(totalNumber));
            }
            if (bodyJson.has(apps)) {
                appsJson = bodyJson.getJSONArray(apps);
                appList = new ArrayList<>();
//                for (int i = 0; i < appsJson.length(); i++) {
//                    AppInfo item = AppInfo.convert(appsJson.getJSONObject(i));
//                    AppInfoSync.sync(context, item);
//                    appList.add(item);
//                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public List<AppInfo> getAppList() {
        return appList;
    }

    public int getTotal() {
        return total;
    }

    public JSONArray getAppsJson() {
        return appsJson;
    }
}
