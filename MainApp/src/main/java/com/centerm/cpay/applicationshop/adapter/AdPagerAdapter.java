package com.centerm.cpay.applicationshop.adapter;

import android.content.Context;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.bumptech.glide.Glide;
import com.centerm.cpay.applicationshop.R;
import com.centerm.cpay.applicationshop.bean.Advertisement;

import java.util.List;

/**
 * Created by liubit on 2016/11/28.
 */

public class AdPagerAdapter extends BaseAdapter {
    private Context mContext;
    private List<Advertisement> adCollection;

    public AdPagerAdapter(Context context,List<Advertisement> list){
        mContext = context;
        adCollection = list;
    }

    @Override
    public int getCount() {
        return adCollection.size();
    }

    @Override
    public Object getItem(int i) {
        return i;
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View convertView, ViewGroup viewGroup) {
        if (convertView == null) {
            convertView = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_ad,
                    viewGroup, false);
        }
        ImageView iv = (ImageView) convertView.findViewById(R.id.mIvAd);
        if(!TextUtils.isEmpty(adCollection.get(i).getImgUrl())){
            Glide.with(mContext)
                    .load(adCollection.get(i).getImgUrl())
                    .centerCrop()
                    .into(iv);
        }else {
            Glide.with(mContext)
                    .load(adCollection.get(i).getDrawableId())
                    .centerCrop()
                    .into(iv);
        }
        return convertView;
    }


}
