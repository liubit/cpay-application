package com.centerm.cpay.applicationshop.base;

import android.annotation.TargetApi;
import android.app.ActivityOptions;
import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.KeyEvent;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.centerm.cpay.applicationshop.R;
import com.centerm.cpay.applicationshop.activity.LaunchActivity;
import com.centerm.cpay.applicationshop.db.DbHelper;
import com.centerm.cpay.applicationshop.net.DefaultHttpClient;
import com.centerm.cpay.applicationshop.view.LoadingDialog;
import com.centerm.cloudsys.sdk.common.log.Log4d;
import com.j256.ormlite.android.apptools.OpenHelperManager;

import config.Config;

/**
 * Created by linwanliang on 2016/6/20.
 */
public abstract class BaseActivity_v21 extends AppCompatActivity implements IComponentCreator {

    protected static Log4d logger = Log4d.getDefaultInstance();
    protected String TAG;
    private DbHelper dbHelper;
    protected DefaultHttpClient httpClient;
    protected Context context;
    private long lastClick;
    private LoadingDialog loading;
    private Handler viewHanlder;
    private Runnable runnable;

    public ImageView mIvBack;
    public TextView mTvTitle;
    public ImageButton mBtnManage;
    public Toolbar mToolbar;

    public BaseActivity_v21() {
        httpClient = DefaultHttpClient.getInstance();
        TAG = this.getClass().getSimpleName();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = getWindow();
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS
                    | WindowManager.LayoutParams.FLAG_TRANSLUCENT_NAVIGATION);
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            if (this instanceof LaunchActivity) {
                window.getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                        | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                        | View.SYSTEM_UI_FLAG_LAYOUT_STABLE);
                window.setStatusBarColor(Color.TRANSPARENT);
                //getSupportActionBar().hide();
            } else {
                window.setStatusBarColor(getResources().getColor(R.color.colorPrimary));
            }
            window.setNavigationBarColor(Color.TRANSPARENT);
        }else {
            getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                    WindowManager.LayoutParams.FLAG_FULLSCREEN);
        }
        this.context = this;
        int layoutId = onInitLayoutId();
        if (layoutId > 0) {
            setContentView(layoutId);
        }
        onInitLocalData();
        onInitView(getWindow().getDecorView());
//        if (viewHanlder == null) {
        initRunnable();
        viewHanlder = new Handler();
        viewHanlder.postDelayed(runnable, Config.DELAY_LOAD_DATA);
//        }
    }

    private void initRunnable() {
        runnable = new Runnable() {
            @Override
            public void run() {
                doThingsAfterInitView();
            }
        };
    }

    @Override
    public void doThingsAfterInitView() {

    }

    @Override
    public boolean dispatchKeyEvent(KeyEvent event) {
//        logger.debug(TAG, "keyCode == " + event.getKeyCode());
        return super.dispatchKeyEvent(event);
    }

    @Override
    public boolean onKeyUp(int keyCode, KeyEvent event) {
//        logger.debug(TAG, "keyCode == " + keyCode + " onKeyUp");
        return super.onKeyUp(keyCode, event);
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
//        logger.debug(TAG, "keyCode == " + keyCode + " onKeyDown");
        return super.onKeyDown(keyCode, event);
    }

    /**
     * 获取数据库操作帮助对象
     *
     * @return 数据库操作帮助对象
     */
    public DbHelper getDbHelper() {
        if (dbHelper == null) {
            dbHelper = OpenHelperManager.getHelper(this, DbHelper.class);
        }
        return dbHelper;
    }

    public void initUsualToolbar(int title){
        mTvTitle = (TextView) findViewById(R.id.mTvTitle);
        mTvTitle.setText(title);
        findViewById(R.id.mLlBack).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                supportedFinish();
            }
        });
        setSupportActionBar(mToolbar);
    }

    public void initUsualToolbar(String title){
        mTvTitle = (TextView) findViewById(R.id.mTvTitle);
        mTvTitle.setText(title);
        findViewById(R.id.mLlBack).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                supportedFinish();
            }
        });
        setSupportActionBar(mToolbar);
    }



    protected void setRightBtnEnable(ActionBar bar, int visibility) {
        if (bar != null) {
            bar.getCustomView().findViewById(R.id.operate_btn).setVisibility(visibility);
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (dbHelper != null) {
            OpenHelperManager.releaseHelper();
            dbHelper = null;
        }
        stopLoading();
        loading = null;
        if (viewHanlder != null && runnable != null) {
            viewHanlder.removeCallbacks(runnable);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home)
            supportedFinish();
        return super.onOptionsItemSelected(item);
    }


    public void jumpTo(Intent intent, boolean finishSelf) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            jump_v21(intent);
        } else {
            jump(intent);
        }
        if (finishSelf) {
            supportedFinish();
        }
    }

    protected void jumpTo(Intent intent) {
        jumpTo(intent, false);
    }

    protected void jumpTo(Intent intent, View view, String sharedName) {
        jumpTo(intent, view, sharedName, false);
    }

    protected void jumpTo(Intent intent, View view, String sharedName, boolean finishSelf) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            jumpWithSharedElement(intent, view, sharedName);
        } else {
            jump(intent);
        }
        if (finishSelf) {
            supportedFinish();
        }
    }


    private void jump(Intent intent) {
        startActivity(intent);
    }


    @TargetApi(value = Build.VERSION_CODES.LOLLIPOP)
    private void jump_v21(Intent intent) {
        startActivity(intent, ActivityOptions.makeSceneTransitionAnimation(this).toBundle());
    }

    @TargetApi(value = Build.VERSION_CODES.LOLLIPOP)
    private void jumpWithSharedElement(Intent intent, View view, String sharedName) {
        startActivity(intent, ActivityOptions.makeSceneTransitionAnimation(this, view, sharedName).toBundle());
    }

    public void supportedFinish() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            finishAfterTransition();
        } else {
            super.finish();
        }
    }

    /**
     * 防止快速点击
     *
     * @return
     */
    protected boolean canPerformClick() {
        long now = System.currentTimeMillis();
        if (now - lastClick >= 1000) {
            lastClick = now;
            return true;
        }
        logger.warn(TAG, "点击速度过快，不响应事件");
        return false;
    }

    public void showLoading() {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (loading == null) {
                    loading = new LoadingDialog(BaseActivity_v21.this);
                }
                loading.show();
            }
        });

    }

    public void stopLoading() {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (loading != null) {
                    loading.dismiss();
                }
            }
        });
    }

    public DefaultHttpClient getHttpClient() {
        return httpClient;
    }

    @Override
    public boolean dispatchTouchEvent(MotionEvent ev) {
        if (ev.getAction() == MotionEvent.ACTION_DOWN) {
            View v = getCurrentFocus();
            if (isShouldHideInput(v, ev)) {
                InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                if (imm != null) {
                    imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
                }
            }
            return super.dispatchTouchEvent(ev);
        }
        // 必不可少，否则所有的组件都不会有TouchEvent了
        if (getWindow().superDispatchTouchEvent(ev)) {
            return true;
        }
        return onTouchEvent(ev);
    }

    private boolean isShouldHideInput(View v, MotionEvent event) {
        if (v != null && (v instanceof EditText)) {
            int[] leftTop = {0, 0};
            //获取输入框当前的location位置
            v.getLocationInWindow(leftTop);
            int left = leftTop[0];
            int top = leftTop[1];
            int bottom = top + v.getHeight();
            int right = left + v.getWidth();
            if (event.getX() > left && event.getX() < right
                    && event.getY() > top && event.getY() < bottom) {
                // 点击的是输入框区域，保留点击EditText的事件
                return false;
            } else {
                return true;
            }
        }
        return false;
    }

    @Override
    public Resources getResources() {
        Resources res = super.getResources();
        Configuration config = new Configuration();
        config.setToDefaults();
        res.updateConfiguration(config, res.getDisplayMetrics());
        return res;
    }
}
