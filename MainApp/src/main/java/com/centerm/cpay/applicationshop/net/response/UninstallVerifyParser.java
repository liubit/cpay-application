package com.centerm.cpay.applicationshop.net.response;

import com.centerm.cpay.applicationshop.bean.AppInfo;
import com.centerm.cpay.applicationshop.net.BaseParser;
import com.centerm.cpay.applicationshop.net.cont.JsonKey;


import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * author: linwanliang</br>
 * date:2016/7/13</br>
 */
public class UninstallVerifyParser extends BaseParser {

    private Set<String> uninstallSets = new HashSet<>();

    public UninstallVerifyParser(List<AppInfo> requestParams, String response) {
        super(response);
        if (requestParams == null || requestParams.size() == 0) {
            return;
        }
        if (getBody() != null) {
            try {
                JSONObject result = getBody().getJSONObject(JsonKey.checkResult);
//                LOGGER.info(TAG,"checkResult:"+result.toString());
                for (AppInfo app : requestParams) {
                    String pkgName = app.getPackageName();
                    if (result.has(pkgName)) {
                        boolean value = result.getBoolean(pkgName);
//                        LOGGER.debug(TAG,"pkg:"+pkgName+"  value:"+value);
                        if (!value) {
                            uninstallSets.add(pkgName);
//                            LOGGER.debug(TAG,"sets:"+uninstallSets.toString());
                        }
                    }
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    public Set<String> getUninstallSets() {
        return uninstallSets;
    }

    public boolean canInstall(String pkgName) {
        return !uninstallSets.contains(pkgName);
    }

}
