package com.centerm.cpay.applicationshop.activity;

import android.content.Intent;
import android.view.View;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.TextView;

import com.centerm.cpay.applicationshop.R;
import com.centerm.cpay.applicationshop.base.BaseActivity_v21;
import com.centerm.cpay.applicationshop.common.AppSettings;
import com.centerm.cpay.applicationshop.common.MaterialDialogManager;
import com.centerm.cpay.applicationshop.net.NetCacheManager;
import com.centerm.cpay.applicationshop.utils.FileUtils;
import com.centerm.cpay.applicationshop.utils.PackageUtils;

import java.io.File;

/**
 * Created by linwanliang on 2016/6/27.
 */
public class SetupActivity extends BaseActivity_v21 implements View.OnClickListener {
    @Override
    public int onInitLayoutId() {
        return R.layout.activity_setup;
    }

    @Override
    public void onInitLocalData() {

    }

    @Override
    public void onInitView(View view) {
        initUsualToolbar(R.string.title_setup);
        initItemViews();
    }

    private void initItemViews() {
        //应用更新自动提示
        View item1 = findViewById(R.id.update_auto_remind_btn);
        TextView itemName1 = (TextView) item1.findViewById(R.id.item_name_show);
        itemName1.setText(R.string.setup_title1);
        TextView subItemName1 = (TextView) item1.findViewById(R.id.sub_item_name_show);
        subItemName1.setVisibility(View.VISIBLE);
        subItemName1.setText(R.string.setup_subtitle1);
        item1.findViewById(R.id.tips_show).setVisibility(View.GONE);
        CheckBox toggle1 = (CheckBox) item1.findViewById(R.id.toggle);
        toggle1.setVisibility(View.VISIBLE);
        toggle1.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                AppSettings.setAutoRemind(context, isChecked);
            }
        });
        toggle1.setChecked(AppSettings.isAutoRemind(this));
        //清除缓存
        refreshCacheShow(false);
        //关于
        View item3 = findViewById(R.id.about_btn);
        TextView itemName3 = (TextView) item3.findViewById(R.id.item_name_show);
        itemName3.setText(R.string.setup_title3);
        TextView subItemName3 = (TextView) item3.findViewById(R.id.sub_item_name_show);
        TextView tipShow3 = (TextView) item3.findViewById(R.id.tips_show);
        String versionName = PackageUtils.getInstalledVersionName(this, getPackageName());
        tipShow3.setText(versionName);
        item3.setOnClickListener(this);

    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.operate_btn:
                break;
            case R.id.clear_cache_btn:
                MaterialDialogManager.getChooserByTips(this, R.string.p_clean, new MaterialDialogManager.ButtonClick() {
                    @Override
                    public void onClick() {
                        deleteCache();
                    }
                }).show();
                break;
            case R.id.about_btn:
                Intent intent = new Intent(context, AboutActivity.class);
                jumpTo(intent);
                break;
        }
    }

    private void deleteCache() {
        long size = FileUtils.getDirSize(getCacheDir());
        if (size <= 0) {
            return;
        }
        showLoading();
        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                FileUtils.deleteAllFiles(FileUtils.getCacheDir(context));
                NetCacheManager netCacheManager = NetCacheManager.getInstance();
                FileUtils.deleteAllFiles(netCacheManager.getCacheDir(context));
                stopLoading();
                refreshCacheShow(true);
            }
        }
        ).start();
    }

    private void refreshCacheShow(final boolean reset) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                View item2 = findViewById(R.id.clear_cache_btn);
                TextView itemName2 = (TextView) item2.findViewById(R.id.item_name_show);
                itemName2.setText(R.string.setup_title2);
                TextView subItemName2 = (TextView) item2.findViewById(R.id.sub_item_name_show);
                TextView tipShow2 = (TextView) item2.findViewById(R.id.tips_show);
                if (reset) {
                    tipShow2.setText("0B");
                } else {
                    File cacheDir = new File(FileUtils.getCacheDir(context));
                    tipShow2.setText(FileUtils.formatSize(FileUtils.getDirSize(cacheDir)));
                    item2.setOnClickListener(SetupActivity.this);
                }
            }
        });
    }

}
