package com.centerm.cpay.applicationshop.view;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.drawable.AnimationDrawable;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.centerm.cpay.applicationshop.R;

/**
 * 加载对话框
 *
 * @author wanliang527
 * @date 2014-09-10
 */
public class LoadingDialog extends Dialog {

    private Context mContext;
    private LayoutInflater mInflater;
    private ImageView mLoadingIcon;
    private TextView mHintView;

    public LoadingDialog(Context context, int theme) {
        super(context, theme);
        init(context);
    }

    public LoadingDialog(Context context) {
        super(context, R.style.LoadingDialog);
        init(context);
    }

    private void init(Context context) {
        mContext = context;
        mInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View v = mInflater.inflate(R.layout.common_loading_dialog_layout, null);
        mLoadingIcon = (ImageView) v.findViewById(R.id.loading_icon_show);
        mHintView = (TextView) v.findViewById(R.id.loading_text_show);
        mHintView.setTextColor(context.getResources().getColor(android.R.color.white));
        this.setContentView(v);
        this.setCanceledOnTouchOutside(false);
        this.setOnKeyListener(new OnKeyListener() {
            @Override
            public boolean onKey(DialogInterface dialog, int keyCode, KeyEvent event) {
                if (keyCode == KeyEvent.KEYCODE_BACK || keyCode == KeyEvent.KEYCODE_HOME) {
                    return true;
                }
                return false;
            }
        });
    }

    public TextView getText() {
        return mHintView;
    }

    @Override
    public void show() {
        AnimationDrawable anim = (AnimationDrawable) mLoadingIcon.getDrawable();
        anim.start();
        /*if (mHintView != null)
            mHintView.setText("加载中，请稍候...");
        if (mLoadingIcon != null)
            mLoadingIcon.startAnimation(mRotateAnim);*/
        super.show();
    }

/*    public void show(String hint) {
        AnimationDrawable anim = (AnimationDrawable) mLoadingIcon.getDrawable();
        anim.start();
      *//*  if (mHintView != null)
            mHintView.setText(hint);
        if (mLoadingIcon != null)
            mLoadingIcon.startAnimation(mRotateAnim);*//*
        super.show();
    }

    public void show(int resId) {
        String hint = mContext.getResources().getString(resId);
        show(hint);
    }*/

    @Override
    public void dismiss() {
        AnimationDrawable anim = (AnimationDrawable) mLoadingIcon.getDrawable();
        anim.stop();
        super.dismiss();
    }

  /*  public void setHint(String hint) {
        if (mHintView != null)
            mHintView.setText(hint);
    }*/
}
