apply plugin: 'com.android.application'

android {
    signingConfigs {
        debug {
            storeFile file('../system.keystore')
        }
        release {
            keyAlias 'androiddebugkey'
            keyPassword 'android'
            storeFile file('../system.keystore')
            storePassword 'android'
        }
    }
    compileSdkVersion 23
    buildToolsVersion "23.0.3"
    packagingOptions {
        exclude 'META-INF/LICENSE'
        exclude 'META-INF/NOTICE'
        exclude 'META-INF/DEPENDENCIES'
    }
    defaultConfig {
        applicationId "com.centerm.cpay.applicationshop"
        minSdkVersion 15
        targetSdkVersion 23
//        versionCode 300
//        versionName "3.0.0-yxl"
        versionCode 129
        versionName "3.0.5-RC1"
    }
    buildTypes {
        release {
            minifyEnabled true
            proguardFiles getDefaultProguardFile('proguard-android.txt'), 'proguard-rules.pro'
            signingConfig signingConfigs.release
            //是否删除无用的Resource文件，依赖于minifyEnabled，否则无法生效
            shrinkResources true
        }
        debug {
            signingConfig signingConfigs.debug
        }

        applicationVariants.all { variant ->
            def time = new java.text.SimpleDateFormat("yyyyMMdd").format(new Date())
            variant.outputs.each { output ->
                def file = output.outputFile
                def fileName = "AppCloud_V${defaultConfig.versionName}_B${defaultConfig.versionCode}_" + time + "-${variant.productFlavors[0].name}.apk";
                output.outputFile = new File(file.parent, fileName)
            }
            tasks.copyReleaseVersion.execute()
        }
    }
    productFlavors {
        //杰博实生产环境
        JPosProc {
            manifestPlaceholders = [NET_ADDRESS   : "http://59.56.101.177:6931"
                                    , CHANNEL_NAME: "jpos"]
        }
        //杰博实测试环境
        JPosTest {
            manifestPlaceholders = [NET_ADDRESS   : "http://139.196.226.55:6931"
                                    , CHANNEL_NAME: "jpos"]
        }
        //内部测试环境
        CpayIn {
            manifestPlaceholders = [NET_ADDRESS   : "http://192.168.48.45:8080/cpay-store"
                                    , CHANNEL_NAME: "centerm"]
        }
        //演练环境
        CpayOut {
            manifestPlaceholders = [NET_ADDRESS   : "http://114.55.227.72:6931"
                                    , CHANNEL_NAME: "centerm"]
        }
    }
    lintOptions {
        checkReleaseBuilds false
        abortOnError false
    }

}


dependencies {
    compile fileTree(include: ['*.jar'], dir: 'libs')
    testCompile 'junit:junit:4.12'
    compile files('libs/httpclient-4.5.jar')
    compile files('libs/httpcore-4.4.1.jar')
    compile 'com.android.support:cardview-v7:23.3.0'
    compile 'com.android.support:appcompat-v7:23.3.0'
    compile 'com.android.support:design:23.3.0'
    compile 'com.google.android.gms:play-services-appindexing:8.1.0'
    compile project(':Library_MbRefreshLayout')
    compile files('libs/universal-image-loader-1.9.4-with-sources.jar')
    compile project(':Library_ImageSlider')
    compile 'org.apmem.tools:layouts:1.10'
    compile files('libs/gson-2.2.4.jar')
    compile 'com.github.bumptech.glide:glide:3.6.1'
    compile 'org.xutils:xutils:3.3.34'
    compile('com.github.afollestad.material-dialogs:core:0.8.5.8@aar') {
        transitive = true
    }
    //    compile files('libs/android-async-httpclient-1.4.8-with-sources.jar')
    compile files('libs/android-async-http-1.4.8-with-sources.jar')
    compile files('libs/CpayMidLayerSDK_ForCpay_V0.2.0_B17_20161111.jar')
    compile files('libs/CloudsysLib_Common_V0.0.8_B13_20160819.jar')
}

task copyReleaseVersion(type: Copy) {
    def versionName = android.defaultConfig.versionName
    def fromDir = "${buildDir}/outputs/apk/"
    def toDir = RELEASE_DIR + versionName
    from fromDir
    into toDir
    include "**/*.apk"

    // 不拷贝未签名的文件.
    exclude { details ->
        details.file.name.contains('-unaligned') ||
                details.file.name.contains('-unsigned') ||
                !details.file.name.contains(versionName) ||
                details.file.name.contains('-debug')
    }
}